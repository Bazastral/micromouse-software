#!/bin/bash

# exit when error occurs
set -ex

SCRIPT_PATH=`dirname "$(readlink -f "$0")"`

BUILD_DIR="${SCRIPT_PATH}/build_host"

cd ${BUILD_DIR}

ctest --output-on-failure --schedule-random --tests-regex hw