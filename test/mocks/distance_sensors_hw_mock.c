#include "distance_sensors.h"
#include "distance_sensors_hw_mock.h"
#include "distance_sensors_hw.h"
#include "useful.h"
#include "ret_codes.h"


static int length;
static double distances[10];

ret_code_t dsGetDistances(double* dists, int len)
{
    if(len != length)
    {
        return RET_ERR_INVALID_PARAM;
    }

    for(uint8_t idx = 0; idx < len; idx++)
    {
        dists[idx] = distances[idx];
    }

    return RET_SUCCESS;
}


void dsSetDistances(double* dists, int len)
{
    if(len > (int)ARRAY_SIZE(distances))
    {
        printf("ERROR\n");
        return;
    }
    length = len;

    for(uint8_t idx = 0; idx < len; idx++)
    {
        distances[idx] = dists[idx];
    }

}

dsConfig_t dsGetCfgPlatformDefault(void)
{
  dsConfig_t defaultCfg =
  {
    .numberOfSensors = 1,
    .sensors =
    {
      {.offset = {0, 0, 0}},
    },
    .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
    .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
    .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS
  };
  return defaultCfg;
}