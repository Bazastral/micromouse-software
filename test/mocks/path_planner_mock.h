

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>

#include "path_planner.h"

void pp_mock_initialize(void);
int pp_mock_find_path_call_counter(void);
lab_cellxy_t pp_mock_find_path_param_from(void);
lab_cellxy_t pp_mock_find_path_param_to(void);