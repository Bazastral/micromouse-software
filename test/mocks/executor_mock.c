

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>

#include "executor.h"
#include "executor_mock.h"

static int init_called_times = 0;

void executorMockInit()
{
  init_called_times = 0;
}

uint32_t executorInitMockCalledTimes(void)
{
  return init_called_times;
}

ret_code_t executorInit(const config_t *cfg_)
{
  (void) cfg_;
  init_called_times++;
  return RET_SUCCESS;
}

ret_code_t executorGoToTarget(point_t target, state_t state, double time, motors_speed_t *motors)
{
  (void) target;
  (void) state;
  (void) time;
  (void) motors;
  return RET_ERR_NOT_IMPLEMENTED;
}

ret_code_t executorGoToTargetAndStop(point_t aim, state_t state, double time, motors_speed_t *motors)
{
  (void) aim;
  (void) state;
  (void) time;
  (void) motors;
  return RET_ERR_OTHER;
}

motors_speed_t executorSetRawVelocities(double raw_linear, double raw_angular);

void executorSetSpeedRun(bool speed)
{
  (void)speed;
  return;
}