

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>

#include "executor.h"

void executorMockInit();

uint32_t executorInitMockCalledTimes(void);

ret_code_t executorInit(const config_t *cfg_);

ret_code_t executorGoToTarget(point_t target, state_t state, double time, motors_speed_t *motors);

ret_code_t executorGoToTargetAndStop(point_t aim, state_t state, double time, motors_speed_t *motors);

motors_speed_t executorSetRawVelocities(double raw_linear, double raw_angular);
