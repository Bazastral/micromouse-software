

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>

#include "state.h"


point_t stateGetPoint(state_t state)
{
  (void) state;
  point_t p = {0, 0};
  return p;
}

// pos_t stateGetPosition(state_t state)
// {

// }

void stateInitModule(const config_t *cfg)
{
  (void)cfg;
}


ret_code_t stateStep(
    const double time,
    state_t *state,
    double *measurements,
    uint8_t measurements_num,
    motors_ticks_t ticks)
{
  (void)time;
  (void)state;
  (void)measurements;
  (void)measurements_num;
  (void)ticks;
  return RET_SUCCESS;
}

// ret_code_t stateNextModel(
//     double time,
//     state_t st_now,
//     state_t *st_next);

// ret_code_t stateNextEncoders(
//     double time,
//     state_t now,
//     state_t *next,
//     motors_ticks_t ticks);

// ret_code_t stateNextDistSensors(
//     const state_t prev,
//     state_t *next,
//     double *measurements);

ret_code_t stateInitialize(state_t *st)
{
  (void)st;
  return RET_SUCCESS;
}
state_t stateStartingPoint(void)
{
  state_t state = { 0 };
  return state;
}

void stateDump(char* desc, state_t s)
{
 (void) desc;
 (void) s;
}

ret_code_t stateDetectWalls(
    const state_t current_state,
    const double *measurements,
    const double measurements_num)
{
  (void) current_state;
  (void) measurements;
  (void) measurements_num;
  return RET_SUCCESS;
}
