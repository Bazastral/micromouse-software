

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>

#include "platform_base.h"
#include "useful.h"

static uint32_t timestamp_initialization_us = 0;
static uint32_t time_now_us = 0;

void platformInit(void)
{
  timestamp_initialization_us = 0;
}

void platformDelayMs(uint32_t ms)
{
  time_now_us += MILLISECONDS_TO_MICROSECONDS(ms);
}

void platformDelayUs(uint32_t us)
{
  time_now_us += us;
}

void platformPutChar(const char c)
{
  (void) c;
  printf("%c", c);
}

bool platoformIsButtonPressed(uint8_t idx)
{
  (void) idx;
  return false;
}

uint32_t platformGetElapsedUsFromInit(void)
{
  return time_now_us - timestamp_initialization_us;
}