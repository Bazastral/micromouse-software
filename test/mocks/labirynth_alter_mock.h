
#pragma once

#include "labirynth.h"
#include "labirynth_internal.h"
#include "labirynth_alter.h"
#include "ret_codes.h"
#include "typedef.h"
#include "useful.h"

void labAlterMockInit(void);

changeCallback_t labMockGetChangeCallback(void);
