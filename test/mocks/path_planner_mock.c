

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "path_planner.h"


static int _find_path_call_counter = 0;
static lab_cellxy_t _from = {0};
static lab_cellxy_t _to = {0};

void pp_mock_initialize(void)
{
  _find_path_call_counter = 0;
  memset(&_from, 0, sizeof(_from));
  memset(&_to, 0, sizeof(_to));
}

int pp_mock_find_path_call_counter(void)
{
  return _find_path_call_counter;
}

lab_cellxy_t pp_mock_find_path_param_from(void)
{
  return _from;
}

lab_cellxy_t pp_mock_find_path_param_to(void)
{
  return _to;
}

ret_code_t pp_find_path(lab_cellxy_t from, lab_cellxy_t to, vector_t *path)
{
  _find_path_call_counter++;
  _from = from;
  _to = to;
  (void) path;

  return RET_ERR_NOT_IMPLEMENTED;
}

ret_code_t pp_plan_mapping(
    lab_cellxy_t pos,
    lab_cellxy_t aim,
    lab_cellxy_t *target,
    vector_t *path,
    vector_t *pathB)
{
  (void) pos;
  (void) aim;
  (void) target;
  (void) path;
  (void) pathB;
  return RET_ERR_NOT_IMPLEMENTED;
}
