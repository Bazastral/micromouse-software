#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>

#include "labirynth.h"
#include "labirynth_internal.h"
#include "labirynth_alter.h"
#include "ret_codes.h"
#include "typedef.h"
#include "useful.h"

static changeCallback_t _callback = NULL;

void labAlterMockInit(void)
{
    _callback = NULL;
}

ret_code_t labWallDel(lab_cellxy_t coords, lab_cell_t cell)
{
  (void) coords;
  (void) cell;
  return RET_SUCCESS;
}

ret_code_t labWallAdd(lab_cellxy_t coords, lab_cell_t cell)
{
  (void) coords;
  (void) cell;
  return RET_SUCCESS;
}

ret_code_t labWallChange(lab_cellxy_t coords, lab_cell_t cell, bool add)
{
  (void) coords;
  (void) cell;
  (void) add;
  return RET_SUCCESS;
}


ret_code_t labResetToDefault(void)
{
  return RET_SUCCESS;
}

ret_code_t labLoad(const labirynth_t load)
{
  (void) load;
  return RET_SUCCESS;
}

void labRegisterChangeCallback(changeCallback_t callback)
{
    _callback = callback;
}

changeCallback_t labMockGetChangeCallback(void)
{
    return _callback;
}
