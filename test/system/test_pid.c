
#include <string.h>
#include <math.h>

#include "unity.h"
#include "unity_fixture.h"

#include "state.h"
#include "platform_state.h"
#include "platform_base.h"
#include "labirynth.h"
#include "pid.h"
#include "executor.h"
#include "logger.h"
#include "useful.h"
#include "labirynth_alter.h"
#include "exhibit.h"
#include "timing.h"
#include "logger.h"


static const double LINEAR_VELOCITY_OFFSET_ALLOWED = 20;
static const double ANGULAR_VELOCITY_OFFSET_ALLOWED = DEG_2_RAD(10);
static const double LINEAR_VELOCITY_TYPICAL = 400;
static const double ANGULAR_VELOCITY_TYPICAL = 2*M_PI;
static const double TIME_STEP_S = 0.01;

state_t st_now;

void setUpCommon(void)
{
  labResetToDefault();
  stateInitModule(configGet());

  stateInitialize(&st_now);
  statePlatformSet(st_now);
  log_set_verbosity_level(LOGGER_LVL_INFO); // omit all debug output
}

void tearDownCommon()
{
  motorSetSpeed((motors_speed_t){0, 0});
  platformDelayMs(1000);
}

static void achieve_velocity( pid_controller_t pidController,
  bool linear, double target_speed, const double DRIVE_TIME, const double STEP_TIME);

TEST_GROUP(pidVelocityUsingEncoders);
TEST_SETUP(pidVelocityUsingEncoders) { setUpCommon(); }
TEST_TEAR_DOWN(pidVelocityUsingEncoders) { tearDownCommon(); }

void test_pid(bool linear, double target_speed)
{
  const double STEP_TIME = 0.01;
  const double DRIVE_TIME = 1.0;
  uint8_t pidMem[PID_CONTROLLER_STRUCT_SIZE_BYTES];
  pid_controller_t pidController = (pid_controller_t)pidMem;

  if(linear)
  {
    pidInit(pidController, configGet()->linearPIDp, configGet()->linearPIDi, configGet()->linearPIDd);
  }
  else
  {
    pidInit(pidController, configGet()->angularPIDp, configGet()->angularPIDi, configGet()->angularPIDd);
  }

  log_info("Test params: step %f, acceleration_time %f, target_velocity %f, linear %d",
    STEP_TIME, DRIVE_TIME, target_speed, (int)linear);


  log_info("\n\nACCELERATE");
  achieve_velocity(pidController, linear, target_speed, DRIVE_TIME, STEP_TIME);
  if(linear) { TEST_ASSERT_DOUBLE_WITHIN(LINEAR_VELOCITY_OFFSET_ALLOWED, target_speed, st_now.v); }
  else { TEST_ASSERT_DOUBLE_WITHIN(ANGULAR_VELOCITY_OFFSET_ALLOWED, target_speed, st_now.w); }

  state_t st;
  ret_code_t ret = statePlatformGet(&st);
  if(ret == RET_SUCCESS)
  {
    //only on simulator
    if(linear) { TEST_ASSERT_DOUBLE_WITHIN(LINEAR_VELOCITY_OFFSET_ALLOWED, st.v, st_now.v); }
    else { TEST_ASSERT_DOUBLE_WITHIN(ANGULAR_VELOCITY_OFFSET_ALLOWED, st.w, st_now.w); }
  }

  log_info("\n\nDECELERATE");
  target_speed = 0;
  achieve_velocity(pidController, linear, target_speed, DRIVE_TIME, STEP_TIME);
  if(linear) { TEST_ASSERT_DOUBLE_WITHIN(LINEAR_VELOCITY_OFFSET_ALLOWED, target_speed, st_now.v); }
  else { TEST_ASSERT_DOUBLE_WITHIN(ANGULAR_VELOCITY_OFFSET_ALLOWED, target_speed, st_now.w); }

  ret = statePlatformGet(&st);
  if(ret == RET_SUCCESS)
  {
    //only on simulator
    if(linear) { TEST_ASSERT_DOUBLE_WITHIN(LINEAR_VELOCITY_OFFSET_ALLOWED, st.v, st_now.v); }
    else { TEST_ASSERT_DOUBLE_WITHIN(ANGULAR_VELOCITY_OFFSET_ALLOWED, st.w, st_now.w); }
  }

}

void achieve_velocity( pid_controller_t pidController,
  bool linear, double target_speed, const double DRIVE_TIME, const double STEP_TIME)
{
  
  double pid_output = 0;
  state_t st_next = {0};
  ret_code_t ret = 0;
  motors_ticks_t ticks = {0};

  timing_loop_ctx_t looping_ctx = {0};

  timingLoopInitialize(&looping_ctx, SECONDS_TO_MICROSECONDS(TIME_STEP_S));
  for (double time = DRIVE_TIME; time > 0; time -= STEP_TIME)
  {
    double velocity;
    if(linear) { velocity = st_now.v; }
    else{ velocity = st_now.w; }

    double error = target_speed - velocity;

    ret = pidStep(pidController, STEP_TIME, error, &pid_output);
    TEST_ASSERT_EQUAL(ret, RET_SUCCESS);

    if(linear) { TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(executorSetRawVelocities(pid_output, 0))); }
    else{ TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(executorSetRawVelocities(0, pid_output))); }

    timingLoopControl(&looping_ctx);

    TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
    TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(STEP_TIME, st_now, &st_next, ticks));
    st_now = st_next;
    log_info("e%+7.1f u%+6.2f, v %+7.2f w %+7.2f", error, pid_output, st_now.v, st_now.w);
    statePlatformNotify(st_now);
  }
  timingLoopPrintStats(&looping_ctx);
  TEST_ASSERT_EQUAL(0, looping_ctx.external.errors_number);
}

TEST(pidVelocityUsingEncoders, When_DriveForwardWithConstantSpeed_Then_CheckThatVelocity)
{
  test_pid(true, LINEAR_VELOCITY_TYPICAL);
}

TEST(pidVelocityUsingEncoders, When_RotateWithConstantSpeed_Then_CheckThatVelocity)
{
  test_pid(false, ANGULAR_VELOCITY_TYPICAL);
}

TEST_GROUP_RUNNER(pidVelocityUsingEncoders)
{
  RUN_TEST_CASE(pidVelocityUsingEncoders, When_DriveForwardWithConstantSpeed_Then_CheckThatVelocity);
  RUN_TEST_CASE(pidVelocityUsingEncoders, When_RotateWithConstantSpeed_Then_CheckThatVelocity);
}

int main(void)
{
  UNITY_BEGIN();
  platformInit();

  RUN_TEST_GROUP(pidVelocityUsingEncoders);

  int failures = UnityEnd();
  exhibitFailures(failures);

  return failures;
}
