
#include <string.h>

#include "unity.h"
#include "unity_fixture.h"

#include "executor.h"
#include "useful.h"
#include "state.h"
#include "platform_state.h"
#include "platform_logger.h"
#include "platform_base.h"
#include "labirynth.h"
#include "labirynth_alter.h"
#include "labirynths.h"
#include "distance_sensors_hw.h"
#include "platform_labirynth.h"
#include "exhibit.h"
#include "timing.h"
#include "logger.h"

const double OFFSET_ALLOWED_IN_STATE_POSITION_MM = 17;

point_t target;
state_t state_now, sim_state;
motors_speed_t mot;
motors_ticks_t ticks;
const double TIME_STEP_S = 0.01;
ret_code_t ret;
double measurements[MAX_DISTANCE_SENSORS_NUM];
timing_loop_ctx_t looping_ctx = {0};

void setUpCommon(void)
{
  labResetToDefault();
  labLoad(labirynths[LABIRYNTHS_ARENA]);
  labirynthSetup(LABIRYNTHS_ARENA);
  platformInit();
  labTotalReset();
  stateInitModule(configGet());
  log_set_verbosity_level(LOGGER_LVL_INFO);
}

void tearDownCommon()
{
  log_raw("\n");
  timingLoopPrintStats(&looping_ctx);
  motorSetSpeed((motors_speed_t){0, 0});
  platformDelayMs(1000);
}

TEST_GROUP(stateBasedOnMotorEncoder);
TEST_SETUP(stateBasedOnMotorEncoder) { setUpCommon(); }
TEST_TEAR_DOWN(stateBasedOnMotorEncoder) { tearDownCommon(); }

TEST(stateBasedOnMotorEncoder, When_DriveForwardAbit_Then_AchieveTarget)
{
  const double MAX_TIME_SECONDS = 8;
  const double METERS_TO_DRIVE = 0.3;
  double time = 0;

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&state_now));
  state_now.x = ONE_METER;
  state_now.y = LAB_DIM_WALL + LAB_DIM_CORRIDOR_HALF;
  state_now.t = 0;
  target.x = state_now.x + METERS_TO_MILLIMETERS(METERS_TO_DRIVE);
  target.y = state_now.y;
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(state_now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformNotify(state_now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  memset(&ticks, 0, sizeof(ticks));

  platformLoggerFlush();
  timingLoopInitialize(&looping_ctx, SECONDS_TO_MICROSECONDS(TIME_STEP_S));

  uint32_t start_timestamp = 0;
  timingMeasureStart(&start_timestamp);
  do
  {
    ret = executorGoToTargetAndStop(target, state_now, TIME_STEP_S, &mot);
    motorSetSpeed(mot);
    timingLoopControl(&looping_ctx);

    motorGetTicks(&ticks);
    stateNextEncoders(TIME_STEP_S, state_now, &state_now, ticks);
    statePlatformNotify(state_now);
    time = MICROSECONDS_TO_SECONDS(timingMeasureElapsedUs(start_timestamp));
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  stateDump("END", state_now);
  platformLoggerFlush();
  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, state_now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, state_now.y);
  TEST_ASSERT_EQUAL(0, looping_ctx.external.errors_number);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, state_now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, state_now.y);
  }

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, state_now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, state_now.y);
  }
}

TEST(stateBasedOnMotorEncoder, When_DriveBackwardCell_Then_RotateAndGo)
{
  const double MAX_TIME_SECONDS = 3;
  const int CELLS_TO_DRIVE = 1;
  double time = 0;

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&state_now));
  target.x = state_now.x;
  target.y = state_now.y;
  state_now.y = target.y + CELLS_TO_DRIVE*LAB_DIM_CELL;

  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(state_now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  memset(&ticks, 0, sizeof(ticks));

  timingLoopInitialize(&looping_ctx, SECONDS_TO_MICROSECONDS(TIME_STEP_S));
  uint32_t start_timestamp = 0;
  timingMeasureStart(&start_timestamp);
  do
  {
    ret = executorGoToTargetAndStop(target, state_now, TIME_STEP_S, &mot);
    motorSetSpeed(mot);
    timingLoopControl(&looping_ctx);

    motorGetTicks(&ticks);
    stateNextEncoders(TIME_STEP_S, state_now, &state_now, ticks);
    statePlatformNotify(state_now);
    time = MICROSECONDS_TO_SECONDS(timingMeasureElapsedUs(start_timestamp));
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));
  platformLoggerFlush();

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, state_now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, state_now.y);
  TEST_ASSERT_EQUAL(0, looping_ctx.external.errors_number);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, state_now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, state_now.y);
  }
}

TEST(stateBasedOnMotorEncoder, When_DriveLeftOneCell_Then_RotateLeftAndGo)
{
  const double MAX_TIME_SECONDS = 3;
  double time = 0;

  target = labCellToPoint((lab_cellxy_t){0, 1});

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&state_now));
  state_now.t = 0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(state_now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  memset(&ticks, 0, sizeof(ticks));

  timingLoopInitialize(&looping_ctx, SECONDS_TO_MICROSECONDS(TIME_STEP_S));
  uint32_t start_timestamp = 0;
  timingMeasureStart(&start_timestamp);
  do
  {
    ret = executorGoToTargetAndStop(target, state_now, TIME_STEP_S, &mot);
    motorSetSpeed(mot);
    timingLoopControl(&looping_ctx);

    motorGetTicks(&ticks);
    stateNextEncoders(TIME_STEP_S, state_now, &state_now, ticks);
    statePlatformNotify(state_now);
    time = MICROSECONDS_TO_SECONDS(timingMeasureElapsedUs(start_timestamp));
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(0, looping_ctx.external.errors_number);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, state_now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, state_now.y);
  }
}


TEST_GROUP(stateBasedOnEncodersAndDistanceSensors);
TEST_SETUP(stateBasedOnEncodersAndDistanceSensors) { setUpCommon(); }
TEST_TEAR_DOWN(stateBasedOnEncodersAndDistanceSensors) { tearDownCommon(); }

TEST(stateBasedOnEncodersAndDistanceSensors, Given_NoWallsAtAll_When_DriveForward5cells_Then_DoIt)
{
  const double MAX_TIME_SECONDS = 6;
  double time = 0;
  lab_cellxy_t beginning_cell = {2, 0};
  lab_cellxy_t end_cell = {2, 4};

  labLoad(labirynths[LABIRYNTHS_ARENA]);
  labirynthSetup(LABIRYNTHS_ARENA);
  target = labCellToPoint(end_cell);

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&state_now));
  state_now.x = labCellToPoint(beginning_cell).x;
  state_now.y = labCellToPoint(beginning_cell).y;
  statePlatformNotify(state_now);
  statePlatformSet(state_now);
  executorInit(configGet());
  dsInit(dsGetCfgPlatformDefault());
  memset(&ticks, 0, sizeof(ticks));

  timingLoopInitialize(&looping_ctx, SECONDS_TO_MICROSECONDS(TIME_STEP_S));
  uint32_t start_timestamp = 0;
  timingMeasureStart(&start_timestamp);
  do
  {
    ret = executorGoToTargetAndStop(target, state_now, TIME_STEP_S, &mot);
    motorSetSpeed(mot);
    timingLoopControl(&looping_ctx);

    motorGetTicks(&ticks);
    dsGetDistances(measurements, ARRAY_SIZE(measurements));
    stateStep(TIME_STEP_S, &state_now, measurements, dsGetCfgPlatformDefault().numberOfSensors, ticks);
    statePlatformNotify(state_now);
    time = MICROSECONDS_TO_SECONDS(timingMeasureElapsedUs(start_timestamp));
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, state_now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, state_now.y);
  TEST_ASSERT_EQUAL(0, looping_ctx.external.errors_number);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, state_now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, state_now.y);
  }
}

TEST(stateBasedOnEncodersAndDistanceSensors, Given_WallsKnownOnLeftAndRight_When_DriveForward5cellsN_Then_DoIt)
{
  const double MAX_TIME_SECONDS = 6;
  double time = 0;

  labResetToDefault();
  labLoad(labirynths[LABIRYNTHS_TEST_STRAIGHT_NS]);
  labirynthSetup(LABIRYNTHS_TEST_STRAIGHT_NS);

  target = labCellToPoint((lab_cellxy_t){0, 4});

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&state_now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(state_now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  TEST_ASSERT_EQUAL(RET_SUCCESS, dsInit(dsGetCfgPlatformDefault()));
  memset(&ticks, 0, sizeof(ticks));

  timingLoopInitialize(&looping_ctx, SECONDS_TO_MICROSECONDS(TIME_STEP_S));
  uint32_t start_timestamp = 0;
  timingMeasureStart(&start_timestamp);
  do
  {
    ret = executorGoToTargetAndStop(target, state_now, TIME_STEP_S, &mot);
    motorSetSpeed(mot);
    timingLoopControl(&looping_ctx);

    motorGetTicks(&ticks);
    dsGetDistances(measurements, ARRAY_SIZE(measurements));
    stateStep(TIME_STEP_S, &state_now, measurements, dsGetCfgPlatformDefault().numberOfSensors, ticks);
    statePlatformNotify(state_now);
    time = MICROSECONDS_TO_SECONDS(timingMeasureElapsedUs(start_timestamp));
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, state_now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, state_now.y);
  TEST_ASSERT_EQUAL(0, looping_ctx.external.errors_number);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, state_now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, state_now.y);
  }
}

TEST(stateBasedOnEncodersAndDistanceSensors, Given_WallsKnownOnLeftAndRight_When_DriveForwardEtoTheEndOfCorridor_Then_DoIt)
{
  const double MAX_TIME_SECONDS = 10;
  double time = 0;

  labResetToDefault();
  labLoad(labirynths[LABIRYNTHS_TEST_STRAIGHT_EW]);
  labirynthSetup(LABIRYNTHS_TEST_STRAIGHT_EW);

  target = labCellToPoint((lab_cellxy_t){LAB_N-1, 0});

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&state_now));
  const int CELLS_TO_DRIVE = 3;
  state_now.x = labCellToPoint((lab_cellxy_t){LAB_N-CELLS_TO_DRIVE-1, 0}).x;
  state_now.t = 0; // don't turn, go straight
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(state_now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  TEST_ASSERT_EQUAL(RET_SUCCESS, dsInit(dsGetCfgPlatformDefault()));
  memset(&ticks, 0, sizeof(ticks));

  timingLoopInitialize(&looping_ctx, SECONDS_TO_MICROSECONDS(TIME_STEP_S));
  uint32_t start_timestamp = 0;
  timingMeasureStart(&start_timestamp);
  do
  {
    ret = executorGoToTargetAndStop(target, state_now, TIME_STEP_S, &mot);
    motorSetSpeed(mot);
    timingLoopControl(&looping_ctx);

    motorGetTicks(&ticks);
    dsGetDistances(measurements, ARRAY_SIZE(measurements));
    stateStep(TIME_STEP_S, &state_now, measurements, dsGetCfgPlatformDefault().numberOfSensors, ticks);
    statePlatformNotify(state_now);
    time = MICROSECONDS_TO_SECONDS(timingMeasureElapsedUs(start_timestamp));
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, state_now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, state_now.y);
  TEST_ASSERT_EQUAL(0, looping_ctx.external.errors_number);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, state_now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, state_now.y);
  }
}

TEST(stateBasedOnEncodersAndDistanceSensors, When_DriveBackwardCell_Then_RotateAndGo)
{
  const double MAX_TIME_SECONDS = 3;
  const int CELLS_TO_DRIVE = 1;
  double time = 0;

  labResetToDefault();
  labLoad(labirynths[LABIRYNTHS_TEST_STRAIGHT_NS]);
  labirynthSetup(LABIRYNTHS_TEST_STRAIGHT_NS);

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&state_now));
  target.x = state_now.x;
  target.y = state_now.y;
  state_now.y = target.y + CELLS_TO_DRIVE*LAB_DIM_CELL;
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(state_now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  TEST_ASSERT_EQUAL(RET_SUCCESS, dsInit(dsGetCfgPlatformDefault()));
  memset(&ticks, 0, sizeof(ticks));

  timingLoopInitialize(&looping_ctx, SECONDS_TO_MICROSECONDS(TIME_STEP_S));
  do
  {
    ret = executorGoToTargetAndStop(target, state_now, TIME_STEP_S, &mot);
    motorSetSpeed(mot);
    timingLoopControl(&looping_ctx);

    motorGetTicks(&ticks);
    dsGetDistances(measurements, ARRAY_SIZE(measurements));
    stateStep(TIME_STEP_S, &state_now, measurements, dsGetCfgPlatformDefault().numberOfSensors, ticks);
    statePlatformNotify(state_now);
    time += TIME_STEP_S;
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, state_now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, state_now.y);
  TEST_ASSERT_EQUAL(0, looping_ctx.external.errors_number);


  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, state_now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, state_now.y);
  }
}

TEST(stateBasedOnEncodersAndDistanceSensors, When_DriveLeftOneCell_Then_RotateLeftAndGo)
{
  const double MAX_TIME_SECONDS = 3;
  double time = 0;

  labResetToDefault();
  labLoad(labirynths[LABIRYNTHS_TEST_STRAIGHT_NS]);
  labirynthSetup(LABIRYNTHS_TEST_STRAIGHT_NS);

  target = labCellToPoint((lab_cellxy_t){0, 1});

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateInitialize(&state_now));
  state_now.t = 0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, statePlatformSet(state_now));
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
  TEST_ASSERT_EQUAL(RET_SUCCESS, dsInit(dsGetCfgPlatformDefault()));
  memset(&ticks, 0, sizeof(ticks));

  timingLoopInitialize(&looping_ctx, SECONDS_TO_MICROSECONDS(TIME_STEP_S));
  uint32_t start_timestamp = 0;
  timingMeasureStart(&start_timestamp);
  do
  {
    ret = executorGoToTargetAndStop(target, state_now, TIME_STEP_S, &mot);
    motorSetSpeed(mot);
    timingLoopControl(&looping_ctx);

    motorGetTicks(&ticks);
    dsGetDistances(measurements, ARRAY_SIZE(measurements));
    stateStep(TIME_STEP_S, &state_now, measurements, dsGetCfgPlatformDefault().numberOfSensors, ticks);
    statePlatformNotify(state_now);
    time = MICROSECONDS_TO_SECONDS(timingMeasureElapsedUs(start_timestamp));
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));

  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(MAX_TIME_SECONDS, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, state_now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, state_now.y);
  TEST_ASSERT_EQUAL(0, looping_ctx.external.errors_number);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, state_now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, state_now.y);
  }
}

TEST_GROUP_RUNNER(stateBasedOnMotorEncoder)
{
  RUN_TEST_CASE(stateBasedOnMotorEncoder, When_DriveForwardAbit_Then_AchieveTarget);
  RUN_TEST_CASE(stateBasedOnMotorEncoder, When_DriveBackwardCell_Then_RotateAndGo);
  RUN_TEST_CASE(stateBasedOnMotorEncoder, When_DriveLeftOneCell_Then_RotateLeftAndGo);
}

TEST_GROUP_RUNNER(stateBasedOnEncodersAndDistanceSensors)
{
  RUN_TEST_CASE(stateBasedOnEncodersAndDistanceSensors, Given_NoWallsAtAll_When_DriveForward5cells_Then_DoIt);
  RUN_TEST_CASE(stateBasedOnEncodersAndDistanceSensors, Given_WallsKnownOnLeftAndRight_When_DriveForward5cellsN_Then_DoIt);
  RUN_TEST_CASE(stateBasedOnEncodersAndDistanceSensors, Given_WallsKnownOnLeftAndRight_When_DriveForwardEtoTheEndOfCorridor_Then_DoIt);
  RUN_TEST_CASE(stateBasedOnEncodersAndDistanceSensors, When_DriveBackwardCell_Then_RotateAndGo);
  RUN_TEST_CASE(stateBasedOnEncodersAndDistanceSensors, When_DriveLeftOneCell_Then_RotateLeftAndGo);
}

int main(void)
{
  UNITY_BEGIN();

  RUN_TEST_GROUP(stateBasedOnMotorEncoder);
  RUN_TEST_GROUP(stateBasedOnEncodersAndDistanceSensors);

  int failures = UnityEnd();
  exhibitFailures(failures);

  return failures;
}

