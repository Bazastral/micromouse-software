
#include <string.h>

#include "unity.h"
#include "unity_fixture.h"

#include "mission.h"
#include "useful.h"
#include "logger.h"
#include "state.h"
#include "platform_state.h"
#include "platform_base.h"
#include "labirynth.h"
#include "labirynth_alter.h"
#include "labirynths.h"
#include "distance_sensors_hw.h"
#include "platform_labirynth.h"
#include "exhibit.h"
#include "timing.h"

const double OFFSET_ALLOWED_IN_STATE_POSITION_MM = 25;

state_t sim_state;
motors_speed_t mot;
const double TIME_STEP_S = 0.01;

void setUpCommon(void)
{
  platformInit();
  labTotalReset();
  stateInitModule(configGet());
  dsRegisterNoiseFunction(NULL);
  log_set_verbosity_level(LOGGER_LVL_INFO);
}

void tearDownCommon()
{
  motorSetSpeed((motors_speed_t){0, 0});
  platformDelayMs(1000);
}


TEST_GROUP(labirynthIsKnown);
TEST_SETUP(labirynthIsKnown) { setUpCommon(); }
TEST_TEAR_DOWN(labirynthIsKnown) { tearDownCommon(); }

static void TestDriving(
  state_t initial_state,
  lab_cellxy_t target_cell,
  double timeout_seconds
)
{
  double time = 0;
  ret_code_t ret = RET_AGAIN;

  missionInit(initial_state, target_cell);

  uint32_t start_timestamp = 0;
  timingMeasureStart(&start_timestamp);
  do
  {
    log_debug("---------------------------------------\n\n");
    ret = missionCompetitionMainThread();
    time = MICROSECONDS_TO_SECONDS(timingMeasureElapsedUs(start_timestamp));
  } while((ret == RET_AGAIN) && (time < timeout_seconds));

  point_t target = labCellToPoint(target_cell);
  TEST_ASSERT_LESS_THAN_DOUBLE_MESSAGE(timeout_seconds, time, "max time achieved,"
      "did not arrive at aim destination");
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

  state_t state_now = missionGetCurrentMouseState();
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.x, state_now.x);
  TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, target.y, state_now.y);

  if(statePlatformGet(&sim_state) == RET_SUCCESS) //only on simulator
  {
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.x, state_now.x);
    TEST_ASSERT_DOUBLE_WITHIN(OFFSET_ALLOWED_IN_STATE_POSITION_MM, sim_state.y, state_now.y);
  }

}

TEST(labirynthIsKnown, DriveOneCorner)
{
  const double MAX_TIME_SECONDS = 6;
  lab_cellxy_t beginning_cell = {15, 1};
  state_t state_initial = {
    .x = labCellToPoint(beginning_cell).x,
    .y = labCellToPoint(beginning_cell).y,
    .t = -M_PI_2,
    .v = 0,
    .w = 0,
  };
  lab_cellxy_t end_cell = {14, 0};

  labLoad(labirynths[LABIRYNTHS_A]);
  labirynthSetup(LABIRYNTHS_A);

  TestDriving(state_initial, end_cell, MAX_TIME_SECONDS);
}

TEST(labirynthIsKnown, When_DriveStraightDifferentDirection_Then_FirstlyRotate)
{
  const double MAX_TIME_SECONDS = 10;
  lab_cellxy_t beginning_cell = {7, 13};
  state_t state_initial = {
    .x = labCellToPoint(beginning_cell).x,
    .y = labCellToPoint(beginning_cell).y,
    .t = 0,
    .v = 0,
    .w = 0,
  };
  lab_cellxy_t end_cell = {6, 13};

  labLoad(labirynths[LABIRYNTHS_A]);
  labirynthSetup(LABIRYNTHS_A);

  TestDriving(state_initial, end_cell, MAX_TIME_SECONDS);
}

TEST(labirynthIsKnown, When_DriveStraightWithOnlyOneWall_Then_DoIt)
{
  const double MAX_TIME_SECONDS = 10;
  lab_cellxy_t beginning_cell = {15, 15};
  state_t state_initial = {
    .x = labCellToPoint(beginning_cell).x,
    .y = labCellToPoint(beginning_cell).y,
    .t = -M_PI,
    .v = 0,
    .w = 0,
  };
  lab_cellxy_t end_cell = {10, 15};

  labLoad(labirynths[LABIRYNTHS_ARENA]);
  labirynthSetup(LABIRYNTHS_ARENA);

  TestDriving(state_initial, end_cell, MAX_TIME_SECONDS);
}


TEST(labirynthIsKnown, When_DriveStraightWithOnlyOneWallAndSecondPerpendicular_Then_DoIt)
{
  const double MAX_TIME_SECONDS = 10;
  lab_cellxy_t beginning_cell = {10, 15};
  state_t state_initial = {
    .x = labCellToPoint(beginning_cell).x,
    .y = labCellToPoint(beginning_cell).y,
    .t = 0,
    .v = 0,
    .w = 0,
  };
  lab_cellxy_t end_cell = {15, 15};

  labLoad(labirynths[LABIRYNTHS_TEST_STRAIGHT_NS]);
  labirynthSetup(LABIRYNTHS_TEST_STRAIGHT_NS);

  TestDriving(state_initial, end_cell, MAX_TIME_SECONDS);
}


TEST(labirynthIsKnown, When_FromStartingPointToTarget_Then_DoIt)
{
  TEST_IGNORE_MESSAGE("this test is a simplified LearnItAndAchieveGoal, so skip it in CI");
  const double MAX_TIME_SECONDS = 120;
  state_t state_initial = stateStartingPoint();
  lab_cellxy_t end_cell = labCoordsGoalNE();

  labLoad(labirynths[LABIRYNTHS_A]);
  labirynthSetup(LABIRYNTHS_A);

  TestDriving(state_initial, end_cell, MAX_TIME_SECONDS);
}

TEST_GROUP(labirynthIsUndiscovered);
TEST_SETUP(labirynthIsUndiscovered) { setUpCommon(); }
TEST_TEAR_DOWN(labirynthIsUndiscovered) { tearDownCommon(); }

TEST(labirynthIsUndiscovered, LearnItAndAchieveGoal)
{
  const double MAX_TIME_SECONDS = 300;
  state_t state_initial = stateStartingPoint();
  lab_cellxy_t end_cell = labCoordsGoalNE();

  labResetToDefault();
  labirynthSetup(LABIRYNTHS_ALLJAPAN_001_1980);

  TestDriving(state_initial, end_cell, MAX_TIME_SECONDS);
}
double noiseFunctionConstantOffset(double measurement)
{
  const double OFFSET_MM = 10;
  return measurement + OFFSET_MM;
}

TEST(labirynthIsUndiscovered, map5x5_distance_sensors_with_error_constatnt_offset)
{
  const double MAX_TIME_SECONDS = 3000;
  state_t state_initial = stateStartingPoint();
  lab_cellxy_t end_cell = (lab_cellxy_t){4u, 4u};

  labResetToDefault();
  platformInit();
  labirynthSetup(LABIRYNTHS_5_TWO_ROUTES);
  dsRegisterNoiseFunction(noiseFunctionConstantOffset);

  TestDriving(state_initial, end_cell, MAX_TIME_SECONDS);
}


TEST_GROUP(wholeCompetitionFlow);
TEST_SETUP(wholeCompetitionFlow) { setUpCommon(); }
TEST_TEAR_DOWN(wholeCompetitionFlow) { tearDownCommon(); }

TEST(wholeCompetitionFlow, map5x5)
{
  ret_code_t ret;
  platformInit();
  missionInit(stateStartingPoint(), (lab_cellxy_t){4u, 4u});
  labirynthSetup(LABIRYNTHS_5_TWO_ROUTES);

  int success_returns = 0;
  while (true)
  {
    ret = missionCompetitionMainThread();
    if(ret == RET_AGAIN)
    {
      continue;
    }
    else if(ret == RET_SUCCESS)
    {
      success_returns++;
      if(success_returns >= 5)
      {
        TEST_PASS_MESSAGE("all three states done!");
      }
      platformInit();
      labirynthSetup(LABIRYNTHS_5_TWO_ROUTES);
      platformDelayMs(10000);
    }
    else
    {
      TEST_ABORT();
    }
  }

}

TEST_GROUP_RUNNER(labirynthIsKnown)
{
  RUN_TEST_CASE(labirynthIsKnown, DriveOneCorner);
  RUN_TEST_CASE(labirynthIsKnown, When_DriveStraightDifferentDirection_Then_FirstlyRotate);
  RUN_TEST_CASE(labirynthIsKnown, When_DriveStraightWithOnlyOneWall_Then_DoIt);
  RUN_TEST_CASE(labirynthIsKnown, When_DriveStraightWithOnlyOneWallAndSecondPerpendicular_Then_DoIt);
  RUN_TEST_CASE(labirynthIsKnown, When_FromStartingPointToTarget_Then_DoIt);
}

TEST_GROUP_RUNNER(labirynthIsUndiscovered)
{
  RUN_TEST_CASE(labirynthIsUndiscovered, LearnItAndAchieveGoal);
  RUN_TEST_CASE(labirynthIsUndiscovered, map5x5_distance_sensors_with_error_constatnt_offset);
}

TEST_GROUP_RUNNER(wholeCompetitionFlow)
{
  RUN_TEST_CASE(wholeCompetitionFlow, map5x5);
}


int main(void)
{
  UNITY_BEGIN();

  RUN_TEST_GROUP(labirynthIsKnown);
  RUN_TEST_GROUP(labirynthIsUndiscovered);
  RUN_TEST_GROUP(wholeCompetitionFlow);

  int failures = UnityEnd();
  exhibitFailures(failures);

  return failures;
}

