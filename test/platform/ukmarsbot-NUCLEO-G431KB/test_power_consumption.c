
#include <stdbool.h>
#include <string.h>

#include "unity.h"
#include "unity_fixture.h"

#include "config.h"
#include "distance_sensors.h"
#include "distance_sensors_hw.h"
#include "led.h"
#include "main.h"
#include "platform_base.h"


void SetUp(void)
{
  // turn off everything
  platformSetLedState(0, LED_OFF);
  platformSetLedState(1, LED_OFF);
  platformSetLedState(2, LED_OFF);
  motorSetSpeed((motors_speed_t){0, 0});
  HAL_GPIO_WritePin(LED_SENSOR_DIST_GPIO_Port, LED_SENSOR_DIST_Pin, GPIO_PIN_RESET);
}

void test_current_nothing(void)
{

}

void test_current_leds(void)
{
  platformSetLedState(0, LED_ON);
  platformSetLedState(1, LED_ON);
  platformSetLedState(2, LED_ON);
}

void test_current_motors(void)
{
  motorSetSpeed((motors_speed_t){1, 1});
}

void test_current_distance_senosrs(void)
{
  HAL_GPIO_WritePin(LED_SENSOR_DIST_GPIO_Port, LED_SENSOR_DIST_Pin, GPIO_PIN_SET);
}

void test_current_all(void)
{
  platformSetLedState(0, LED_ON);
  platformSetLedState(1, LED_ON);
  platformSetLedState(2, LED_ON);
  HAL_GPIO_WritePin(LED_SENSOR_DIST_GPIO_Port, LED_SENSOR_DIST_Pin, GPIO_PIN_SET);
  motorSetSpeed((motors_speed_t){1, 1});
}


int main(void)
{
  UNITY_BEGIN();

  platformInit();

  //here you can comment all lines except one and check the current needed manually
  // RUN_TEST(test_current_nothing);
  // RUN_TEST(test_current_leds);
  // RUN_TEST(test_current_motors);
  // RUN_TEST(test_current_distance_senosrs);
  RUN_TEST(test_current_all);

  while(1){
    if(platformCheckIfBatteryIsEmpty() == true)
    {
      break;
    }
  }

  int failures = UnityEnd();

  return failures;
}






