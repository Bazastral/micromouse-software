
#include <stdbool.h>
#include <string.h>

#include "unity.h"
#include "unity_fixture.h"

#include "config.h"
#include "distance_sensors.h"
#include "distance_sensors_hw.h"
#include "led.h"
#include "main.h"
#include "platform_base.h"
#include "exhibit.h"
#include "timing.h"

// copied from distance_sensors_hw.c
ret_code_t getSensorReadings(uint16_t buf[], uint8_t numberOfSamples);
void TurnIrOn(void);
void TurnIrOff(void);

void SetUp(void)
{
}

// max is 2^12-1 = 4095, so u16 is enough
uint16_t ReadingsPrev[DISTANCE_SENSORS_NUMBER];
uint16_t ReadingsCurrent[DISTANCE_SENSORS_NUMBER];

uint16_t ReadingsIrOn[DISTANCE_SENSORS_NUMBER];
uint16_t ReadingsIrOff[DISTANCE_SENSORS_NUMBER];

void measure_reading_stabilization_time(void)
{
  ret_code_t ret = RET_SUCCESS;
  uint32_t elapsed_us_on = 0;
  uint32_t elapsed_us_off = 0;
  const int SELECT_SENSOR = 1; // 0, 1 or 2
  const int SOME_MANUAL_THRESHOLD = 40; //it is 1.0% difference
  int reps_on = 0;
  int reps_off = 0;

  while (ret == RET_SUCCESS)
  {
    reps_on = 0;
    reps_off = 0;
    log_info("");

    /*******************************************************************************************/
    uint32_t start_timestamp = 0;
    timingMeasureStart(&start_timestamp);

    ret = getSensorReadings(ReadingsCurrent, DISTANCE_SENSORS_NUMBER); if(ret != RET_SUCCESS) {break;}
    do{
      ReadingsPrev[0] = ReadingsCurrent[0];
      ReadingsPrev[1] = ReadingsCurrent[1];
      ReadingsPrev[2] = ReadingsCurrent[2];
      ret = getSensorReadings(ReadingsCurrent, DISTANCE_SENSORS_NUMBER); if(ret != RET_SUCCESS) {break;}
      reps_off++;
    } while(abs((int)ReadingsPrev[SELECT_SENSOR] - (int)ReadingsCurrent[SELECT_SENSOR]) > SOME_MANUAL_THRESHOLD);
    ReadingsIrOff[0] = ReadingsCurrent[0];
    ReadingsIrOff[1] = ReadingsCurrent[1];
    ReadingsIrOff[2] = ReadingsCurrent[2];

    elapsed_us_off = timingMeasureElapsedUs(start_timestamp);
    /*******************************************************************************************/
    timingMeasureStart(&start_timestamp);
    TurnIrOn();

    ret = getSensorReadings(ReadingsCurrent, DISTANCE_SENSORS_NUMBER); if(ret != RET_SUCCESS) {break;}
    do{
      ReadingsPrev[0] = ReadingsCurrent[0];
      ReadingsPrev[1] = ReadingsCurrent[1];
      ReadingsPrev[2] = ReadingsCurrent[2];
      ret = getSensorReadings(ReadingsCurrent, DISTANCE_SENSORS_NUMBER); if(ret != RET_SUCCESS) {break;}
      reps_on++;
    } while(abs((int)ReadingsPrev[SELECT_SENSOR] - (int)ReadingsCurrent[SELECT_SENSOR]) > SOME_MANUAL_THRESHOLD);
    ReadingsIrOn[0] = ReadingsCurrent[0];
    ReadingsIrOn[1] = ReadingsCurrent[1];
    ReadingsIrOn[2] = ReadingsCurrent[2];

    TurnIrOff();
    elapsed_us_on = timingMeasureElapsedUs(start_timestamp);
    /*******************************************************************************************/

    log_info("Elapsed us: off=%4d (%2d reads) on=%4d (%2d reads)",
             (int)elapsed_us_off,
             (int)reps_off,
             (int)elapsed_us_on,
             (int)reps_on);
    for (int i = 0; i < 3; i++)
    {
      log_info("%d off=%4d on=%4d diff=%4d",
               (int)i,
               (int)ReadingsIrOff[i],
               (int)ReadingsIrOn[i],
               (int)ReadingsIrOn[i] - (int)ReadingsIrOff[i]);
    }
    platformDelayMs(200);

    if (platformCheckIfBatteryIsEmpty() == true)
    {
      break;
    }
  }
  TEST_ASSERT(ret == RET_SUCCESS);
}

void measure_in_loop(void)
{
  ret_code_t ret = RET_SUCCESS;

  while (ret == RET_SUCCESS)
  {
    log_info("");

    /*******************************************************************************************/

    ret = getSensorReadings(ReadingsCurrent, DISTANCE_SENSORS_NUMBER); if(ret != RET_SUCCESS) {break;}
    ReadingsIrOff[0] = ReadingsCurrent[0];
    ReadingsIrOff[1] = ReadingsCurrent[1];
    ReadingsIrOff[2] = ReadingsCurrent[2];

    /*******************************************************************************************/
    TurnIrOn();
    platformDelayUs(100);

    ret = getSensorReadings(ReadingsCurrent, DISTANCE_SENSORS_NUMBER); if(ret != RET_SUCCESS) {break;}
    ReadingsIrOn[0] = ReadingsCurrent[0];
    ReadingsIrOn[1] = ReadingsCurrent[1];
    ReadingsIrOn[2] = ReadingsCurrent[2];

    TurnIrOff();
    /*******************************************************************************************/
    platformDelayMs(10);

    double distances[3];
    ret = dsGetDistances(distances, 3);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
    for (int i = 0; i < 3; i++)
    {
      log_info("%d off=%4d on=%4d diff=%4d, dist %+6.2f",
               (int)i,
               (int)ReadingsIrOff[i],
               (int)ReadingsIrOn[i],
               (int)ReadingsIrOn[i] - (int)ReadingsIrOff[i],
               distances[i]);
    }

    platformDelayMs(400);

    if (platformCheckIfBatteryIsEmpty() == true)
    {
      break;
    }
  }
  TEST_ASSERT(ret == RET_SUCCESS);
}

int main(void)
{
  UNITY_BEGIN();

  platformInit();

  // RUN_TEST(measure_reading_stabilization_time);

  /**
   * @brief Results
  hadc2.Init.Resolution = ADC_RESOLUTION_10B;
  sampletime -> total measurements, time in us
  2  -> 11, 137
  6  -> 11, 139
  12 -> 11, 143
  24 -> 11, 139
  47 -> 9,  140
  92 -> 9,  170
  640-> 4 , 332
  Turns out that it is better to keep the sample time rather low
  After some more tests with 12B resolution decision is to
  firstly measure backgroud, then turn IR on and wait 100us
  (70us should be enough, but add something for margin), and measure again
  Then of course turn IR off and do other things
   *
   */
  RUN_TEST(measure_in_loop);

  int failures = UnityEnd();
  exhibitFailures(failures);

  return failures;
}






