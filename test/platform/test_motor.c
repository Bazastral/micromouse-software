


#include <string.h>
#include <stdlib.h>

#include "unity.h"

#include "motor.h"
#include "platform_state.h"
#include "platform_base.h"
#include "exhibit.h"


void setUp(void)
{
  state_t state;
  stateInitialize(&state);
  statePlatformSet(state);
}

void tearDown(void)
{
  // wait until motors slow down
  motorSetSpeed((motors_speed_t){0, 0});
  platformDelayMs(500);
}

void test_going_straight(void)
{
  motors_speed_t values;
  motors_ticks_t ticks;

  memset(&ticks, 0, sizeof(ticks));
  memset(&values, 0, sizeof(values));

  values.left = 1.0; values.right = 1.0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(values));
  platformDelayMs(1000);


  TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
  TEST_ASSERT_GREATER_OR_EQUAL_INT32(1, ticks.left);
  TEST_ASSERT_GREATER_OR_EQUAL_INT32(1, ticks.left);
}

void test_still(void)
{
  motors_speed_t values;
  motors_ticks_t ticks;

  // make sure that mouse stays
  values.left = 0.0; values.right = 0.0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(values));
  platformDelayMs(1000);
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));

}

void test_turning_CW(void)
{
  motors_speed_t values;
  motors_ticks_t ticks;

  // turn around
  values.left = 1.0; values.right = -1.0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(values));
  platformDelayMs(400);

  TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
  TEST_ASSERT_GREATER_OR_EQUAL_INT32(1, ticks.left);
  TEST_ASSERT_LESS_OR_EQUAL_INT32(-1, ticks.right);

  double precision_percent = 20;
  TEST_ASSERT_GREATER_OR_EQUAL_DOUBLE(((100-precision_percent)/100) * abs(ticks.right), ticks.left);
  TEST_ASSERT_LESS_OR_EQUAL_DOUBLE(((100+precision_percent)/100) * abs(ticks.right), ticks.left);
}

void test_turning_CCW(void)
{
  motors_speed_t values;
  motors_ticks_t ticks;

  values.left = -0.2; values.right = 0.2;
  TEST_ASSERT_EQUAL(RET_SUCCESS, motorSetSpeed(values));
  platformDelayMs(1000);

  TEST_ASSERT_EQUAL(RET_SUCCESS, motorGetTicks(&ticks));
  TEST_ASSERT_GREATER_OR_EQUAL_INT32(1, ticks.right);
  TEST_ASSERT_LESS_OR_EQUAL_INT32(-1, ticks.left);
}


int main(void)
{
  UNITY_BEGIN();
  platformInit();

  RUN_TEST(test_still);
  RUN_TEST(test_going_straight);
  RUN_TEST(test_turning_CW);
  RUN_TEST(test_turning_CCW);

  int failures = UnityEnd();
  exhibitFailures(failures);

  return failures;
}
