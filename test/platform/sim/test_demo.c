
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "unity.h"
#include "unity_fixture.h"

#include "simulator_broker.h"
#include "path_planner.h"
#include "state.h"
#include "platform_base.h"
#include "labirynths.h"
#include "labirynth.h"
#include "config.h"

#include "logger.h"
#include "printer.h"
#include "labirynths.h"
#include "platform_printer.h"
#include "labirynth_alter.h"
#include "platform_labirynth.h"
#include "mission.h"
#include "exhibit.h"
#include "timing.h"

static vector_t path;
char mem[1000];
static vector_t pathB;
char memB[1000];
lab_cellxy_t from, to;
ret_code_t ret;
lab_cellxy_t next_target;

// Used for small demo
static const bool VISUALIZE = true;

void pathplanner_show_A_maze(void)
{
  TEST_IGNORE_MESSAGE ("This is demo test, so do not run it in CI by default");
  memset(mem, 0, sizeof(mem));
  memset(memB, 0, sizeof(mem));
  from.x = rand();
  from.y = rand();
  to.x = rand();
  to.y = rand();
  vector_init(&path, mem, sizeof(mem), sizeof(lab_cellxy_t));
  vector_init(&pathB, memB, sizeof(memB), sizeof(lab_cellxy_t));
  // Use well known labirynth
  labLoad(labirynths[LABIRYNTHS_A]);
  labirynthSetup(LABIRYNTHS_A);
  pp_init();

  const uint16_t MAX_ITER = 1000u;
  lab_cellxy_t currentPos = labCoordsStartingPoint();
  const lab_cellxy_t GOAL = labCoordsGoalNE();

  // labirynth is not known
  labResetToDefault();

  for (uint16_t i = 0u; i < MAX_ITER; i++)
  {
    // plan where to go
    ret = pp_find_path(currentPos, GOAL, &path);
    if(ret != RET_SUCCESS && ret != RET_MAPPING_NEEDED)
    {
      TEST_ASSERT_MESSAGE(false, "Finding error");
    }

    if(VISUALIZE)
    {
      // print also full path from start to middle
      pp_find_path(labCoordsStartingPoint(), GOAL, &pathB);

      printLabWithTwoRoutes(path, pathB);
      printPathPlannerCells(path);
      sim_show_current_lab_knowledge();

      usleep(400*1000);
    }

    if(ret == RET_SUCCESS)
    {
      printf("break\n");
      break;
    }

    // move one tile ahead
    vector_at(path, 1u, &currentPos);
    // discover the cell
    labWallChange(currentPos, labirynths[LABIRYNTHS_A][currentPos.x][currentPos.y], true);
    labWallChange(currentPos, (~labirynths[LABIRYNTHS_A][currentPos.x][currentPos.y]), false);
  }

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
}

void mission_driving_unknown_maze(void)
{
  TEST_IGNORE_MESSAGE ("This is demo test, so do not run it in CI by default");

  platformInit();
  labTotalReset();
  stateInitModule(configGet());

  const double MAX_TIME_SECONDS = 500;
  state_t state_initial = stateStartingPoint();
  lab_cellxy_t end_cell = labCoordsGoalNE();
  double time = 0;
  ret = RET_AGAIN;

  labirynthSetup(LABIRYNTHS_HITEL);

  missionInit(state_initial, end_cell);
  uint32_t start_timestamp = 0;
  timingMeasureStart(&start_timestamp);

  do
  {
    log_debug("---------------------------------------\n\n");
    ret = missionCompetitionMainThread();
    time = MICROSECONDS_TO_SECONDS(timingMeasureElapsedUs(start_timestamp));
  } while((ret == RET_AGAIN) && (time < MAX_TIME_SECONDS));


  motorSetSpeed((motors_speed_t){0, 0});
  platformDelayMs(1000);
}

int main(void)
{
  UNITY_BEGIN();

  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
  RUN_TEST(pathplanner_show_A_maze);
  RUN_TEST(mission_driving_unknown_maze);

  int failures = UnityEnd();
  exhibitFailures(failures);

  return failures;
}


