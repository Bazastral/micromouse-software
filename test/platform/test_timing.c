
#include <stdbool.h>
#include <string.h>

#include "unity.h"

#include "platform_base.h"
#include "exhibit.h"
#include "logger.h"
#include "timing.h"

void setUp(void)
{
  platformInit();
  log_set_verbosity_level(LOGGER_LVL_DEBUG);
}
void tearDown(void)
{

}


int main(void)
{
  UNITY_BEGIN();

  // previous tests
  

  int failures = UnityEnd();
  exhibitFailures(failures);

  return failures;
}






