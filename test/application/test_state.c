

#include "unity.h"
#include "unity_fixture.h"

#include "state.h"
#include "position.h"
#include "platform_base.h"
#include "useful.h"
#include "distance_sensors.h"
#include "labirynths.h"
#include "distance_sensors_hw_mock.h"
#include "motor_mock.h"
#include "config.h"
#include "labirynth_alter.h"
#include "logger.h"


const double BIGGER = 1.01;
const double SMALLER = 0.99;
#define ALLOWED_DELTA_DEFAULT_POSITION 1
#define ALLOWED_DELTA_DEFAULT_ANGLE deg2rad(5)

#define TEST_ASSERT_STATE_WITHIN(expected, actual)  {  \
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(ALLOWED_DELTA_DEFAULT_POSITION, expected.x, actual.x, "x");     \
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(ALLOWED_DELTA_DEFAULT_POSITION, expected.y, actual.y, "y");     \
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(ALLOWED_DELTA_DEFAULT_ANGLE, expected.t, actual.t, "t");     \
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(ALLOWED_DELTA_DEFAULT_POSITION, expected.v, actual.v, "v");     \
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(ALLOWED_DELTA_DEFAULT_ANGLE, expected.w, actual.w, "w");     \
}

static config_t cfg;

static state_t st_now, st_expected, st_next;
static const dsConfig_t dsCfgZero = {
  .numberOfSensors = 0,
};
static const dsConfig_t dsCfgOneRight = {
  .numberOfSensors = 1,
  .sensors = {
    {.offset = {  0, 0, -M_PI/2 } }
    },
  .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
  .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
  .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS_ALLOW_ALL_READINGS
};
static const dsConfig_t dsCfgOneLeft = {
  .numberOfSensors = 1,
  .sensors = {
    {.offset = {  0, 0, M_PI/2 } }
    },
  .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
  .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
  .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS_ALLOW_ALL_READINGS
};
static const dsConfig_t dsCfgOneFront = {
  .numberOfSensors = 1,
  .sensors = {
    {.offset = {  0, 0, 0 } }
    },
  .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
  .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
  .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS_ALLOW_ALL_READINGS
};
static const dsConfig_t dsCfgOneRear = {
  .numberOfSensors = 1,
  .sensors = {
    {.offset = {  0, 0, M_PI } }
    },
  .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
  .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
  .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS_ALLOW_ALL_READINGS

};
static const dsConfig_t dsCfgTwoLeftRight = {
  .numberOfSensors = 2,
  .sensors = {
    {.offset = {  0, 0, M_PI_2 } },
    {.offset = {  0, 0, -M_PI_2 } }
    },
  .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
  .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
  .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS_ALLOW_ALL_READINGS
};
static const dsConfig_t dsCfgTwoLeftFront = {
  .numberOfSensors = 2,
  .sensors = {
    {.offset = {  0, 0, M_PI_2 } },
    {.offset = {  0, 0, 0 } }
    },
  .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
  .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
  .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS_ALLOW_ALL_READINGS
};
static const dsConfig_t dsCfgFive = {
  .numberOfSensors = 5,
  .sensors = {
    {.offset = {  0, 0, M_PI_2 } },
    {.offset = {  0, 0, M_PI_4 } },
    {.offset = {  0, 0, 0 } },
    {.offset = {  0, 0, -M_PI_4 } },
    {.offset = {  0, 0, -M_PI_2 } },
    },
  .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
  .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
  .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS_ALLOW_ALL_READINGS
};



void setUpCommon(void)
{
  stateInitModule(configGet());
  stateInitialize(&st_now);
  st_next = st_now;
  st_expected.x = rand();
  st_expected.y = rand();
  st_expected.t = rand();
  st_expected.v = rand();
  st_expected.w = rand();
  cfg = *configGet();
}

void tearDownCommon(void)
{
}

TEST_GROUP(stateNextModel_FUN);
TEST_SETUP(stateNextModel_FUN) { setUpCommon(); }
TEST_TEAR_DOWN(stateNextModel_FUN) { tearDownCommon(); }

TEST(stateNextModel_FUN, When_VelocitiesZero_Then_StayStill)
{
  double time = 1;

  // nothing happens, should stay still
  st_now.x = st_now.y = st_now.t = 0;
  st_now.v = 0;
  st_now.w = 0;
  st_expected = st_now;
  TEST_ASSERT_EQUAL(RET_SUCCESS,
      stateNextModel(time, st_now, &st_next));
  TEST_ASSERT_STATE_WITHIN(st_expected, st_next);

  // nothing happens, should stay still but in other position
  st_now.x = 1.1;
  st_now.y = 2.2;
  st_now.t = 0.3;
  st_now.v = 0;
  st_now.w = 0;
  st_expected = st_now;
  TEST_ASSERT_EQUAL(RET_SUCCESS,
      stateNextModel(time, st_now, &st_next));
  TEST_ASSERT_STATE_WITHIN(st_expected, st_next);
}

TEST(stateNextModel_FUN, When_DrivesStraight_Then_UpdateOnlyXY)
{
  double time = 1;

  // drive forward without turning to right
  st_now.x = st_now.y = st_now.t = 0;
  st_now.v = 1;
  st_expected = st_now;
  st_expected.x += 1;
  TEST_ASSERT_EQUAL(RET_SUCCESS,
      stateNextModel(time, st_now, &st_next));
  TEST_ASSERT_STATE_WITHIN(st_expected, st_next);

  // drive forward without turning to top
  st_now.x = st_now.y = st_now.t = 0;
  st_now.t = M_PI/2;
  st_now.v = 1;
  st_expected = st_now;
  st_expected.y += 1;
  TEST_ASSERT_EQUAL(RET_SUCCESS,
      stateNextModel(time, st_now, &st_next));
  TEST_ASSERT_STATE_WITHIN(st_expected, st_next);

  // drive forward, down left.
  st_now.x = st_now.y = st_now.t = 0;
  st_now.t = -3*M_PI/4;
  st_now.v = 1;
  st_expected = st_now;
  st_expected.x -= 1/sqrt(2);
  st_expected.y -= 1/sqrt(2);
  TEST_ASSERT_EQUAL(RET_SUCCESS,
      stateNextModel(time, st_now, &st_next));
  TEST_ASSERT_STATE_WITHIN(st_expected, st_next);
}


TEST_GROUP(stateNextEncoders_FUN);
TEST_SETUP(stateNextEncoders_FUN) { setUpCommon(); }
TEST_TEAR_DOWN(stateNextEncoders_FUN) { tearDownCommon(); }

TEST(stateNextEncoders_FUN, When_NoTicks_Then_DoNotMove)
{
  motors_ticks_t ticks;
  double time;

  st_now.x = st_now.y = st_now.t = 0;
  st_now.v = 0;
  st_now.w = 0;
  time = 1;
  ticks.left = ticks.right = 0;
  st_expected = st_now;
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time, st_now, &st_next, ticks));
  TEST_ASSERT_STATE_WITHIN(st_expected, st_next);
}


TEST(stateNextEncoders_FUN, Given_Horizontal_When_EncodersEqual_Then_GoStraight)
{
  motors_ticks_t ticks;
  double time;

  // go staight, both wheels
  st_now.x = st_now.y = st_now.t = st_now.v = st_now.w = 0;
  ticks.left = ticks.right = cfg.whlTicksPerTurn;
  st_expected = st_now;
  time = 2;
  st_expected.x = cfg.whlCirc;
  st_expected.v = st_expected.x/time;
  TEST_ASSERT_EQUAL(RET_SUCCESS,
      stateNextEncoders(time,st_now, &st_next, ticks));
  TEST_ASSERT_STATE_WITHIN(st_expected, st_next);
}

TEST(stateNextEncoders_FUN, Given_SomeAngle_When_EncodersEqualAndNegative_Then_GoBackwardsStraightInAngle)
{
  motors_ticks_t ticks;
  double time;

  // go staight, both wheels, but in reverse direction, and at some angle
  st_now.x = st_now.y = st_now.t = st_now.v = st_now.w = 0;
  st_now.t = M_PI/4;
  ticks.left = ticks.right = -2*cfg.whlTicksPerTurn;
  st_expected = st_now;
  time = 2;
  st_expected.x = -2*cfg.whlCirc * sqrt(2)/2;
  st_expected.y = -2*cfg.whlCirc * sqrt(2)/2;
  st_expected.v = -sqrt(pow(st_expected.x, 2) + pow(st_expected.y, 2))/time;
  TEST_ASSERT_EQUAL(RET_SUCCESS,
      stateNextEncoders(time,st_now, &st_next, ticks));
  TEST_ASSERT_STATE_WITHIN(st_expected, st_next);
}

TEST(stateNextEncoders_FUN, Given_SomeAngleAndWeirdPoint_When_EncodersEqual_Then_GoBackwardsStraightInAngle)
{
  motors_ticks_t ticks;
  double time;

  st_now.x = 1.2;
  st_now.y = 2.0;
  st_now.v = 1.0;
  st_now.w = 0;
  st_now.t = -M_PI/4;
  ticks.left = ticks.right = cfg.whlTicksPerTurn;
  time = 2;
  st_expected = st_now;
  st_expected.x = st_now.x+cfg.whlCirc * sqrt(2)/2;
  st_expected.y = st_now.y-cfg.whlCirc * sqrt(2)/2;
  st_expected.v = cfg.whlCirc/time;
  TEST_ASSERT_EQUAL(RET_SUCCESS,
      stateNextEncoders(time,st_now, &st_next, ticks));
  TEST_ASSERT_STATE_WITHIN(st_expected, st_next);
}

TEST(stateNextEncoders_FUN, When_RotateAroundLeftWheel90degs_Then_Rotate)
{
  motors_ticks_t ticks;
  double time = 1;

  st_now.x = st_now.y = st_now.t = st_now.v = st_now.w = 0;
  // poining in OY direction
  st_now.t = deg2rad(90);
  double rotation_rads = deg2rad(90);

  // robot made 90' rotation around left wheel
  double right_wheel_distance = cfg.whlTrack * rotation_rads;
  double right_wheel_rotations = right_wheel_distance / cfg.whlCirc;
  ticks.right = right_wheel_rotations * cfg.whlTicksPerTurn;
  ticks.left = 0;

  // 90' means that middle of the robot will move half of the 'track'
  st_expected.x = -cfg.whlTrack/2;
  st_expected.y = cfg.whlTrack/2;
  st_expected.t = st_now.t+rotation_rads;
  // TODO
  // for now do not test velocities, only position
  // st_expected.v = st_expected.x/time;
  // st_expected.w = alpha/time;
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time,st_now, &st_next, ticks));
  stateDump("now", st_now);
  stateDump("next", st_next);
  stateDump("expected", st_expected);
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.01, st_expected.x, st_next.x, "x");
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.01, st_expected.y, st_next.y, "y");
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.1, st_expected.t, st_next.t, "t");
}

TEST(stateNextEncoders_FUN, Given_RightWheelOn0_When_RotateAroundRightWheel180degs_Then_RotateProperly)
{
  motors_ticks_t ticks;
  double time = 1;

  // poining OX direction
  st_now.x = st_now.y = st_now.t = st_now.v = st_now.w = 0;
  // wheel right is on (0, 0)
  st_now.y = cfg.whlTrack/2;
  double rotation_rads = deg2rad(180);

  // robot made 180' rotation around right wheel
  double left_wheel_distance = cfg.whlTrack * rotation_rads;
  double left_wheel_rotations = left_wheel_distance / cfg.whlCirc;
  ticks.left = left_wheel_rotations * cfg.whlTicksPerTurn;
  ticks.right = 0;

  st_expected.x = 0;
  st_expected.y = -cfg.whlTrack/2;
  st_expected.t = -rotation_rads;
  // TODO
  // for now do not test velocities, only position
  // st_expected.v = st_expected.x/time;
  // st_expected.w = alpha/time;
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time,st_now, &st_next, ticks));
  stateDump("now", st_now);
  stateDump("next", st_next);
  stateDump("expected", st_expected);
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.01, st_expected.x, st_next.x, "x");
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.01, st_expected.y, st_next.y, "y");
  //must add PI, so that theta is around 0, and will not jump from -PI to +PI
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.1, normalizeRad(st_expected.t + M_PI), normalizeRad(st_next.t+M_PI), "t");
}

TEST(stateNextEncoders_FUN, When_LeftWheelDrivesTwiceMoreThenRight_Then_RotateProperly)
{
  motors_ticks_t ticks;
  double time = 1;

  // pointing OX direction
  st_now.x = st_now.y = st_now.t = st_now.v = st_now.w = 0;

  double rotation_rads = deg2rad(90);
  // must be 2, as the radius that the left wheel is making is 2 * track
  double left_wheel_distance = 2*cfg.whlTrack * rotation_rads;
  double left_wheel_rotations = left_wheel_distance / cfg.whlCirc;
  ticks.left = left_wheel_rotations * cfg.whlTicksPerTurn;
  ticks.right = ticks.left/2;

  st_expected.x = 1.5 * cfg.whlTrack;
  st_expected.y = -1.5 * cfg.whlTrack;
  st_expected.t = -rotation_rads;
  // TODO
  // for now do not test velocities, only position
  // st_expected.v = st_expected.x/time;
  // st_expected.w = alpha/time;
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextEncoders(time,st_now, &st_next, ticks));
  stateDump("now", st_now);
  stateDump("next", st_next);
  stateDump("expected", st_expected);
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.01, st_expected.x, st_next.x, "x");
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.01, st_expected.y, st_next.y, "y");
  TEST_ASSERT_DOUBLE_WITHIN_MESSAGE(0.1, st_expected.t, st_next.t, "t");
}

TEST_GROUP(stateNextDistSensors_Zero);
TEST_SETUP(stateNextDistSensors_Zero) { setUpCommon(); }
TEST_TEAR_DOWN(stateNextDistSensors_Zero) { tearDownCommon(); }

TEST(stateNextDistSensors_Zero, When_NoSensor_Then_ReturnFailure)
{
  dsInit(dsCfgZero);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF };
  TEST_ASSERT_EQUAL(RET_ERR_OTHER, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
}

TEST_GROUP(stateNextDistSensors_One);
TEST_SETUP(stateNextDistSensors_One) {
  setUpCommon();
  labLoad(labirynths[LABIRYNTHS_ALL_WALLS]);
}
TEST_TEAR_DOWN(stateNextDistSensors_One) { tearDownCommon(); }

TEST(stateNextDistSensors_One, Given_ValidAngle5deg_When_MouseIsRotatedTooMuch_Then_ReturnFailure)
{
  dsConfig_t dsCfgOne = dsCfgOneRight;
  dsCfgOne.allowed_measurement_angle_deg = 5;
  dsInit(dsCfgOne);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF };
  st_now.t = deg2rad(5.1);
  TEST_ASSERT_EQUAL(RET_ERR_OUT_OF_BOUNDS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  st_now.t = deg2rad(-5.1);
  TEST_ASSERT_EQUAL(RET_ERR_OUT_OF_BOUNDS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
}
TEST(stateNextDistSensors_One, Given_ValidAngle5deg_When_MouseIsRotatedLess_Then_ReturnSuccess)
{
  dsConfig_t dsCfgOne = dsCfgOneRight;
  dsCfgOne.allowed_measurement_angle_deg = 5;
  dsInit(dsCfgOne);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF };
  st_now.t = deg2rad(4.9);
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  st_now.t = deg2rad(-4.9);
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
}
TEST(stateNextDistSensors_One, Given_ValidAngle0deg_When_MouseIsRotatedLess_Then_ReturnSuccess)
{
  dsConfig_t dsCfgOne = dsCfgOneRight;
  dsCfgOne.allowed_measurement_angle_deg = 0;
  dsInit(dsCfgOne);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF };
  st_now.t = 0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
}
TEST(stateNextDistSensors_One, Given_ValidAngle45deg_When_MouseIsRotatedMore_Then_ReturnSuccess)
{
  dsConfig_t dsCfgOne = dsCfgOneRight;
  dsCfgOne.allowed_measurement_angle_deg = 45;
  dsInit(dsCfgOne);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF };
  st_now.t =  normalizeRad(((double)rand())/100);
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
}

TEST(stateNextDistSensors_One, Given_StartPosAndRight_When_MeasureSmaller_Then_MoveToRight)
{
  dsInit(dsCfgOneRight);
  // the measurement should be half of the corridor, but we want to move the robot to the
  // right, so the measurement is smaller (closer to wall)
  double measurements[] = {
    LAB_DIM_CORRIDOR_HALF - LAB_DIM_WALL
  };
  TEST_ASSERT_EQUAL(RET_SUCCESS,
      stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));

  stateDump("now", st_now);
  stateDump("next", st_next);

  TEST_ASSERT_GREATER_THAN_DOUBLE(st_now.x, st_next.x);
  TEST_ASSERT_LESS_THAN_DOUBLE(LAB_DIM_CELL, st_next.x);
}

TEST(stateNextDistSensors_One, Given_StartPosAndRight_When_MeasureBigger_Then_MoveToLeft)
{
  dsInit(dsCfgOneRight);
  // the measurement should be half of the corridor,
  // but we want to move the robot to the left,
  // so the measurement is bigger (far from the East wall)
  double measurements[] = {
    LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL
  };
  TEST_ASSERT_EQUAL(RET_SUCCESS,
      stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));

  stateDump("now", st_now);
  stateDump("next", st_next);

  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.x, st_next.x);
  TEST_ASSERT_GREATER_THAN_DOUBLE(LAB_DIM_WALL, st_next.x);
}

TEST(stateNextDistSensors_One, Given_StartPosAndRight_When_MeasureTheSame_Then_DoNotMove)
{
  dsInit(dsCfgOneRight);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF };
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_STATE_WITHIN(st_now, st_next);
}

TEST(stateNextDistSensors_One, Given_StartPosAndLeft_When_MeasureBigger_Then_MoveToRight)
{
  dsInit(dsCfgOneLeft);
  double measurements[] = {
    LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL
  };
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_GREATER_THAN_DOUBLE(st_now.x, st_next.x);
  TEST_ASSERT_LESS_THAN_DOUBLE(LAB_DIM_CELL, st_next.x);
}

TEST(stateNextDistSensors_One, Given_StartPosAndLeft_When_MeasureTheSame_Then_DoNotMove)
{
  dsInit(dsCfgOneLeft);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF };
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_STATE_WITHIN(st_now, st_next);
}

TEST(stateNextDistSensors_One, Given_StartPosAndFront_When_MeasureTheSame_Then_DoNotMove)
{
  dsInit(dsCfgOneFront);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF };

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));

  TEST_ASSERT_STATE_WITHIN(st_now, st_next);
}

TEST(stateNextDistSensors_One, Given_StartPosAndFront_When_MeasureBigger_Then_MoveDown)
{
  dsInit(dsCfgOneFront);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL};
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.y, st_next.y);
  TEST_ASSERT_GREATER_THAN_DOUBLE(LAB_DIM_WALL, st_next.y);
}

TEST(stateNextDistSensors_One, Given_FrontAndUnderAngle_When_MeasureTheSame_Then_DoNotMove)
{
  dsInit(dsCfgOneFront);
  st_now.x = LAB_DIM_CELL*3;
  st_now.y = LAB_DIM_CELL*2 - LAB_DIM_CORRIDOR_HALF;
  st_now.t = deg2rad(-135);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF*sqrt(2)};

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_STATE_WITHIN(st_now, st_next);
}

TEST(stateNextDistSensors_One, Given_FrontAndUnderAngleNegative_When_MeasureSmaller_Then_RotateLeft)
{
  dsInit(dsCfgOneFront);
  double angle_rad = deg2rad(-20);
  // in the starting cell, pointing to east wall under angle
  stateInitialize(&st_now);
  st_now.t = angle_rad;
  double measurements[] = { LAB_DIM_CORRIDOR_HALF/cos(angle_rad) * SMALLER };

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_LESS_THAN_DOUBLE(0, st_next.t);
  TEST_ASSERT_GREATER_THAN_DOUBLE(st_now.t, st_next.t);
}

TEST(stateNextDistSensors_One, Given_FrontAndUnderAnglePositive_When_MeasureSmaller_Then_RotateRight)
{
  dsInit(dsCfgOneFront);
  double angle_rad = deg2rad(+20);
  // in the starting cell, pointing to east wall under angle
  stateInitialize(&st_now);
  st_now.t = angle_rad;
  double measurements[] = { LAB_DIM_CORRIDOR_HALF/cos(angle_rad) * SMALLER };

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_GREATER_THAN_DOUBLE(0, st_next.t);
  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.t, st_next.t);
}

TEST(stateNextDistSensors_One, Given_FrontAndUnderAnglePositive_When_MeasureBigger_Then_RotateLeft)
{
  dsInit(dsCfgOneFront);
  double angle_rad = deg2rad(+20);
  // in the starting cell, pointing to east wall under angle
  stateInitialize(&st_now);
  st_now.t = angle_rad;
  double measurements[] = { (LAB_DIM_CORRIDOR_HALF/cos(angle_rad)) * BIGGER };

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_GREATER_THAN_DOUBLE(st_now.t, st_next.t);
  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.t*2, st_next.t);
}

TEST(stateNextDistSensors_One, Given_FrontAndUnderAngleNegative_When_MeasureBigger_Then_RotateRight)
{
  dsInit(dsCfgOneFront);
  double angle_rad = deg2rad(-20);
  // in the starting cell, pointing to east wall under angle
  stateInitialize(&st_now);
  st_now.t = angle_rad;
  double measurements[] = { (LAB_DIM_CORRIDOR_HALF/cos(angle_rad)) * BIGGER };

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.t, st_next.t);
  TEST_ASSERT_GREATER_THAN_DOUBLE(st_now.t*2, st_next.t);
}

TEST(stateNextDistSensors_One, Given_Front_When_NoWallAndMeasureBigger_Then_DoNotMove)
{
  dsInit(dsCfgOneFront);
  double measurements[] = { LAB_DIM_CORRIDOR * 2 / 3 };

  labResetToDefault();
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_STATE_WITHIN(st_now, st_next);
}

TEST(stateNextDistSensors_One, Given_Front_When_MeasurementTooBig_Then_DoNotMove)
{
  dsInit(dsCfgOneFront);
  double measurements[] = { LAB_DIM_CORRIDOR*sqrt(2) + 0.000001};

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_STATE_WITHIN(st_now, st_next);
}

TEST(stateNextDistSensors_One, Given_Front_When_MeasurementTooSmall_Then_DoNotMove)
{
  dsInit(dsCfgOneFront);
  double measurements[] = { LAB_DIM_WALL_HALF - 0.000001};

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_STATE_WITHIN(st_now, st_next);
}

TEST(stateNextDistSensors_One, Given_StartPosAndRear_When_MeasureTheSame_Then_DoNotMove)
{
  dsInit(dsCfgOneRear);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF};
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_STATE_WITHIN(st_now, st_next);
}
TEST(stateNextDistSensors_One, Given_StartPosAndRear_When_MeasureBigger_Then_MoveUp)
{
  dsInit(dsCfgOneRear);
  double measurements[] = { LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL};
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_LESS_THAN_DOUBLE(LAB_DIM_CELL, st_next.y);
  TEST_ASSERT_GREATER_THAN_DOUBLE(st_now.y, st_next.y);
}

TEST_GROUP(stateNextDistSensors_Two);
TEST_SETUP(stateNextDistSensors_Two) {
  setUpCommon();
  labLoad(labirynths[LABIRYNTHS_ALL_WALLS]);
}
TEST_TEAR_DOWN(stateNextDistSensors_Two) { tearDownCommon(); }

TEST(stateNextDistSensors_Two, Given_LeftRight_When_MeasureProper_Then_DoNotMove)
{
  dsInit(dsCfgTwoLeftRight);
  double measurements[] = {
    LAB_DIM_CORRIDOR_HALF,
    LAB_DIM_CORRIDOR_HALF
  };
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_STATE_WITHIN(st_now, st_next);
}

TEST(stateNextDistSensors_Two, Given_LeftRight_When_MeasureLeftSmaller_Then_MoveLeft)
{
  dsInit(dsCfgTwoLeftRight);
  double measurements[] = {
    LAB_DIM_CORRIDOR_HALF - LAB_DIM_WALL,
    LAB_DIM_CORRIDOR_HALF
  };
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_EQUAL_DOUBLE(st_now.y, st_next.y);
  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.x, st_next.x);
}

TEST(stateNextDistSensors_Two, Given_LeftRight_When_LeftReadingInvalidAndRightBigger_Then_MoveLeft)
{
  dsInit(dsCfgTwoLeftRight);
  double measurements[] = {
    100*LAB_DIM_CORRIDOR_HALF,
    LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL,
  };
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_EQUAL_DOUBLE(st_now.y, st_next.y);
  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.x, st_next.x);
}

TEST(stateNextDistSensors_Two, Given_LeftRight_When_RightReadingInvalidAndLeftBigger_Then_MoveRight)
{
  dsInit(dsCfgTwoLeftRight);
  double measurements[] = {
    LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL,
    100*LAB_DIM_CORRIDOR_HALF,
  };
  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_EQUAL_DOUBLE(st_now.y, st_next.y);
  TEST_ASSERT_GREATER_THAN_DOUBLE(st_now.x, st_next.x);
}

TEST(stateNextDistSensors_Two, Given_LeftFront_When_RotatedALittleRightAndBothBigger_Then_RotateMoreAndMove)
{
  dsInit(dsCfgTwoLeftFront);
  double angle_rad = deg2rad(-20);
  // in the starting cell, pointing to east wall under angle
  stateInitialize(&st_now);
  st_now.t = angle_rad;
  double measurements[] = {
      (LAB_DIM_CORRIDOR_HALF / cos(angle_rad)) * BIGGER,
      (LAB_DIM_CORRIDOR_HALF / cos(angle_rad)) * BIGGER,
  };

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.t, st_next.t);
  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.y, st_next.y);
  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.x, st_next.x);
}

TEST_GROUP(stateNextDistSensors_Five);
TEST_SETUP(stateNextDistSensors_Five) {
  setUpCommon();
  labLoad(labirynths[LABIRYNTHS_ALL_WALLS]);
  dsInit(dsCfgFive);
}
TEST_TEAR_DOWN(stateNextDistSensors_Five) { tearDownCommon(); }

TEST(stateNextDistSensors_Five, When_ReadingsProper_Then_DoNotMove)
{
  double measurements[] = {
      LAB_DIM_CORRIDOR_HALF,
      (LAB_DIM_CORRIDOR_HALF / cos(M_PI_4)),
      LAB_DIM_CORRIDOR_HALF,
      (LAB_DIM_CORRIDOR_HALF / cos(M_PI_4)),
      LAB_DIM_CORRIDOR_HALF,
  };

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_EQUAL_DOUBLE(st_now.t, st_next.t);
  TEST_ASSERT_EQUAL_DOUBLE(st_now.x, st_next.x);
  TEST_ASSERT_EQUAL_DOUBLE(st_now.y, st_next.y);
}

TEST(stateNextDistSensors_Five, When_CloserToBottomWall_Then_MoveDown)
{
  double measurements[] = {
      LAB_DIM_CORRIDOR_HALF,
      (LAB_DIM_CORRIDOR_HALF / cos(M_PI_4)),
      LAB_DIM_CORRIDOR_HALF * BIGGER,
      (LAB_DIM_CORRIDOR_HALF / cos(M_PI_4)),
      LAB_DIM_CORRIDOR_HALF,
  };

  TEST_ASSERT_EQUAL(RET_SUCCESS, stateNextDistSensors(st_now, &st_next, measurements, ARRAY_SIZE(measurements)));
  TEST_ASSERT_EQUAL_DOUBLE(st_now.x, st_next.x);
  TEST_ASSERT_LESS_THAN_DOUBLE(st_now.y, st_next.y);
}

TEST_GROUP(stateStep_FUN);
TEST_SETUP(stateStep_FUN) {
  setUpCommon();
  dsInit(dsCfgFive);
}
TEST_TEAR_DOWN(stateStep_FUN) { tearDownCommon(); }

TEST(stateStep_FUN, When_NothingChanges_Then_StayStill)
{
  state_t state;
  stateInitialize(&state);
  stateInitialize(&st_expected);
  motors_ticks_t ticks = {0,0};

  double measurements[] = {
      LAB_DIM_CORRIDOR_HALF,
      (LAB_DIM_CORRIDOR_HALF / cos(M_PI_4)),
      LAB_DIM_CORRIDOR_HALF * BIGGER,
      (LAB_DIM_CORRIDOR_HALF / cos(M_PI_4)),
      LAB_DIM_CORRIDOR_HALF,
  };

  stateStep(1.0, &state, measurements, ARRAY_SIZE(measurements), ticks);

  TEST_ASSERT_STATE_WITHIN(st_expected, state);
}

TEST_GROUP_RUNNER(stateStep_FUN)
{
  RUN_TEST_CASE(stateStep_FUN, When_NothingChanges_Then_StayStill);
}

TEST_GROUP_RUNNER(stateNextModel_FUN)
{
  RUN_TEST_CASE(stateNextModel_FUN, When_VelocitiesZero_Then_StayStill);
  RUN_TEST_CASE(stateNextModel_FUN, When_DrivesStraight_Then_UpdateOnlyXY);
  /**
   * @brief TODO
   * Add UT for driving backward
   * Add tests for driving straight in a line which is not horizontal nor vertical
   * Add tests for turning
   * Add one UT for both turning and driving
   */

}

TEST_GROUP_RUNNER(stateNextEncoders_FUN)
{
  RUN_TEST_CASE(stateNextEncoders_FUN, When_NoTicks_Then_DoNotMove);
  RUN_TEST_CASE(stateNextEncoders_FUN, Given_Horizontal_When_EncodersEqual_Then_GoStraight);
  RUN_TEST_CASE(stateNextEncoders_FUN, Given_SomeAngle_When_EncodersEqualAndNegative_Then_GoBackwardsStraightInAngle);
  RUN_TEST_CASE(stateNextEncoders_FUN, Given_SomeAngleAndWeirdPoint_When_EncodersEqual_Then_GoBackwardsStraightInAngle);
  RUN_TEST_CASE(stateNextEncoders_FUN, When_RotateAroundLeftWheel90degs_Then_Rotate);
  RUN_TEST_CASE(stateNextEncoders_FUN, Given_RightWheelOn0_When_RotateAroundRightWheel180degs_Then_RotateProperly);
  RUN_TEST_CASE(stateNextEncoders_FUN, When_LeftWheelDrivesTwiceMoreThenRight_Then_RotateProperly);
}

TEST_GROUP_RUNNER(stateNextDistSensors_FUN)
{
  RUN_TEST_CASE(stateNextDistSensors_Zero, When_NoSensor_Then_ReturnFailure);

  RUN_TEST_CASE(stateNextDistSensors_One, Given_ValidAngle5deg_When_MouseIsRotatedTooMuch_Then_ReturnFailure);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_ValidAngle5deg_When_MouseIsRotatedLess_Then_ReturnSuccess);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_ValidAngle0deg_When_MouseIsRotatedLess_Then_ReturnSuccess);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_ValidAngle45deg_When_MouseIsRotatedMore_Then_ReturnSuccess);

  RUN_TEST_CASE(stateNextDistSensors_One, Given_StartPosAndRight_When_MeasureSmaller_Then_MoveToRight);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_StartPosAndRight_When_MeasureBigger_Then_MoveToLeft);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_StartPosAndRight_When_MeasureTheSame_Then_DoNotMove);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_StartPosAndLeft_When_MeasureBigger_Then_MoveToRight);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_StartPosAndLeft_When_MeasureTheSame_Then_DoNotMove);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_StartPosAndFront_When_MeasureTheSame_Then_DoNotMove);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_StartPosAndFront_When_MeasureBigger_Then_MoveDown);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_StartPosAndRear_When_MeasureTheSame_Then_DoNotMove);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_StartPosAndRear_When_MeasureBigger_Then_MoveUp);

  RUN_TEST_CASE(stateNextDistSensors_One, Given_FrontAndUnderAngle_When_MeasureTheSame_Then_DoNotMove);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_FrontAndUnderAnglePositive_When_MeasureBigger_Then_RotateLeft);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_FrontAndUnderAnglePositive_When_MeasureSmaller_Then_RotateRight);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_FrontAndUnderAngleNegative_When_MeasureSmaller_Then_RotateLeft);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_FrontAndUnderAngleNegative_When_MeasureBigger_Then_RotateRight);

  RUN_TEST_CASE(stateNextDistSensors_One, Given_Front_When_NoWallAndMeasureBigger_Then_DoNotMove);

  RUN_TEST_CASE(stateNextDistSensors_One, Given_Front_When_MeasurementTooBig_Then_DoNotMove);
  RUN_TEST_CASE(stateNextDistSensors_One, Given_Front_When_MeasurementTooSmall_Then_DoNotMove);


  RUN_TEST_CASE(stateNextDistSensors_Two, Given_LeftRight_When_MeasureProper_Then_DoNotMove);
  RUN_TEST_CASE(stateNextDistSensors_Two, Given_LeftRight_When_MeasureLeftSmaller_Then_MoveLeft);
  RUN_TEST_CASE(stateNextDistSensors_Two, Given_LeftRight_When_LeftReadingInvalidAndRightBigger_Then_MoveLeft);
  RUN_TEST_CASE(stateNextDistSensors_Two, Given_LeftRight_When_RightReadingInvalidAndLeftBigger_Then_MoveRight);

  RUN_TEST_CASE(stateNextDistSensors_Two, Given_LeftFront_When_RotatedALittleRightAndBothBigger_Then_RotateMoreAndMove);

  RUN_TEST_CASE(stateNextDistSensors_Five, When_ReadingsProper_Then_DoNotMove);
  RUN_TEST_CASE(stateNextDistSensors_Five, When_CloserToBottomWall_Then_MoveDown);
}

int main(void)
{
  UNITY_BEGIN();
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);

  RUN_TEST_GROUP(stateNextModel_FUN);
  RUN_TEST_GROUP(stateNextEncoders_FUN);
  RUN_TEST_GROUP(stateNextDistSensors_FUN);

  RUN_TEST_GROUP(stateStep_FUN);

  return (UnityEnd());
}
