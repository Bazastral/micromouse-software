
#include "unity_fixture.h"

#include "labirynth.h"
#include "labirynth_alter.h"
#include "labirynth_mapping.h"

#include "useful.h"
#include "platform_base.h"
#include "labirynths.h"
#include "typedef.h"
#include "logger.h"
#include "logger.h"

typedef uint8_t lab_cell_t;
STATIC ret_code_t labWallAdd(lab_cellxy_t coords, lab_cell_t cell);
STATIC ret_code_t labWallDel(lab_cellxy_t coords, lab_cell_t cell);
STATIC ret_code_t labGetCell(lab_cellxy_t coords, lab_cell_t *cell);

TEST_GROUP(labResetToDefault_FUN);
TEST_SETUP(labResetToDefault_FUN) { }
TEST_TEAR_DOWN(labResetToDefault_FUN) { }

TEST(labResetToDefault_FUN, all)
{
  lab_cellxy_t cellxy = {0, 0};
  lab_cell_t cell;
  ret_code_t ret;

  TEST_ASSERT_EQUAL(RET_SUCCESS, labResetToDefault());

  //check if all bottom cells have south wall
  cellxy.y = 0;
  cellxy.x = 1;
  //this cell has also west wall
  TEST_ASSERT_TRUE(labWallsExist(cellxy, LAB_SOUTH));
  TEST_ASSERT_TRUE(labWallsExist(cellxy, LAB_WEST));
  TEST_ASSERT_FALSE(labWallsExist(cellxy, LAB_NORTH));
  TEST_ASSERT_FALSE(labWallsExist(cellxy, LAB_EAST));
  //check other
  for(uint8_t x = 2; x < LAB_N-1; x++)
  {
    cellxy.x = x;
    TEST_ASSERT_EQUAL(RET_SUCCESS, labGetCell(cellxy, &cell));
    TEST_ASSERT_EQUAL(LAB_SOUTH, cell);
  }

  //check if all left cells have west wall
  cellxy.x = 0;
  for(uint8_t y = 1; y < LAB_N-1; y++)
  {
    cellxy.y = y;
    ret = labGetCell(cellxy, &cell);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
    TEST_ASSERT_EQUAL(LAB_WEST, cell);
  }

  //check if all right cells have east wall
  cellxy.x = LAB_N-1;
  for(uint8_t y = 1; y < LAB_N-1; y++)
  {
    cellxy.y = y;
    ret = labGetCell(cellxy, &cell);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
    TEST_ASSERT_EQUAL(LAB_EAST, cell);
  }

  //check if all top cells have north wall
  cellxy.y = LAB_N-1;
  for(uint8_t x = 1; x < LAB_N-1; x++)
  {
    cellxy.x = x;
    ret = labGetCell(cellxy, &cell);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
    TEST_ASSERT_EQUAL(LAB_NORTH, cell);
  }

  //check corners
  cellxy.x = 0;
  cellxy.y = 0;
  ret = labGetCell(cellxy, &cell);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(LAB_SOUTH | LAB_WEST | LAB_EAST, cell);

  cellxy.x = LAB_N-1;
  cellxy.y = 0;
  ret = labGetCell(cellxy, &cell);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(LAB_SOUTH | LAB_EAST, cell);

  cellxy.x = LAB_N-1;
  cellxy.y = LAB_N-1;
  ret = labGetCell(cellxy, &cell);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(LAB_NORTH | LAB_EAST, cell);

  cellxy.x = 0;
  cellxy.y = LAB_N-1;
  ret = labGetCell(cellxy, &cell);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(LAB_NORTH | LAB_WEST, cell);

  //check if all cells inside are empty
  for(uint8_t x = 1; x < LAB_N-1; x++)
  {
    for(uint8_t y = 1; y < LAB_N-1; y++)
    {
      cellxy.x = x; cellxy.y = y;
      ret = labGetCell(cellxy, &cell);
      TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
      TEST_ASSERT_EQUAL(0, cell);
    }
  }
}


TEST_GROUP(labWallAddDel_FUN);
TEST_SETUP(labWallAddDel_FUN) { }
TEST_TEAR_DOWN(labWallAddDel_FUN) { }

TEST(labWallAddDel_FUN, When_InvalidCellState_Then_ReturnInvalidParam)
{
  lab_cellxy_t cellxy;
  cellxy.x = 0; cellxy.y = 0;

  lab_cell_t cell = 0;
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallAdd(cellxy, cell));
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallDel(cellxy, cell));

  cell = 0xf0;
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallAdd(cellxy, cell));
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallDel(cellxy, cell));
}

TEST(labWallAddDel_FUN, When_InvalidCellxy_Then_ReturnInvalidParam)
{
  lab_cellxy_t cellxy;
  lab_cell_t cell = 0;

  cellxy.x = LAB_N; cellxy.y = 0;
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labGetCell(cellxy, &cell));
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallAdd(cellxy, cell));
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallDel(cellxy, cell));

  cellxy.x = 0; cellxy.y = LAB_N;
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labGetCell(cellxy, &cell));
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallAdd(cellxy, cell));
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallDel(cellxy, cell));

  cellxy.x = -1; cellxy.y = 0;
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labGetCell(cellxy, &cell));
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallAdd(cellxy, cell));
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallDel(cellxy, cell));

  cellxy.x = 0; cellxy.y = -1;
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labGetCell(cellxy, &cell));
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallAdd(cellxy, cell));
  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, labWallDel(cellxy, cell));

}

TEST(labWallAddDel_FUN, When_WallAddedOrDeleted_Then_NeighborCellWallAddedOrDeleted)
{
  lab_cellxy_t coords_to_test[] = {
    {0, 0},
    {0, LAB_N-1},
    {LAB_N-1, 0},
    {LAB_N-1, LAB_N-1},
  };

  lab_cell_t walls_to_test[] = {
    LAB_NORTH |  LAB_EAST,
    LAB_SOUTH |  LAB_EAST,
    LAB_NORTH |  LAB_WEST,
    LAB_SOUTH |  LAB_WEST,
  };

  lab_cellxy_t neighbours[][2] = {
    {{1, 0}, {0, 1}},
    {{1, LAB_N-1}, {0, LAB_N-2}},
    {{LAB_N-2, 0}, {LAB_N-1, 1}},
    {{LAB_N-2, LAB_N-1}, {LAB_N-1, LAB_N-2}}
  };

  lab_cell_t neighbours_walls[][2] = {
    {LAB_WEST, LAB_SOUTH},
    {LAB_WEST, LAB_NORTH},
    {LAB_EAST, LAB_SOUTH},
    {LAB_EAST, LAB_NORTH},
  };

  lab_cell_t cell_prev, cell_now;
  ret_code_t ret;

  for(unsigned int i=0; i<(sizeof(coords_to_test)/sizeof(coords_to_test[0])); i++)
  {
    //save previous state
    TEST_ASSERT_EQUAL(RET_SUCCESS, labGetCell(coords_to_test[i], &cell_prev));

    //test set&get
    ret = labWallDel(coords_to_test[i], LAB_ALL_DIRS);
    ret = labGetCell(coords_to_test[i], &cell_now);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
    TEST_ASSERT_EQUAL(0, cell_now);
    ret = labWallAdd(coords_to_test[i], walls_to_test[i]);
    ret = labGetCell(coords_to_test[i], &cell_now);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
    TEST_ASSERT_EQUAL(walls_to_test[i], cell_now);

    //check if neighbourgh also got a wall
    for(unsigned int n=0; n<sizeof(neighbours[0])/sizeof(neighbours[0][0]); n++)
    {
      TEST_ASSERT_EQUAL(RET_SUCCESS, labGetCell(neighbours[i][n], &cell_now));
      TEST_ASSERT_EQUAL(neighbours_walls[i][n], (neighbours_walls[i][n] & cell_now));
    }

    //restore saved state
    ret = labWallDel(coords_to_test[i], LAB_ALL_DIRS);
    ret = labWallAdd(coords_to_test[i], cell_prev);
    ret = labGetCell(coords_to_test[i], &cell_now);
    TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
    TEST_ASSERT_EQUAL(cell_prev, cell_now);
  }
}


TEST_GROUP(labPoint2Cellxy_FUN);
TEST_SETUP(labPoint2Cellxy_FUN) { }
TEST_TEAR_DOWN(labPoint2Cellxy_FUN) { }

TEST(labPoint2Cellxy_FUN, all)
{
  lab_cellxy_t cellxy;
  point_t coords;
  ret_code_t ret;

  // *********************************
  // Coords -> cellxy
  coords.x = 0; coords.y = 0;
  ret = labPoint2Cellxy(&cellxy, coords);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(0, cellxy.x);
  TEST_ASSERT_EQUAL(0, cellxy.y);
  // Coords -> cellxy
  coords.x = LAB_DIM_CELL; coords.y = LAB_DIM_CELL + (LAB_DIM_WALL_HALF*0.9);
  ret = labPoint2Cellxy(&cellxy, coords);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(0, cellxy.x);
  TEST_ASSERT_EQUAL(0, cellxy.y);
  // Coords -> cellxy
  coords.x = LAB_DIM_CELL + LAB_DIM_WALL;
  coords.y = LAB_DIM_CELL + LAB_DIM_WALL_HALF + LAB_DIM_WALL;
  ret = labPoint2Cellxy(&cellxy, coords);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(1, cellxy.x);
  TEST_ASSERT_EQUAL(1, cellxy.y);
  // Coords -> cellxy
  coords.x = LAB_DIM_CELL + 2*LAB_DIM_WALL;
  coords.y = LAB_DIM_CELL + 2*LAB_DIM_WALL;
  ret = labPoint2Cellxy(&cellxy, coords);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(1, cellxy.x);
  TEST_ASSERT_EQUAL(1, cellxy.y);
  // Coords -> cellxy
  coords.x = 3*LAB_DIM_CELL + 2*LAB_DIM_WALL;
  coords.y = 5*LAB_DIM_CELL + 2*LAB_DIM_WALL;
  ret = labPoint2Cellxy(&cellxy, coords);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(3, cellxy.x);
  TEST_ASSERT_EQUAL(5, cellxy.y);
  // Coords -> cellxy
  coords.x = LAB_DIM_MAX;
  coords.y = LAB_DIM_MAX-LAB_DIM_WALL_HALF;
  ret = labPoint2Cellxy(&cellxy, coords);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(LAB_N-1, cellxy.x);
  TEST_ASSERT_EQUAL(LAB_N-1, cellxy.y);
}

TEST_GROUP(labGetMidCell_FUN);
TEST_SETUP(labGetMidCell_FUN) { }
TEST_TEAR_DOWN(labGetMidCell_FUN) { }

TEST(labGetMidCell_FUN, all)
{
  lab_cellxy_t cellxy;
  point_t coords;
  ret_code_t ret;
  // *********************************
  // Cellxy -> coords
  cellxy.x = 0; cellxy.y = 0;
  ret = labGetMidCell(cellxy, &coords);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL_DOUBLE(LAB_DIM_CELL_HALF + LAB_DIM_WALL_HALF, coords.x);
  TEST_ASSERT_EQUAL_DOUBLE(LAB_DIM_CELL_HALF + LAB_DIM_WALL_HALF, coords.y);
  // Cellxy -> coords
  cellxy.x = 1; cellxy.y = 2;
  ret = labGetMidCell(cellxy, &coords);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL_DOUBLE(1*LAB_DIM_CELL + LAB_DIM_WALL + LAB_DIM_CORRIDOR_HALF, coords.x);
  TEST_ASSERT_EQUAL_DOUBLE(2*LAB_DIM_CELL + LAB_DIM_WALL + LAB_DIM_CORRIDOR_HALF, coords.y);


  cellxy.x = LAB_N; cellxy.y = -1;
  ret = labGetMidCell(cellxy, &coords);
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, ret);
  // Coords -> cellxy
  coords.x = -1; coords.y = 123123;
  ret = labPoint2Cellxy(&cellxy, coords);
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, ret);
}

TEST_GROUP(labCheckXy_FUN);
TEST_SETUP(labCheckXy_FUN) { }
TEST_TEAR_DOWN(labCheckXy_FUN) { }

TEST(labCheckXy_FUN, all)
{
  lab_cellxy_t xy;

  xy.x = 0; xy.y = 0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, labCheckXy(xy));
  xy.x = 0; xy.y = 16;
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, labCheckXy(xy));
  xy.x = 16; xy.y = 0;
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, labCheckXy(xy));
  xy.x = 18; xy.y = -1;
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, labCheckXy(xy));
}

static int neigh_cnt = 0;
void neigh_cb(lab_cellxy_t ref, lab_cellxy_t coords, lab_cell_t cell)
{
  (void) ref;
  (void) coords;
  (void) cell;
  neigh_cnt++;
}

TEST_GROUP(labForEachNeighbor_FUN);
TEST_SETUP(labForEachNeighbor_FUN) { }
TEST_TEAR_DOWN(labForEachNeighbor_FUN) { }

TEST(labForEachNeighbor_FUN, all)
{
  lab_cellxy_t coords;

  TEST_ASSERT_EQUAL(RET_SUCCESS, labLoad(labirynths[LABIRYNTHS_A]));

  neigh_cnt = 0; coords.x = 0; coords.y = 0;
  TEST_ASSERT_EQUAL(RET_SUCCESS, labForEachNeighbor(coords, neigh_cb));
  TEST_ASSERT_EQUAL(1, neigh_cnt);

  neigh_cnt = 0; coords.x = 1; coords.y = 1;
  TEST_ASSERT_EQUAL(RET_SUCCESS, labForEachNeighbor(coords, neigh_cb));
  TEST_ASSERT_EQUAL(1, neigh_cnt);

  neigh_cnt = 0; coords.x = 8; coords.y = 8;
  TEST_ASSERT_EQUAL(RET_SUCCESS, labForEachNeighbor(coords, neigh_cb));
  TEST_ASSERT_EQUAL(3, neigh_cnt);

  neigh_cnt = 0; coords.x = 13; coords.y = 12;
  TEST_ASSERT_EQUAL(RET_SUCCESS, labForEachNeighbor(coords, neigh_cb));
  TEST_ASSERT_EQUAL(3, neigh_cnt);

  neigh_cnt = 0; coords.x = 4; coords.y = 3;
  TEST_ASSERT_EQUAL(RET_SUCCESS, labForEachNeighbor(coords, neigh_cb));
  TEST_ASSERT_EQUAL(0, neigh_cnt);

  // test invalid values
  coords.x = 19; coords.y = 12;
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, labForEachNeighbor(coords, neigh_cb));
}

TEST_GROUP(labNoticeWall_FUN);
TEST_SETUP(labNoticeWall_FUN)
{
  labResetToDefault();
  labMappingInit();
}
TEST_TEAR_DOWN(labNoticeWall_FUN) { }

static void LabNoticeTimes(lab_cellxy_t cellxy, lab_cell_t wall, bool exists, uint16_t times)
{
  lab_specific_wall_t spec_wall = { .cell_xy = cellxy, .direction = wall };

  for(uint8_t i = 0u; i < times; i++)
  {
    labNoticeWall(spec_wall, exists);
  }
}

TEST(labNoticeWall_FUN, When_NoWallNoticed_Then_ReturnInvalidParams)
{
  lab_cell_t wallNoticedInvalid;
  wallNoticedInvalid = 0u;
  lab_specific_wall_t wall = { .cell_xy = {0,0}, .direction = wallNoticedInvalid };
  ret_code_t ret = labNoticeWall(wall, true);
  TEST_ASSERT_EQUAL(ret, RET_ERR_INVALID_PARAM);
}

TEST(labNoticeWall_FUN, When_MoreThanOneWallsNoticed_Then_ReturnInvalidParams)
{
  lab_cell_t wallNoticedInvalid;
  wallNoticedInvalid = LAB_NORTH | LAB_SOUTH;
  lab_specific_wall_t wall = { .cell_xy = {0,0}, .direction = wallNoticedInvalid };
  ret_code_t ret = labNoticeWall(wall, true);
  TEST_ASSERT_EQUAL(ret, RET_ERR_INVALID_PARAM);
}

TEST(labNoticeWall_FUN, When_CalledWithProperParams_Then_ReturnSuccess)
{
  lab_specific_wall_t wall = { .cell_xy = {0,0}, .direction = LAB_EAST };
  ret_code_t ret = labNoticeWall(wall, true);
  TEST_ASSERT_EQUAL(ret, RET_SUCCESS);
}

TEST(labNoticeWall_FUN, When_NoticedNotEnoughTimes_Then_NothingHappens)
{
  lab_cellxy_t cellxy = {1, 1};
  lab_cell_t cell = LAB_NORTH;

  LabNoticeTimes(cellxy, cell, true, LAB_WALL_NOTICE_TO_SAVE-1u);
  TEST_ASSERT_FALSE(labWallsExist(cellxy, LAB_NORTH));
  TEST_ASSERT_FALSE(labWallsExist(cellxy, LAB_SOUTH));
  TEST_ASSERT_FALSE(labWallsExist(cellxy, LAB_EAST));
  TEST_ASSERT_FALSE(labWallsExist(cellxy, LAB_WEST));
}

TEST(labNoticeWall_FUN, When_NoticedEnoughTimesSet_Then_WallIsSet)
{
  lab_cellxy_t cellxy = {1, 1};
  lab_cell_t wall = LAB_NORTH;

  LabNoticeTimes(cellxy, wall, true, LAB_WALL_NOTICE_TO_SAVE);
  TEST_ASSERT_TRUE(labWallsExist(cellxy, LAB_NORTH));
}

TEST(labNoticeWall_FUN, When_NoticedEnoughTimesAbsence_Then_WallDoesNotExist)
{
  lab_cellxy_t cellxy = {1, 1};
  lab_cell_t wall = LAB_NORTH;

  LabNoticeTimes(cellxy, wall, false, LAB_WALL_NOTICE_TO_SAVE);

  // check that wall is set
  TEST_ASSERT_FALSE(labWallsExist(cellxy, LAB_NORTH));
}

TEST(labNoticeWall_FUN, When_NoticedEnoughTimes_Then_WallIsKnown)
{
  lab_cellxy_t cellxy = {1, 1};
  lab_cell_t wall = LAB_NORTH;

  LabNoticeTimes(cellxy, wall, false, LAB_WALL_NOTICE_TO_SAVE);

  TEST_ASSERT_TRUE(labWallsKnown(cellxy, LAB_NORTH));
}

TEST(labNoticeWall_FUN, When_NoticedEnoughTimes_Then_OtherWallsNotChanged)
{
  lab_cellxy_t cellxy = {1, 1};
  lab_cell_t wall = LAB_NORTH;

  LabNoticeTimes(cellxy, wall, true, LAB_WALL_NOTICE_TO_SAVE);

  // other walls are not known
  TEST_ASSERT_FALSE(labWallsKnown(cellxy, LAB_SOUTH));
  TEST_ASSERT_FALSE(labWallsKnown(cellxy, LAB_EAST));
  TEST_ASSERT_FALSE(labWallsKnown(cellxy, LAB_WEST));
}

TEST(labNoticeWall_FUN, When_NoticedEnoughTimesFromDifferentCellsNS_Then_WallIsKnown)
{
  lab_cellxy_t cellxy = {1, 1};
  lab_cell_t wall = LAB_NORTH;

  // it is once seen from other side
  LabNoticeTimes(cellxy, wall, true, LAB_WALL_NOTICE_TO_SAVE-1u);
  cellxy.y = 2; wall = LAB_SOUTH;
  LabNoticeTimes(cellxy, wall, true, 1u);

  // check that wall is set
  TEST_ASSERT_TRUE(labWallsExist(cellxy, LAB_SOUTH));
  // wall is known
  TEST_ASSERT_TRUE(labWallsKnown(cellxy, LAB_SOUTH));
}

TEST(labNoticeWall_FUN, When_NoticedEnoughTimesFromDifferentCellsEW_Then_WallIsKnown)
{
  lab_cellxy_t cellxy = {1, 1};
  lab_cell_t wall = LAB_WEST;

  // it is once seen from other side
  LabNoticeTimes(cellxy, wall, true, LAB_WALL_NOTICE_TO_SAVE-1u);
  cellxy.x = 0; wall = LAB_EAST;
  LabNoticeTimes(cellxy, wall, true, 1u);

  // check that wall is set
  TEST_ASSERT_TRUE(labWallsExist(cellxy, LAB_EAST));
  // wall is known
  TEST_ASSERT_TRUE(labWallsKnown(cellxy, LAB_EAST));
}

TEST(labNoticeWall_FUN, When_NoticedKnownWall_Then_NothingHappens)
{
  lab_cellxy_t cellxy = {0, 4};
  lab_cell_t wall = LAB_WEST;

  LabNoticeTimes(cellxy, wall, true, 200u);

  // check that wall is set
  TEST_ASSERT_TRUE(labWallsExist(cellxy, LAB_WEST));
  // wall is known
  TEST_ASSERT_TRUE(labWallsKnown(cellxy, LAB_WEST));
}

TEST(labNoticeWall_FUN, When_NoticedTwoWallsIndependently_Then_HandleItProperly)
{
  lab_specific_wall_t wallFirst, wallSecond;

  wallFirst.cell_xy.x = 0; wallFirst.cell_xy.y = 4;
  wallSecond.cell_xy.x = 1; wallSecond.cell_xy.y = 4;
  wallFirst.direction = LAB_WEST;
  wallSecond.direction = LAB_EAST;

  for (uint8_t i = 0u; i < LAB_WALL_NOTICE_TO_SAVE; i++)
  {
    labNoticeWall(wallFirst, true);
    labNoticeWall(wallSecond, false);
  }

  TEST_ASSERT_TRUE(labWallsKnown(wallFirst.cell_xy, wallFirst.direction));
  TEST_ASSERT_TRUE(labWallsExist(wallFirst.cell_xy, wallFirst.direction));

  TEST_ASSERT_TRUE(labWallsKnown(wallSecond.cell_xy, wallSecond.direction));
  TEST_ASSERT_FALSE(labWallsExist(wallSecond.cell_xy, wallSecond.direction));
}

TEST(labNoticeWall_FUN, When_NoticedManyWallsOneByOne_Then_DoNotOverflow)
{
  lab_cellxy_t cellxy;

  // try to overflow, set all interial walls
  // but do not test borders (because they are corner cases)

  for (uint8_t x = 1u; x < LAB_N - 1; x++)
  {
    for (uint8_t y = 1u; y < LAB_N - 1; y++)
    {
      cellxy.x = x;
      cellxy.y = y;
      LabNoticeTimes(cellxy, LAB_NORTH, false, LAB_WALL_NOTICE_TO_SAVE);
      LabNoticeTimes(cellxy, LAB_EAST, false, LAB_WALL_NOTICE_TO_SAVE);

      TEST_ASSERT_TRUE(labWallsKnown(cellxy, LAB_NORTH));
      TEST_ASSERT_TRUE(labWallsKnown(cellxy, LAB_EAST));
    }
  }
}

TEST(labNoticeWall_FUN, When_NoticedManyWallsAtTheSameTime_Then_DoNotOverflow)
{
  lab_cellxy_t cellxy;

  // try to overflow, set all interial walls
  // but do not test borders (because they are corner cases)
  for (uint8_t x = 1u; x < LAB_N - 1; x++)
  {
    for (uint8_t y = 1u; y < LAB_N - 1; y++)
    {
      cellxy.x = x;
      cellxy.y = y;
      // Notice not enough times to set as known wall...
      // so buffer will eventually be full
      LabNoticeTimes(cellxy, LAB_NORTH, false, LAB_WALL_NOTICE_TO_SAVE - 1u);
      LabNoticeTimes(cellxy, LAB_EAST, false, LAB_WALL_NOTICE_TO_SAVE - 1u);
    }
  }

  // Then try to notice new wall - should work, buffer should be somehow freed
  cellxy.x = 5;
  cellxy.y = 5;
  LabNoticeTimes(cellxy, LAB_NORTH, false, LAB_WALL_NOTICE_TO_SAVE);
  TEST_ASSERT_TRUE(labWallsKnown(cellxy, LAB_NORTH));
}

/**
 * @brief In some cases, there might be invalid measurement from sensors
 * and wall will be once seen and once not.
 * In such scenario, one notice which is opposite to another,
 * will be deleted with one oposite.
 * 
 * @example Wall is noticed as absence. 
 * This wall must be noticed as existance (LAB_WALL_NOTICE_TO_SAVE + 1u) times now, 
 * to make sure that the saved state is ok
 */
TEST(labNoticeWall_FUN, When_NoticedDifferentState_Then_MakeSureTheStateIsOk)
{
  lab_cellxy_t cellxy = {1, 2};

  // scenario from example
  LabNoticeTimes(cellxy, LAB_NORTH, true, 1u);
  LabNoticeTimes(cellxy, LAB_NORTH, false, LAB_WALL_NOTICE_TO_SAVE);
  TEST_ASSERT_FALSE(labWallsKnown(cellxy, LAB_NORTH)); // not yet known
  LabNoticeTimes(cellxy, LAB_NORTH, false, 1u);
  TEST_ASSERT_TRUE(labWallsKnown(cellxy, LAB_NORTH)); // now it is known

  // another scenario
  cellxy.x = 3;
  LabNoticeTimes(cellxy, LAB_NORTH, false, LAB_WALL_NOTICE_TO_SAVE - 1u);
  LabNoticeTimes(cellxy, LAB_NORTH, true, 1u);
  LabNoticeTimes(cellxy, LAB_NORTH, false, 2u);
  TEST_ASSERT_TRUE(labWallsKnown(cellxy, LAB_NORTH));
}

TEST(labNoticeWall_FUN, When_WallIsKnown_Then_DoNotChangeItsState)
{
  lab_cellxy_t StartingCellxy = {0u, 0u};

  LabNoticeTimes(StartingCellxy, LAB_NORTH, true, LAB_WALL_NOTICE_TO_SAVE);

  TEST_ASSERT_TRUE(labWallsKnown(StartingCellxy, LAB_NORTH));
  // State of a wall did not change
  TEST_ASSERT_FALSE(labWallsExist(StartingCellxy, LAB_NORTH));
}

TEST(labNoticeWall_FUN, When_CornerCases_Then_HandleThemProperly)
{
  lab_cellxy_t corners[] =
      {
          {0, 0},
          {LAB_N - 1, 0},
          {0, LAB_N - 1},
          {LAB_N - 1, 0}};

  for (uint8_t i = 0u; i < ARRAY_SIZE(corners); i++)
  {

    LabNoticeTimes(corners[i], LAB_NORTH, false, LAB_WALL_NOTICE_TO_SAVE);
    LabNoticeTimes(corners[i], LAB_EAST, false, LAB_WALL_NOTICE_TO_SAVE);
    LabNoticeTimes(corners[i], LAB_SOUTH, false, LAB_WALL_NOTICE_TO_SAVE);
    LabNoticeTimes(corners[i], LAB_WEST, false, LAB_WALL_NOTICE_TO_SAVE);

    TEST_ASSERT_TRUE(labWallsKnown(corners[i], LAB_NORTH));
    TEST_ASSERT_TRUE(labWallsKnown(corners[i], LAB_EAST));
    TEST_ASSERT_TRUE(labWallsKnown(corners[i], LAB_SOUTH));
    TEST_ASSERT_TRUE(labWallsKnown(corners[i], LAB_WEST));
  }
}

TEST_GROUP(labWallsKnown_FUN);
TEST_SETUP(labWallsKnown_FUN) { }
TEST_TEAR_DOWN(labWallsKnown_FUN) { }

TEST(labWallsKnown_FUN, When_LabirynthDefault_Then_StartPointIsKnown)
{
  lab_cellxy_t StartingCellxy = {0u, 0u};
  labResetToDefault();
  TEST_ASSERT_TRUE(labWallsKnown(StartingCellxy, LAB_NORTH));
  TEST_ASSERT_TRUE(labWallsKnown(StartingCellxy, LAB_SOUTH));
  TEST_ASSERT_TRUE(labWallsKnown(StartingCellxy, LAB_EAST));
  TEST_ASSERT_TRUE(labWallsKnown(StartingCellxy, LAB_WEST));
  TEST_ASSERT_TRUE(labWallsKnown(StartingCellxy, LAB_ALL_DIRS));
}

TEST(labWallsKnown_FUN, When_LabirynthDefault_Then_BorderIsKnown)
{
  lab_cellxy_t border = {LAB_N/2, 0u};
  labResetToDefault();
  TEST_ASSERT_TRUE(labWallsKnown(border, LAB_SOUTH));
}

TEST(labWallsKnown_FUN, When_LabirynthDefault_Then_AllOthersUnknown)
{
  lab_cellxy_t border = {LAB_N/2, 0u};
  labResetToDefault();
  TEST_ASSERT_FALSE(labWallsKnown(border, LAB_NORTH));
  TEST_ASSERT_FALSE(labWallsKnown(border, LAB_EAST));
  TEST_ASSERT_FALSE(labWallsKnown(border, LAB_WEST));
}

TEST_GROUP(labGetWallsCoordsFromCellXY_FUN);
TEST_SETUP(labGetWallsCoordsFromCellXY_FUN) { }
TEST_TEAR_DOWN(labGetWallsCoordsFromCellXY_FUN) { }

TEST(labGetWallsCoordsFromCellXY_FUN, When_StartingCell_Then_ReturnProperWalls)
{
  lab_walls_coords_t walls = labGetWallsCoordsFromCellXY(labCoordsStartingPoint());
  TEST_ASSERT_EQUAL_DOUBLE(LAB_DIM_CELL, walls.N);
  TEST_ASSERT_EQUAL_DOUBLE(LAB_DIM_WALL, walls.S);
  TEST_ASSERT_EQUAL_DOUBLE(LAB_DIM_CELL, walls.E);
  TEST_ASSERT_EQUAL_DOUBLE(LAB_DIM_WALL, walls.W);
}

TEST(labGetWallsCoordsFromCellXY_FUN, When_SpecificCell_Then_ReturnProperWalls)
{
  lab_walls_coords_t walls = labGetWallsCoordsFromCellXY((lab_cellxy_t){2u, 5u,});
  TEST_ASSERT_EQUAL_DOUBLE(6*LAB_DIM_CELL, walls.N);
  TEST_ASSERT_EQUAL_DOUBLE(5*LAB_DIM_CELL + LAB_DIM_WALL, walls.S);
  TEST_ASSERT_EQUAL_DOUBLE(3*LAB_DIM_CELL, walls.E);
  TEST_ASSERT_EQUAL_DOUBLE(2*LAB_DIM_CELL + LAB_DIM_WALL, walls.W);
}

TEST_GROUP_RUNNER(labResetToDefault_FUN)
{
  RUN_TEST_CASE(labResetToDefault_FUN, all);
}
TEST_GROUP_RUNNER(labForEachNeighbor_FUN)
{
  RUN_TEST_CASE(labForEachNeighbor_FUN, all);
}
TEST_GROUP_RUNNER(labGetMidCell_FUN)
{
  RUN_TEST_CASE(labGetMidCell_FUN, all);
}
TEST_GROUP_RUNNER(labPoint2Cellxy_FUN)
{
  RUN_TEST_CASE(labPoint2Cellxy_FUN, all);
}
TEST_GROUP_RUNNER(labWallAddDel_FUN)
{
  RUN_TEST_CASE(labWallAddDel_FUN, When_InvalidCellState_Then_ReturnInvalidParam);
  RUN_TEST_CASE(labWallAddDel_FUN, When_InvalidCellxy_Then_ReturnInvalidParam);
  RUN_TEST_CASE(labWallAddDel_FUN, When_WallAddedOrDeleted_Then_NeighborCellWallAddedOrDeleted);
}
TEST_GROUP_RUNNER(labCheckXy_FUN)
{
  RUN_TEST_CASE(labCheckXy_FUN, all);
}
TEST_GROUP_RUNNER(labNoticeWall_FUN)
{
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoWallNoticed_Then_ReturnInvalidParams);
  RUN_TEST_CASE(labNoticeWall_FUN, When_MoreThanOneWallsNoticed_Then_ReturnInvalidParams);
  RUN_TEST_CASE(labNoticeWall_FUN, When_CalledWithProperParams_Then_ReturnSuccess);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedNotEnoughTimes_Then_NothingHappens);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedEnoughTimesSet_Then_WallIsSet);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedEnoughTimesAbsence_Then_WallDoesNotExist);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedEnoughTimes_Then_WallIsKnown);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedEnoughTimes_Then_OtherWallsNotChanged);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedEnoughTimesFromDifferentCellsNS_Then_WallIsKnown);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedEnoughTimesFromDifferentCellsEW_Then_WallIsKnown);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedKnownWall_Then_NothingHappens);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedTwoWallsIndependently_Then_HandleItProperly);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedManyWallsOneByOne_Then_DoNotOverflow);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedManyWallsAtTheSameTime_Then_DoNotOverflow);
  RUN_TEST_CASE(labNoticeWall_FUN, When_CornerCases_Then_HandleThemProperly);
  RUN_TEST_CASE(labNoticeWall_FUN, When_NoticedDifferentState_Then_MakeSureTheStateIsOk);
  RUN_TEST_CASE(labNoticeWall_FUN, When_WallIsKnown_Then_DoNotChangeItsState);
}

TEST_GROUP_RUNNER(labWallsKnown_FUN)
{
  RUN_TEST_CASE(labWallsKnown_FUN, When_LabirynthDefault_Then_StartPointIsKnown);
  RUN_TEST_CASE(labWallsKnown_FUN, When_LabirynthDefault_Then_BorderIsKnown);
  RUN_TEST_CASE(labWallsKnown_FUN, When_LabirynthDefault_Then_AllOthersUnknown);
}

TEST_GROUP_RUNNER(labGetNEwallsFromCellXY_FUN)
{
  RUN_TEST_CASE(labGetWallsCoordsFromCellXY_FUN, When_StartingCell_Then_ReturnProperWalls);
  RUN_TEST_CASE(labGetWallsCoordsFromCellXY_FUN, When_SpecificCell_Then_ReturnProperWalls);
}

static void run_all_tests(void)
{
  RUN_TEST_GROUP(labResetToDefault_FUN);
  RUN_TEST_GROUP(labForEachNeighbor_FUN);
  RUN_TEST_GROUP(labGetMidCell_FUN);
  RUN_TEST_GROUP(labPoint2Cellxy_FUN);
  RUN_TEST_GROUP(labWallAddDel_FUN);
  RUN_TEST_GROUP(labCheckXy_FUN);
  RUN_TEST_GROUP(labNoticeWall_FUN);
  RUN_TEST_GROUP(labWallsKnown_FUN);
  RUN_TEST_GROUP(labGetNEwallsFromCellXY_FUN);

  // not covered at all, but it only loads memory
  // ret_code_t labLoad(const labirynth_t lab);
}

int main(int argc, const char **argv)
{
  log_initialize(platformPutChar, LOGGER_LVL_ERROR);
  return UnityMain(argc, argv, run_all_tests);
}
