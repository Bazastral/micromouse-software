

#include "point.h"
#include "useful.h"
#include "unity.h"
#include "platform_base.h"
#include "logger.h"

void setUp(void)
{
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
}

void tearDown(void)
{
}

void pointRotateAroundOrigin_test(void)
{
  point_t end;

  struct PointRotationPoint{
    point_t start;
    point_t expected;
    double angleRad;
  };
  struct PointRotationPoint testValues[] =
  {
    {{ 1, 2},     { 1, 2}, 0},
    {{ 4, -3},    {4, -3}, 0},
    {{ 4, -3},    {4, -3}, M_PI*2},
    {{ 4, -3},    {4, -3}, M_PI*4},
    {{ 1, 2},     { -2, 1}, M_PI/2},
    {{ 1, 2},     { -1, -2}, M_PI},
    {{ 1, 2},     { -1, -2}, M_PI},
    {{ 1, 2},     { -1, -2}, M_PI + 2*M_PI},
    {{ 1, 2},     { -1, -2}, M_PI - 2*M_PI},

    {{ 1, 2},     { 2.2320508075689, 0.13397459621556}, -2*M_PI/6},
  };

  for(uint8_t i = 0; i < ARRAY_SIZE(testValues); i++)
  {
    end = pointRotateAroundOrigin(testValues[i].start, testValues[i].angleRad);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, testValues[i].expected.x, end.x);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, testValues[i].expected.y, end.y);
  }
}

void pointRotateAroundPoint_test(void)
{
  point_t end;

  struct PointRotationPoint{
    point_t start;
    point_t about;
    point_t expected;
    double angleRad;
  };
  struct PointRotationPoint testValues[] =
  {
    // same from rotating around origin (rotate about (0, 0))
    {{ 1, 2},     { 0.00,  0.00 },   { 1, 2}, 0},
    {{ 4, -3},    { 0.00,  0.00 },   {4, -3}, 0},
    {{ 4, -3},    { 0.00,  0.00 },   {4, -3}, M_PI*2},
    {{ 4, -3},    { 0.00,  0.00 },   {4, -3}, M_PI*4},
    {{ 1, 2},     { 0.00,  0.00 },   { -2, 1}, M_PI/2},
    {{ 1, 2},     { 0.00,  0.00 },   { -1, -2}, M_PI},
    {{ 1, 2},     { 0.00,  0.00 },   { -1, -2}, M_PI},
    {{ 1, 2},     { 0.00,  0.00 },   { -1, -2}, M_PI + 2*M_PI},
    {{ 1, 2},     { 0.00,  0.00 },   { -1, -2}, M_PI - 2*M_PI},
    {{ 1, 2},     { 0.00,  0.00 },   { 2.2320508075689, 0.13397459621556}, -2*M_PI/6},

    {{ 1, 2},     { 1.00,  1.00 },   { 0.00, 1.00 }, M_PI/2 },
    {{ 1, 2},     { 1.00,  1.00 },   { 1.00, 0.00 }, M_PI },
    {{ 1, 2},     { 1.00,  1.00 },   { 2.00, 1.00 }, 3*M_PI/2 },

    {{ 2, -1},     { 1.00,  0.00 },   { 1.00 + sqrt(2), 0.00 }, M_PI/4 },
  };

  for(uint8_t i = 0; i < ARRAY_SIZE(testValues); i++)
  {
    end = pointRotateAroundPoint(
        testValues[i].start, testValues[i].about, testValues[i].angleRad);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, testValues[i].expected.x, end.x);
    TEST_ASSERT_DOUBLE_WITHIN(1e-5, testValues[i].expected.y, end.y);
  }
}

void pointGetDistance_test(void)
{
  point_t one, two;

  one.x = 0, one.y = 0; two.x = 0, two.y = 0;
  TEST_ASSERT_EQUAL_DOUBLE(0, pointGetDistance(one, two));
  TEST_ASSERT_EQUAL_DOUBLE(0, pointGetDistance(two, one));

  one.x = 0, one.y = 0; two.x = 3, two.y = 4;
  TEST_ASSERT_EQUAL_DOUBLE(5, pointGetDistance(one, two));
  TEST_ASSERT_EQUAL_DOUBLE(5, pointGetDistance(two, one));

  one.x = -4, one.y = -3; two.x = 0, two.y = 0;
  TEST_ASSERT_EQUAL_DOUBLE(5, pointGetDistance(one, two));
  TEST_ASSERT_EQUAL_DOUBLE(5, pointGetDistance(two, one));
}

int main(void)
{
  UNITY_BEGIN();

  RUN_TEST(pointRotateAroundOrigin_test);
  RUN_TEST(pointRotateAroundPoint_test);
  RUN_TEST(pointGetDistance_test);

  return (UnityEnd());
}
