
#include <stdlib.h>
#include <time.h>

#include "executor.h"
#include "unity.h"
#include "config.h"
#include "state.h"
#include "useful.h"
#include "platform_base.h"
#include "logger.h"

void setUp(void)
{
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
}

void tearDown(void)
{
}

void executorInit_test(void)
{
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorInit(configGet()));
}

void executorGoToTarget_When_NullProvided_Then_ReturnFailure(void)
{
  point_t target = {0,0};
  state_t state_now;
  double time = 1;

  executorInit(configGet());
  stateInitialize(&state_now);
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, executorGoToTarget(target, state_now, time, NULL));
}

void executorGoToTarget_testBasic(void)
{
  point_t target = {0,0};
  state_t state_now;
  motors_speed_t mot;
  double time = 1;

  state_now.x = 0; state_now.y = 0; state_now.t = 0; state_now.v = 0; state_now.w = 0;
  executorInit(configGet());
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorGoToTarget(target, state_now, time, &mot));
  target.x = rand() % ONE_METER; target.y = rand() % ONE_METER;
  state_now.x = target.x;
  state_now.y = target.y;
  TEST_ASSERT_EQUAL(RET_SUCCESS, executorGoToTarget(target, state_now, time, &mot));

  target.x = 1234*ONE_METER;
  TEST_ASSERT_EQUAL(RET_ERR_OUT_OF_BOUNDS, executorGoToTarget(target, state_now, time, &mot));

  target.x = METERS_TO_MILLIMETERS(1.34);
  TEST_ASSERT_EQUAL(RET_AGAIN, executorGoToTarget(target, state_now, time, &mot));
}

void executorGoToTarget_testAdvanced(void)
{
  point_t target;
  state_t state_now;
  motors_speed_t mot;
  double time = 1;

  // straight
  state_now.x = 0; state_now.y = 0; state_now.t = 0; state_now.v = 0; state_now.w = 0;
  target.x = 2*ONE_METER; target.y = 0;
  mot.right = 0; mot.left = 0;
  executorInit(configGet());
  TEST_ASSERT_EQUAL(RET_AGAIN, executorGoToTarget(target, state_now, time, &mot));
  TEST_ASSERT_EQUAL(mot.left, mot.right);

  // turn left
  state_now.x = ONE_METER; state_now.y = ONE_METER; state_now.t = M_PI; state_now.v = 0; state_now.w = 0;
  target.x = 0; target.y = 0;
  mot.right = 0; mot.left = 0;
  TEST_ASSERT_EQUAL(RET_AGAIN, executorGoToTarget(target, state_now, time, &mot));
  TEST_ASSERT(mot.left <= mot.right);

}

void executorGoToTarget_When_TargetStraighAhead_Then_AccelerateSlowly(void)
{
  point_t target;
  state_t state_now;
  motors_speed_t mot;
  double time = 1;

  // straight
  state_now.x = 0; state_now.y = 0; state_now.t = 0; state_now.v = 0; state_now.w = 0;
  target.x = 2*ONE_METER; target.y = 0;

  executorInit(configGet());
  motors_speed_t motor_prev = mot;
  for(int i = 0; i < 10; i++)
  {
    TEST_ASSERT_EQUAL(RET_AGAIN, executorGoToTarget(target, state_now, time, &mot));
    TEST_ASSERT_EQUAL(mot.left, mot.right);
    state_now.x += mot.left;
    log_debug("motor set to: %f", mot.left);
    TEST_ASSERT_GREATER_THAN_DOUBLE(motor_prev.left, mot.left);
  }
}

void executorGoToTargetAndStop_Given_PreviousTargetAchieved_When_NewTarget_Then_SetMotors(void)
{
  state_t state_now = stateStartingPoint();
  point_t first_aim = stateGetPoint(state_now);
  point_t second_aim = {.x = ONE_METER, .y = 2*ONE_METER};
  const double NOT_IMPORTANT_TIME = 0.01;
  motors_speed_t motors;
  ret_code_t ret;

  executorInit(configGet());

  // first aim is achieved
  ret = executorGoToTargetAndStop(first_aim, state_now, NOT_IMPORTANT_TIME, &motors);
  ret = executorGoToTargetAndStop(second_aim, state_now, NOT_IMPORTANT_TIME, &motors);

  TEST_ASSERT_EQUAL(RET_AGAIN, ret);
  TEST_ASSERT_NOT_EQUAL_DOUBLE(0, motors.left);
  TEST_ASSERT_NOT_EQUAL_DOUBLE(0, motors.right);
}


void executorGoToTargetAndStop_When_TargetAchieved_Then_StopMotorsAndReturnSuccess(void)
{
  state_t state_now = stateStartingPoint();
  point_t aim = stateGetPoint(state_now);
  const double NOT_IMPORTANT_TIME = 0.01;
  motors_speed_t motors;
  ret_code_t ret;

  executorInit(configGet());

  ret = executorGoToTargetAndStop(aim, state_now, NOT_IMPORTANT_TIME, &motors);
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL_DOUBLE(0, motors.left);
  TEST_ASSERT_EQUAL_DOUBLE(0, motors.right);
}

int main(void)
{
  UNITY_BEGIN();
  srand(time(NULL));
  RUN_TEST(executorInit_test);
  RUN_TEST(executorGoToTarget_When_NullProvided_Then_ReturnFailure);
  RUN_TEST(executorGoToTarget_testBasic);
  RUN_TEST(executorGoToTarget_testAdvanced);
  RUN_TEST(executorGoToTarget_When_TargetStraighAhead_Then_AccelerateSlowly);
  RUN_TEST(executorGoToTargetAndStop_When_TargetAchieved_Then_StopMotorsAndReturnSuccess);
  RUN_TEST(executorGoToTargetAndStop_Given_PreviousTargetAchieved_When_NewTarget_Then_SetMotors);


  return (UnityEnd());
}

