

#include "unity.h"
#include "unity_fixture.h"

#include "state.h"
#include "position.h"
#include "platform_base.h"
#include "useful.h"
#include "distance_sensors.h"
#include "labirynth_alter.h"
#include "distance_sensors_hw_mock.h"
#include "motor_mock.h"
#include "config.h"
#include "logger.h"

#include "labirynth_mapping_mock.h"


// take some margin
const double MAX_VALID_SENSOR_MEASUREMENT = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM*0.9;

static config_t cfg;

static state_t st_now, st_expected, st_next;
static const dsConfig_t dsCfgOneFront = {
  .numberOfSensors = 1,
  .sensors = {
    {.offset = {  0, 0, 0 } }
    },
  .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
  .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
  .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS_ALLOW_ALL_READINGS
};
static const dsConfig_t dsCfgTwoLeftFront = {
  .numberOfSensors = 2,
  .sensors = {
    {.offset = {  0, 0, M_PI_2 } },
    {.offset = {  0, 0, 0 } }
    },
  .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
  .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
  .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS_ALLOW_ALL_READINGS
};


void setUpCommon(void)
{
  stateInitModule(configGet());
  stateInitialize(&st_now);
  st_next = st_now;
  st_expected.x = rand();
  st_expected.y = rand();
  st_expected.t = rand();
  st_expected.v = rand();
  st_expected.w = rand();
  cfg = *configGet();
  labResetToDefault();
}

void tearDownCommon(void)
{
}

TEST_GROUP(detectInputParams);
TEST_SETUP(detectInputParams) {
  setUpCommon();
  dsInit(dsCfgOneFront);
  labNoticeWall_mock_init();
}
TEST_TEAR_DOWN(detectInputParams) { tearDownCommon(); }

TEST(detectInputParams, When_NullPointer_Then_ReturnError)
{
  ret_code_t ret = stateDetectWalls(st_now, NULL, 4);

  TEST_ASSERT_EQUAL(RET_ERR_INVALID_PARAM, ret);
}

TEST_GROUP_RUNNER(detectInputParams)
{
  RUN_TEST_CASE(detectInputParams, When_NullPointer_Then_ReturnError);
}

TEST_GROUP(detectOneSensor);
TEST_SETUP(detectOneSensor) {
  setUpCommon();
  dsInit(dsCfgOneFront);
  labNoticeWall_mock_init();
}
TEST_TEAR_DOWN(detectOneSensor) { tearDownCommon(); }

TEST(detectOneSensor, Given_StartingPointFront_When_DetectedNotExists_Then_NotifyIsCalled)
{
  double measurements[1] = {MAX_VALID_SENSOR_MEASUREMENT};

  // There is no wall in the starting cell in front of the robot
  ret_code_t ret = stateDetectWalls(st_now, measurements, ARRAY_SIZE(measurements));
  lab_cellxy_t expected_coords = labCoordsStartingPoint();
  lab_cell_t expected_wall = LAB_NORTH;

  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

  lab_cellxy_t coords;
  coords = labNoticeWall_mock_coords();
  TEST_ASSERT_EQUAL(1, labNoticeWall_mock_called_times());
  TEST_ASSERT_EQUAL_MEMORY(&expected_coords, &coords, sizeof(expected_coords));
  TEST_ASSERT_EQUAL(expected_wall, labNoticeWall_mock_wall());
  TEST_ASSERT_EQUAL(false, labNoticeWall_mock_existence());
}

TEST(detectOneSensor, Given_StartingPointFront_When_DetectedExists_Then_NotifyIsCalled)
{
  double measurements[1] = {LAB_DIM_CORRIDOR_HALF};

  lab_cellxy_t expected_coords = labCoordsStartingPoint();
  lab_cell_t expected_wall = LAB_NORTH;

  ret_code_t ret = stateDetectWalls(st_now, measurements, ARRAY_SIZE(measurements));
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

  lab_cellxy_t coords = labNoticeWall_mock_coords();
  TEST_ASSERT_EQUAL(1, labNoticeWall_mock_called_times());
  TEST_ASSERT_EQUAL_MEMORY(&expected_coords, &coords, sizeof(expected_coords));
  TEST_ASSERT_EQUAL(expected_wall, labNoticeWall_mock_wall());
  TEST_ASSERT_EQUAL(true, labNoticeWall_mock_existence());
}

TEST(detectOneSensor, Given_FrontSensorPointingEast_When_SmallMeasurement_Then_NotifyWallExistence)
{
  lab_cellxy_t expected_coords = {4, 5};

  double measurements[1] = {DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM*2};
  st_now.x = LAB_DIM_CELL*(expected_coords.x+1) - LAB_DIM_CORRIDOR_HALF;
  st_now.y = LAB_DIM_CELL*expected_coords.y + LAB_DIM_WALL*3/2;
  st_now.t = 0;

  lab_cell_t expected_wall = LAB_EAST;

  ret_code_t ret = stateDetectWalls(st_now, measurements, ARRAY_SIZE(measurements));
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

  lab_cellxy_t coords = labNoticeWall_mock_coords();
  TEST_ASSERT_EQUAL(1, labNoticeWall_mock_called_times());
  TEST_ASSERT_EQUAL_MEMORY(&expected_coords, &coords, sizeof(expected_coords));
  TEST_ASSERT_EQUAL(expected_wall, labNoticeWall_mock_wall());
  TEST_ASSERT_EQUAL(true, labNoticeWall_mock_existence());
}

TEST(detectOneSensor, Given_FrontSensorPointingEast_When_ProperBigMeasurementButWallIsTooFar_Then_DoNotNotify)
{
  lab_cellxy_t expected_coords = {2, 8};
  // the range of the sensor must be < LAB_DIM_CELL
  dsGetCfg()->max_range_mm = 150;
  double measurements[1] = {dsGetCfg()->max_range_mm * 0.9};
  st_now.x = LAB_DIM_CELL*(expected_coords.x+1) - dsGetCfg()->max_range_mm; // clue
  st_now.y = LAB_DIM_CELL*expected_coords.y + LAB_DIM_WALL*3/2;
  st_now.t = 0;

  stateDetectWalls(st_now, measurements, ARRAY_SIZE(measurements));

  TEST_ASSERT_EQUAL(0, labNoticeWall_mock_called_times());
}

TEST(detectOneSensor, Given_FrontSensorPointingSouthWithAngle_When_BigMeasurement_Then_NotifyNoWall)
{
  lab_cellxy_t expected_coords = {8, 0};

  double measurements[1] = {MAX_VALID_SENSOR_MEASUREMENT};
  st_now.x = labCellToPoint(expected_coords).x + LAB_DIM_WALL*3/2;
  st_now.y = labCellToPoint(expected_coords).y + LAB_DIM_WALL*3/2;
  st_now.t = -M_PI_4;

  lab_cell_t expected_wall = LAB_EAST;

  ret_code_t ret = stateDetectWalls(st_now, measurements, ARRAY_SIZE(measurements));
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

  lab_cellxy_t coords = labNoticeWall_mock_coords();
  TEST_ASSERT_EQUAL(1, labNoticeWall_mock_called_times());
  TEST_ASSERT_EQUAL_MEMORY(&expected_coords, &coords, sizeof(expected_coords));
  TEST_ASSERT_EQUAL(expected_wall, labNoticeWall_mock_wall());
  TEST_ASSERT_EQUAL(false, labNoticeWall_mock_existence());
}

TEST(detectOneSensor, When_MeasurementBelowThreshold_Then_ReturnFailureAndDontNotify)
{
  const double LOWER_THRESHOLD = 0.03;
  double measurements[1] = {LOWER_THRESHOLD};

  ret_code_t ret = stateDetectWalls(st_now, measurements, ARRAY_SIZE(measurements));
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, ret);
  TEST_ASSERT_EQUAL(0, labNoticeWall_mock_called_times());
}

TEST_GROUP_RUNNER(detectOneSensor)
{
  RUN_TEST_CASE(detectOneSensor, Given_StartingPointFront_When_DetectedNotExists_Then_NotifyIsCalled);
  RUN_TEST_CASE(detectOneSensor, Given_StartingPointFront_When_DetectedExists_Then_NotifyIsCalled);
  RUN_TEST_CASE(detectOneSensor, Given_FrontSensorPointingEast_When_SmallMeasurement_Then_NotifyWallExistence);
  RUN_TEST_CASE(detectOneSensor, Given_FrontSensorPointingEast_When_ProperBigMeasurementButWallIsTooFar_Then_DoNotNotify);
  RUN_TEST_CASE(detectOneSensor, Given_FrontSensorPointingSouthWithAngle_When_BigMeasurement_Then_NotifyNoWall);
  RUN_TEST_CASE(detectOneSensor, When_MeasurementBelowThreshold_Then_ReturnFailureAndDontNotify);
}

TEST_GROUP(detectTwoSensorsFrontAndLeft);
TEST_SETUP(detectTwoSensorsFrontAndLeft) {
  setUpCommon();
  dsInit(dsCfgTwoLeftFront);
  labNoticeWall_mock_init();
}
TEST_TEAR_DOWN(detectTwoSensorsFrontAndLeft) { tearDownCommon(); }

TEST(detectTwoSensorsFrontAndLeft, Given_StartingPoint_When_BothReadingsHigh_Then_NotifyNoWalls)
{
  double measurements[2] = {MAX_VALID_SENSOR_MEASUREMENT, MAX_VALID_SENSOR_MEASUREMENT};
  lab_cellxy_t expected_coords = labCoordsStartingPoint();

  ret_code_t ret = stateDetectWalls(st_now, measurements, ARRAY_SIZE(measurements));
  TEST_ASSERT_EQUAL(RET_SUCCESS, ret);

  lab_cellxy_t coords = labNoticeWall_mock_coords();
  TEST_ASSERT_EQUAL(2, labNoticeWall_mock_called_times());
  TEST_ASSERT_EQUAL_MEMORY(&expected_coords, &coords, sizeof(expected_coords));
  TEST_ASSERT_EQUAL(false, labNoticeWall_mock_existence());
}

TEST(detectTwoSensorsFrontAndLeft, Given_PointingWestAndFirstInvalid_When_SecondExists_Then_NotifyWest)
{
  lab_cellxy_t expected_coords = {3, 10};
  lab_cell_t expected_wall = LAB_WEST;
  double measurements[2] = {-1, LAB_DIM_CORRIDOR_HALF};

  st_now.x = labCellToPoint(expected_coords).x;
  st_now.y = labCellToPoint(expected_coords).y;
  st_now.t = M_PI;

  ret_code_t ret = stateDetectWalls(st_now, measurements, ARRAY_SIZE(measurements));

  TEST_ASSERT_EQUAL(RET_ERR_OUT_OF_BOUNDS, ret);
  lab_cellxy_t coords = labNoticeWall_mock_coords();
  TEST_ASSERT_EQUAL(1, labNoticeWall_mock_called_times());
  TEST_ASSERT_EQUAL_MEMORY(&expected_coords, &coords, sizeof(expected_coords));
  TEST_ASSERT_EQUAL(expected_wall, labNoticeWall_mock_wall());
  TEST_ASSERT_EQUAL(true, labNoticeWall_mock_existence());
}

TEST_GROUP_RUNNER(detectTwoSensorsFrontAndLeft)
{
  RUN_TEST_CASE(detectTwoSensorsFrontAndLeft, Given_StartingPoint_When_BothReadingsHigh_Then_NotifyNoWalls);
  RUN_TEST_CASE(detectTwoSensorsFrontAndLeft, Given_PointingWestAndFirstInvalid_When_SecondExists_Then_NotifyWest);
}

int main(void)
{
  UNITY_BEGIN();
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);

  RUN_TEST_GROUP(detectInputParams);
  RUN_TEST_GROUP(detectOneSensor);
  RUN_TEST_GROUP(detectTwoSensorsFrontAndLeft);

  return (UnityEnd());
}
