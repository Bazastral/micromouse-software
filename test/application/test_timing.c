

#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "unity.h"
#include "unity_fixture.h"

#include "timing.h"
#include "platform_base.h"
#include "logger.h"

const uint32_t ALLOWED_DELTA_US = 10;
const uint32_t DEFAULT_LOOP_DURATION_US = 10000;

TEST_GROUP(timing_measurement);
TEST_SETUP(timing_measurement)
{
}
TEST_TEAR_DOWN(timing_measurement) {}

TEST(timing_measurement, Given_Starting_When_NullPointer_Then_ReturnWithoutCoreDump)
{
  timingMeasureStart(NULL);
}

TEST(timing_measurement, When_StartAndStop_Then_ReturnLessThan1us)
{
  uint32_t start = 0;
  timingMeasureStart(&start);
  uint32_t elapsed_us = timingMeasureElapsedUs(start);
  TEST_ASSERT_LESS_OR_EQUAL_UINT32(1, elapsed_us);
}

TEST(timing_measurement, When_Waiting1ms_Then_ReturnAround1ms)
{
  uint32_t start = 0;
  timingMeasureStart(&start);

  platformDelayMs(1);
  uint32_t elapsed_us = timingMeasureElapsedUs(start);

  TEST_ASSERT_UINT32_WITHIN(
      ALLOWED_DELTA_US,
      MILLISECONDS_TO_MICROSECONDS(1),
      elapsed_us);
}

TEST(timing_measurement, Given_PlatformTimerOverflows_When_Waiting1ms_Then_ReturnAround1ms)
{
  uint32_t start = 0;
  const uint32_t LITTLE_WHILE_US = 100;
  platformDelayUs(UINT32_MAX-LITTLE_WHILE_US);

  timingMeasureStart(&start);
  platformDelayMs(1);
  uint32_t elapsed_us = timingMeasureElapsedUs(start);

  TEST_ASSERT_UINT32_WITHIN(
      ALLOWED_DELTA_US,
      MILLISECONDS_TO_MICROSECONDS(1),
      elapsed_us);
}

TEST_GROUP(looping_control);
TEST_SETUP(looping_control)
{
}
TEST_TEAR_DOWN(looping_control) {}

TEST(looping_control, When_CalledFirstTime_Then_WaitUpToLoopDuration)
{
  timing_loop_ctx_t ctx = {0};
  uint32_t start = 0;
  timingMeasureStart(&start);
  timingLoopInitialize(&ctx, DEFAULT_LOOP_DURATION_US);

  timingLoopControl(&ctx);

  uint32_t elapsed_us = timingMeasureElapsedUs(start);
  TEST_ASSERT_EQUAL(DEFAULT_LOOP_DURATION_US, elapsed_us);
}

TEST(looping_control, When_NothingElseConsumedTime_Then_WaitUpToLoopDuration)
{
  timing_loop_ctx_t ctx = {0};
  uint32_t start = 0;
  timingLoopInitialize(&ctx, DEFAULT_LOOP_DURATION_US);
  timingMeasureStart(&start);

  timingLoopControl(&ctx);

  uint32_t elapsed_us = timingMeasureElapsedUs(start);
  TEST_ASSERT_UINT32_WITHIN(ALLOWED_DELTA_US, DEFAULT_LOOP_DURATION_US, elapsed_us);
}

TEST(looping_control, When_SomethingAteHalfOfGivenTime_Then_WaitUpToLoopDuration)
{
  timing_loop_ctx_t ctx = {0};
  uint32_t start = 0;
  timingLoopInitialize(&ctx, DEFAULT_LOOP_DURATION_US);
  timingMeasureStart(&start);

  // some task that lasts half of duration
  platformDelayUs(DEFAULT_LOOP_DURATION_US/2);
  timingLoopControl(&ctx);

  uint32_t elapsed_us = timingMeasureElapsedUs(start);
  TEST_ASSERT_UINT32_WITHIN(ALLOWED_DELTA_US, DEFAULT_LOOP_DURATION_US, elapsed_us);
}

TEST(looping_control, When_DurationNotSet_Then_SaveError)
{
  timing_loop_ctx_t ctx = {0};
  timingLoopControl(&ctx);
  uint32_t errors = ctx.external.errors_number;

  TEST_ASSERT_EQUAL(1, errors);
}

TEST(looping_control, When_LoopTookTooLong_Then_DoNotWaitAndCountThatLoopAsTooLong)
{
  timing_loop_ctx_t ctx = {0};
  uint32_t start = 0;
  timingLoopInitialize(&ctx, DEFAULT_LOOP_DURATION_US);

  // some task that lasts whole duration
  platformDelayUs(DEFAULT_LOOP_DURATION_US);

  timingMeasureStart(&start);
  timingLoopControl(&ctx);

  uint32_t elapsed_us = timingMeasureElapsedUs(start);
  TEST_ASSERT_EQUAL(0, elapsed_us);
  TEST_ASSERT_EQUAL(1, ctx.external.too_long_total_loops_number);
  TEST_ASSERT_EQUAL(DEFAULT_LOOP_DURATION_US, ctx.external.previous_task_duration);
}

TEST(looping_control, Complex_When_LoopingWithDifferentTimeTask_Then_WaitProperlyAndMakeProperNumberOfLoops)
{
  const uint16_t TOTAL_LOOPS_NUMBER = 1234;
  timing_loop_ctx_t ctx = {0};
  uint32_t start = 0;
  timingLoopInitialize(&ctx, DEFAULT_LOOP_DURATION_US);
  timingMeasureStart(&start);

  const uint32_t TASK_DURATION[] = {
    1000, 995, 1005, 1000
  };

  for(uint16_t i=0; i<TOTAL_LOOPS_NUMBER; i++)
  {
    uint32_t task_duration_us = TASK_DURATION[i%(ARRAY_SIZE(TASK_DURATION))];
    // some task that lasts whole duration
    platformDelayUs(task_duration_us);
    timingLoopControl(&ctx);
  }

  uint32_t elapsed_us = timingMeasureElapsedUs(start);
  timingLoopPrintStats(&ctx);
  TEST_ASSERT_UINT32_WITHIN(ALLOWED_DELTA_US, TOTAL_LOOPS_NUMBER*DEFAULT_LOOP_DURATION_US, elapsed_us);
  TEST_ASSERT_EQUAL(TOTAL_LOOPS_NUMBER, ctx.external.total_loops_number);
  TEST_ASSERT_EQUAL(0, ctx.external.errors_number);
  TEST_ASSERT_EQUAL(0, ctx.external.too_long_total_loops_number);

  TEST_ASSERT_UINT32_WITHIN(1, 1000, ctx.external.task_duration_average_us);
  TEST_ASSERT_EQUAL(1005, ctx.external.task_duration_max);
  TEST_ASSERT_EQUAL(995, ctx.external.task_duration_min);
  TEST_ASSERT_EQUAL(995, ctx.external.previous_task_duration); // might fail when TOTAL_LOOPS_NUMBER changes
}

TEST_GROUP_RUNNER(looping_control)
{
  RUN_TEST_CASE(looping_control, When_CalledFirstTime_Then_WaitUpToLoopDuration)
  RUN_TEST_CASE(looping_control, When_NothingElseConsumedTime_Then_WaitUpToLoopDuration)
  RUN_TEST_CASE(looping_control, When_SomethingAteHalfOfGivenTime_Then_WaitUpToLoopDuration)
  RUN_TEST_CASE(looping_control, When_DurationNotSet_Then_SaveError)
  RUN_TEST_CASE(looping_control, When_LoopTookTooLong_Then_DoNotWaitAndCountThatLoopAsTooLong)
  RUN_TEST_CASE(looping_control, Complex_When_LoopingWithDifferentTimeTask_Then_WaitProperlyAndMakeProperNumberOfLoops);
}

TEST_GROUP_RUNNER(timing_measurement)
{
  RUN_TEST_CASE(timing_measurement, Given_Starting_When_NullPointer_Then_ReturnWithoutCoreDump)
  RUN_TEST_CASE(timing_measurement, When_StartAndStop_Then_ReturnLessThan1us)
  RUN_TEST_CASE(timing_measurement, When_Waiting1ms_Then_ReturnAround1ms)
  RUN_TEST_CASE(timing_measurement, Given_PlatformTimerOverflows_When_Waiting1ms_Then_ReturnAround1ms)
}

int main(void)
{
  UNITY_BEGIN();
  log_initialize(platformPutChar, LOGGER_LVL_INFO);

  RUN_TEST_GROUP(timing_measurement);
  RUN_TEST_GROUP(looping_control);


  return (UnityEnd());
}
