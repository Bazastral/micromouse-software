
#include "vector.h"
#include "unity.h"
#include "logger.h"
#include "platform_base.h"


void setUp(void)
{
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
}

void tearDown(void)
{
}

typedef struct {
  int32_t a;
  uint8_t b;
} item_t;

void vector_basic_operations(void)
{
  vector_t vect;
  char mem[100];

  item_t i1 = {513214,  10};
  item_t i2 = {398021,  0};
  item_t i3 = {784710,  11};
  item_t i4 = {429810,  12};
  item_t i5 = {103982,  13};
  item_t it;
  uint32_t index;

  // init
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_init(&vect, mem, sizeof(mem), sizeof(item_t)));
  TEST_ASSERT_EQUAL(0, vector_item_cnt(vect));

  // adding
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_add(&vect, &i1));
  TEST_ASSERT_EQUAL(1, vector_item_cnt(vect));
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 0, &it));
  TEST_ASSERT_EQUAL(i1.a, it.a); TEST_ASSERT_EQUAL(i1.b, it.b);

  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_add(&vect, &i2));
  TEST_ASSERT_EQUAL(2, vector_item_cnt(vect));
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 0, &it));
  TEST_ASSERT_EQUAL(i1.a, it.a); TEST_ASSERT_EQUAL(i1.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 1, &it));
  TEST_ASSERT_EQUAL(i2.a, it.a); TEST_ASSERT_EQUAL(i2.b, it.b);

  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_add(&vect, &i3));
  TEST_ASSERT_EQUAL(3, vector_item_cnt(vect));
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 0, &it));
  TEST_ASSERT_EQUAL(i1.a, it.a); TEST_ASSERT_EQUAL(i1.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 1, &it));
  TEST_ASSERT_EQUAL(i2.a, it.a); TEST_ASSERT_EQUAL(i2.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 2, &it));
  TEST_ASSERT_EQUAL(i3.a, it.a); TEST_ASSERT_EQUAL(i3.b, it.b);

  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_add(&vect, &i4));
  TEST_ASSERT_EQUAL(4, vector_item_cnt(vect));
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 0, &it));
  TEST_ASSERT_EQUAL(i1.a, it.a); TEST_ASSERT_EQUAL(i1.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 1, &it));
  TEST_ASSERT_EQUAL(i2.a, it.a); TEST_ASSERT_EQUAL(i2.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 2, &it));
  TEST_ASSERT_EQUAL(i3.a, it.a); TEST_ASSERT_EQUAL(i3.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 3, &it));
  TEST_ASSERT_EQUAL(i4.a, it.a); TEST_ASSERT_EQUAL(i4.b, it.b);

  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_add(&vect, &i5));
  TEST_ASSERT_EQUAL(5, vector_item_cnt(vect));
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 0, &it));
  TEST_ASSERT_EQUAL(i1.a, it.a); TEST_ASSERT_EQUAL(i1.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 1, &it));
  TEST_ASSERT_EQUAL(i2.a, it.a); TEST_ASSERT_EQUAL(i2.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 2, &it));
  TEST_ASSERT_EQUAL(i3.a, it.a); TEST_ASSERT_EQUAL(i3.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 3, &it));
  TEST_ASSERT_EQUAL(i4.a, it.a); TEST_ASSERT_EQUAL(i4.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 4, &it));
  TEST_ASSERT_EQUAL(i5.a, it.a); TEST_ASSERT_EQUAL(i5.b, it.b);

  // get indexes
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_get_item_idx(vect, &index, &i1));
  TEST_ASSERT_EQUAL(0, index);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_get_item_idx(vect, &index, &i2));
  TEST_ASSERT_EQUAL(1, index);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_get_item_idx(vect, &index, &i3));
  TEST_ASSERT_EQUAL(2, index);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_get_item_idx(vect, &index, &i4));
  TEST_ASSERT_EQUAL(3, index);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_get_item_idx(vect, &index, &i5));
  TEST_ASSERT_EQUAL(4, index);

  // removing - first
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_del_idx(&vect, 0));
  TEST_ASSERT_EQUAL(4, vector_item_cnt(vect));
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 0, &it));
  TEST_ASSERT_EQUAL(i2.a, it.a); TEST_ASSERT_EQUAL(i2.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 1, &it));
  TEST_ASSERT_EQUAL(i3.a, it.a); TEST_ASSERT_EQUAL(i3.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 2, &it));
  TEST_ASSERT_EQUAL(i4.a, it.a); TEST_ASSERT_EQUAL(i4.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 3, &it));
  TEST_ASSERT_EQUAL(i5.a, it.a); TEST_ASSERT_EQUAL(i5.b, it.b);

  // removing - inside
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_del_idx(&vect, 1));
  TEST_ASSERT_EQUAL(3, vector_item_cnt(vect));
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 0, &it));
  TEST_ASSERT_EQUAL(i2.a, it.a); TEST_ASSERT_EQUAL(i2.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 1, &it));
  TEST_ASSERT_EQUAL(i4.a, it.a); TEST_ASSERT_EQUAL(i4.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 2, &it));
  TEST_ASSERT_EQUAL(i5.a, it.a); TEST_ASSERT_EQUAL(i5.b, it.b);

  // removing - last
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_del_idx(&vect, 2));
  TEST_ASSERT_EQUAL(2, vector_item_cnt(vect));
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 0, &it));
  TEST_ASSERT_EQUAL(i2.a, it.a); TEST_ASSERT_EQUAL(i2.b, it.b);
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 1, &it));
  TEST_ASSERT_EQUAL(i4.a, it.a); TEST_ASSERT_EQUAL(i4.b, it.b);

  // clear vector
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_clear(&vect));
  TEST_ASSERT_EQUAL(0, vector_item_cnt(vect));

  // initialize again
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_init(&vect, mem, sizeof(mem), sizeof(item_t)));
  TEST_ASSERT_EQUAL(0, vector_item_cnt(vect));
}

void vector_error_operations(void)
{
  vector_t vect;
  char mem[sizeof(item_t) * 15];
  item_t it = {0, 0};
  log_debug("vector_error_operations");
  uint32_t index;

  // init empty or too small
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, vector_init(&vect, NULL, sizeof(mem), sizeof(item_t)));
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, vector_init(&vect, mem, 0, sizeof(item_t)));
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, vector_init(&vect, mem, sizeof(item_t) - 1, sizeof(item_t)));
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, vector_init(&vect, mem, sizeof(item_t) - 1, 0));
  // get index from not inited vector
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, vector_get_item_idx(vect, &index, &it));

  // init properly for next tests
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_init(&vect, mem, sizeof(mem), sizeof(item_t)));

  // get index when no items
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, vector_get_item_idx(vect, &index, &it));

  // try to delete item when no items available
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, vector_del_idx(&vect, 0));

  // full array of items
  for(unsigned int i=0; i < vect.itemTotalAvailable; i++)
  {
    it.a = i; it.b = i;
    TEST_ASSERT_EQUAL(RET_SUCCESS, vector_add(&vect, &it));
    TEST_ASSERT_EQUAL(i+1, vector_item_cnt(vect));
    // try to delete item that not exist
    TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, vector_del_idx(&vect, i+1));
  }

  // add item when no space
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, vector_add(&vect, &it));

  // get index of item that not exists
  it.a = 123; it.b = 42;
  TEST_ASSERT_NOT_EQUAL(RET_SUCCESS, vector_get_item_idx(vect, &index, &it));
}

void vector_reverse(void)
{
  vector_t vect;
  char mem[100];
  item_t i1 = {513214,  10};
  item_t i2 = {398021,  0};

  // init
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_init(&vect, mem, sizeof(mem), sizeof(item_t)));
  TEST_ASSERT_EQUAL(0, vector_item_cnt(vect));
  // add
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_add(&vect, &i1));
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_add(&vect, &i2));

  //reverse
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_turnover(&vect));
  TEST_ASSERT_EQUAL(RET_SUCCESS, vector_at(vect, 0, &i1));
  TEST_ASSERT_EQUAL(i1.a, i2.a);
  TEST_ASSERT_EQUAL(i1.b, i2.b);

}

int main(void)
{
  UNITY_BEGIN();
  RUN_TEST(vector_basic_operations);
  RUN_TEST(vector_error_operations);
  RUN_TEST(vector_reverse);


  return (UnityEnd());
}
