

#include "useful.h"


#include "unity.h"

void setUp(void)
{
}

void tearDown(void)
{
}




void normalizeRad_test(void)
{
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, 0, normalizeRad(0));
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, M_PI/4, normalizeRad(M_PI/4));
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, M_PI/2, normalizeRad(M_PI/2));
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, 3*M_PI/4, normalizeRad(3*M_PI/4));
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, M_PI, normalizeRad(M_PI));

  TEST_ASSERT_DOUBLE_WITHIN(1e-8, -M_PI/4, normalizeRad(-M_PI/4));
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, -M_PI/2, normalizeRad(-M_PI/2));
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, -3*M_PI/4, normalizeRad(-3*M_PI/4));
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, M_PI, normalizeRad(-M_PI));

  TEST_ASSERT_DOUBLE_WITHIN(1e-8, 0, normalizeRad(2*M_PI));
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, 0, normalizeRad(4*M_PI));
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, 0, normalizeRad(400*M_PI));

  TEST_ASSERT_DOUBLE_WITHIN(1e-8, 1, normalizeRad(2*M_PI + 1));
  TEST_ASSERT_DOUBLE_WITHIN(1e-8, -1, normalizeRad(2*M_PI - 1));
}

void atan2_test(void)
{
  // make sure that atan2 works as expected
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, 0,        atan2(0, 0));
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, M_PI/2,   atan2(1, 0));
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, M_PI/4,   atan2(1, 1));
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, 0,        atan2(0, 1));
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, -M_PI/4,  atan2(-1, 1));
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, -M_PI/2,  atan2(-1, 0));
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, M_PI,     atan2(0, -1));
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, 3*M_PI/4, atan2(1, -1));
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, 3*M_PI/4, atan2(1, -1));
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, M_PI,     atan2(0.000000000001, -1));
  TEST_ASSERT_DOUBLE_WITHIN(1e-10, -M_PI,    atan2(-0.000000000001, -1));
}

void normal_distribution(void)
{
  for(uint16_t i = 0; i < 1000; i++)
  {
    printf("%d; ",(int) (10000*generate_normal_distribution(0, 1)));
  }
}


int main(void)
{
  UNITY_BEGIN();
  RUN_TEST(normalizeRad_test);
  RUN_TEST(atan2_test);
  RUN_TEST(normal_distribution);

  return (UnityEnd());
}
