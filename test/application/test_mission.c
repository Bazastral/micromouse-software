
#include "unity.h"
#include "unity_fixture.h"


#include "labirynth.h"
#include "labirynths.h"
#include "state.h"
#include "platform_state.h"
#include "config.h"
#include "mission.h"
#include "platform_base.h"
#include "executor.h"
#include "exhibit.h"

#include "executor_mock.h"
#include "labirynth_alter_mock.h"
#include "path_planner_mock.h"
#include "logger.h"

/**
 * @brief I've implemented this module not in TDD framework...
 * my bad!
 */


TEST_GROUP(missionAchieveGoal);
TEST_SETUP(missionAchieveGoal)
{
  executorMockInit();
}
TEST_TEAR_DOWN(missionAchieveGoal)
{

}

TEST(missionAchieveGoal, When_Initialized_Then_InitializeLowerLayers)
{
  TEST_ASSERT_EQUAL(0, executorInitMockCalledTimes());
  state_t state = stateStartingPoint();
  missionInit(state, labCoordsGoalNE());
  TEST_ASSERT_EQUAL(1, executorInitMockCalledTimes());
}

TEST_GROUP_RUNNER(missionAchieveGoal)
{
  RUN_TEST_CASE(missionAchieveGoal, When_Initialized_Then_InitializeLowerLayers);
}

TEST_GROUP(missionPathplanning);
TEST_SETUP(missionPathplanning)
{
  executorMockInit();
  pp_mock_initialize();
}
TEST_TEAR_DOWN(missionPathplanning)
{
}


TEST(missionPathplanning, When_Inited_Then_MazeChangeCallbackIsRegistered)
{
  missionInitDefault();

  TEST_ASSERT_NOT_EQUAL(NULL, labMockGetChangeCallback());
}

TEST(missionPathplanning, When_MainThreadBeginning_Then_FindPathToMiddle)
{
  missionInitDefault();

  missionCompetitionMainThread();

  TEST_ASSERT_EQUAL(1, pp_mock_find_path_call_counter());
  TEST_ASSERT_EQUAL(true, labCoordsEqual(labCoordsStartingPoint(), pp_mock_find_path_param_from()));
  TEST_ASSERT_EQUAL(true, labCoordsEqual(labCoordsGoalNE(), pp_mock_find_path_param_to()));
}

TEST(missionPathplanning, When_MazeUpdatedOnlyOnce_Then_DoNotReplanPath)
{
  missionInitDefault();
  missionCompetitionMainThread();
  pp_mock_initialize();

  labMockGetChangeCallback()();
  missionCompetitionMainThread();

  TEST_ASSERT_EQUAL(0, pp_mock_find_path_call_counter());
}

TEST(missionPathplanning, When_MazeUpdatedOnlyTwice_Then_DoNotReplanPath)
{
  missionInitDefault();
  missionCompetitionMainThread();
  pp_mock_initialize();

  labMockGetChangeCallback()();
  labMockGetChangeCallback()();
  missionCompetitionMainThread();

  TEST_ASSERT_EQUAL(0, pp_mock_find_path_call_counter());
}

TEST(missionPathplanning, When_MazeUpdatedThreeTimes_Then_ReplanPath)
{
  missionInitDefault();
  missionCompetitionMainThread();
  pp_mock_initialize();

  labMockGetChangeCallback()();
  labMockGetChangeCallback()();
  labMockGetChangeCallback()();
  missionCompetitionMainThread();

  TEST_ASSERT_EQUAL(1, pp_mock_find_path_call_counter());
}

TEST(missionPathplanning, When_MazeUpdatedOnlyOnceAndManyLoopsExecuted_Then_ReplanPath)
{
  const int LOOP_EXECUTION_TO_PATH_REPLAN = 5;
  missionInitDefault();
  missionCompetitionMainThread();
  pp_mock_initialize();

  labMockGetChangeCallback()();
  for(int i=0; i<LOOP_EXECUTION_TO_PATH_REPLAN; i++)
  {
    missionCompetitionMainThread();
  }

  TEST_ASSERT_EQUAL(1, pp_mock_find_path_call_counter());
}

TEST_GROUP_RUNNER(missionPathplanning)
{
  RUN_TEST_CASE(missionPathplanning, When_Inited_Then_MazeChangeCallbackIsRegistered);
  RUN_TEST_CASE(missionPathplanning, When_MainThreadBeginning_Then_FindPathToMiddle);
  RUN_TEST_CASE(missionPathplanning, When_MazeUpdatedOnlyOnce_Then_DoNotReplanPath);
  RUN_TEST_CASE(missionPathplanning, When_MazeUpdatedThreeTimes_Then_ReplanPath);
  RUN_TEST_CASE(missionPathplanning, When_MazeUpdatedOnlyOnceAndManyLoopsExecuted_Then_ReplanPath);
}

int main(void)
{
  UNITY_BEGIN();
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);

  RUN_TEST_GROUP(missionPathplanning);
  RUN_TEST_GROUP(missionAchieveGoal);

  int failures = UnityEnd();
  exhibitFailures(failures);

  return failures;
}