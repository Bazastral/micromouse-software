
#include "platform_printer.h"

ret_code_t printPathPlannerPoints(vector_t points)
{
  (void) points;
  return RET_ERR_NOT_IMPLEMENTED;
}

ret_code_t printPathPlannerCells(vector_t lab_cells)
{
  (void) lab_cells;
  // nothing to do
  return RET_ERR_NOT_IMPLEMENTED;
}

ret_code_t printOnlyOneWallNotification(const lab_cellxy_t coords, const lab_cell_t cell, const bool exists)
{
  (void) coords;
  (void) cell;
  (void) exists;
  return RET_SUCCESS;
}