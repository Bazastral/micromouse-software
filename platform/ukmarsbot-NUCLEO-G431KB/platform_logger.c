

/**
 * Need to implement circullar buffer to log characters one by one
 * So the CPU is not used all the time and HW mostly handles it
 */


#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "platform_base.h"
#include "platform_logger.h"
#include "logger.h"
#include "main.h"

#define LOGGING_BUFFER_SIZE 450
/**
 * @brief New items are moving head.
 * Tail is trying to meet head.
 * HEAD == TAIL   -> empty
 * HEAD == TAIL-1 -> full (max size is one less than buffer size)
 */
static volatile uint16_t head;
static volatile uint16_t tail;
static volatile bool loggingOngoing;
static char logging_buffer[LOGGING_BUFFER_SIZE];

_Static_assert(LOGGING_BUFFER_SIZE<= UINT16_MAX, "buffer must fit 16 bits");

void platformLoggerInit(void)
{
  head = 0;
  tail = 0;
  loggingOngoing = false;
  log_initialize(platformPutChar, LOGGER_LVL_DEBUG);
  log_raw("\n\n\n\n\n****************\n");
}

static bool loggingBufferIsFull(void)
{
  // make sure overflow will be handled properly
  if ((head + LOGGING_BUFFER_SIZE - tail) % LOGGING_BUFFER_SIZE >= LOGGING_BUFFER_SIZE - 1)
  {
    return true;
  }

  return false;
}

static bool loggingBufferIsEmpty(void)
{
  if (tail == head)
  {
    return true;
  }

  return false;
}

static void loggingBufferPut(char c)
{
  logging_buffer[head] = c;

  __disable_irq();
  head++;
  head = head % LOGGING_BUFFER_SIZE;
  __enable_irq();
}

char loggingBufferGet(void)
{
  char ret = logging_buffer[tail];

  __disable_irq();
  tail++;
  tail = tail % LOGGING_BUFFER_SIZE;
  __enable_irq();
  return ret;
}

static void loggingBufferClear(void)
{
  __disable_irq();
  tail = head;
  __enable_irq();
}

static void loggingTransmitNextChar(void)
{
  HAL_StatusTypeDef status;
  char ch = loggingBufferGet();
  loggingOngoing = true;

  status = HAL_UART_Transmit_IT(&huart1, (uint8_t *)&ch, sizeof(uint8_t));
  if (status != HAL_OK)
  {
    loggingBufferClear();
  }
}

// used by Unity only
void platformLoggerFlush(void)
{
  while (loggingBufferIsEmpty() == false)
  {
  }
}

void platformPutChar(const char c)
{
  if (loggingBufferIsFull() == false)
  {
    loggingBufferPut(c);

    if (loggingOngoing == false)
    {
      loggingTransmitNextChar();
    }
  }
  else
  {
    loggingBufferClear();
    // notify error
    platformPutChar('#');
    platformPutChar('!');
    platformPutChar('#');
    platformPutChar('\n');
  }
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  (void)huart;

  if (loggingBufferIsEmpty())
  {
    loggingOngoing = false;
  }
  else
  {
    loggingTransmitNextChar();
  }
}
