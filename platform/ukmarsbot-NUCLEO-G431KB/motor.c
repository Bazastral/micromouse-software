#include <math.h>
#include <stdbool.h>

#include "main.h"
#include "stm32g4xx.h"

#include "motor.h"
#include "config.h"


#define MOTOR_LEFT 0
#define MOTOR_RIGHT 1
#define SPEED_MULTIPLIER 1.0

typedef struct
{
  GPIO_TypeDef *clk_gpio_port;
  GPIO_TypeDef *b_gpio_port;
  uint16_t clk_gpio_pin;
  uint16_t b_gpio_pin;
} ENCODER_CONSTS_t;


/**
 * @brief Based on pin states and previous pin states decide to increment or decrement ticks
 */
static void incrementTicks(bool right);

/**
 * @brief Set speed of one motor
 */
static void setOneMotorSpeed(double speed, bool right);

const ENCODER_CONSTS_t encoders[MOTOR_NUMBER] = {
    {
        .b_gpio_port = ENCODER_L_B_GPIO_Port,
        .b_gpio_pin = ENCODER_L_B_Pin,
        .clk_gpio_port = ENCODER_L_CLK_GPIO_Port,
        .clk_gpio_pin = ENCODER_L_CLK_Pin,
    },
    {
        .b_gpio_port = ENCODER_R_B_GPIO_Port,
        .b_gpio_pin = ENCODER_R_B_Pin,
        .clk_gpio_port = ENCODER_R_CLK_GPIO_Port,
        .clk_gpio_pin = ENCODER_R_CLK_Pin,
    }};

static volatile int32_t ticks_general_invalid;
static volatile int32_t ticks_exti[MOTOR_NUMBER];
static GPIO_PinState prev_clk[MOTOR_NUMBER];
static GPIO_PinState prev_b[MOTOR_NUMBER];

static GPIO_TypeDef * MOTOR_DIR_PORT[MOTOR_NUMBER] =
{
  MOTOR_L_DIR_GPIO_Port,
  MOTOR_R_DIR_GPIO_Port
};
static const uint16_t MOTOR_DIR_PIN[MOTOR_NUMBER] =
{
  MOTOR_L_DIR_Pin,
  MOTOR_R_DIR_Pin
};
static const uint32_t MOTOR_TIMER_CHANNEL[MOTOR_NUMBER] =
{
  TIM_CHANNEL_1,
  TIM_CHANNEL_4,
};


static void setOneMotorSpeed(double speed, bool right)
{
  if(speed > MAX_MOTOR_SPEED)
  {
    log_warn("MOTOR: wrong speed! %f > %f", speed, MAX_MOTOR_SPEED);
    speed = MAX_MOTOR_SPEED;
  }
  if(speed < -MAX_MOTOR_SPEED)
  {
    log_warn("MOTOR: wrong speed! %f < %f", speed, -MAX_MOTOR_SPEED);
    speed = -MAX_MOTOR_SPEED;
  }
  // set direction pin
  bool direction = (speed >= 0);
  HAL_GPIO_WritePin(MOTOR_DIR_PORT[right], MOTOR_DIR_PIN[right], direction);

  // set timer PWM
  speed = fabs(speed);
  speed = speed * SPEED_MULTIPLIER;
	uint16_t ccr = speed * ((float)htim1.Init.Period + 1.0f);
  __HAL_TIM_SET_COMPARE(&htim1, MOTOR_TIMER_CHANNEL[right], ccr);
}

ret_code_t motorSetSpeed(motors_speed_t values)
{
  setOneMotorSpeed(values.left, MOTOR_LEFT);
  setOneMotorSpeed(values.right, MOTOR_RIGHT);
  // log_debug("Motor speeds set to: L=%0.2f R=%0.2f", values.left, values.right);
  return RET_SUCCESS;
}

ret_code_t motorGetTicks(motors_ticks_t *ticks)
{
  // log_debug("left %+4d right %+4d invalid %4d",
    // (int)ticks_exti[MOTOR_LEFT], (int)ticks_exti[MOTOR_RIGHT], (int)ticks_general_invalid);

  __disable_irq();
  ticks->left = ticks_exti[MOTOR_LEFT];
  ticks->right = ticks_exti[MOTOR_RIGHT];

  ticks_exti[MOTOR_LEFT] = 0;
  ticks_exti[MOTOR_RIGHT] = 0;
  ticks_general_invalid = 0;
  __enable_irq();


  return RET_SUCCESS;
}

typedef enum{
  DIR_FORWARD,
  DIR_BCKWARD,
  DIR_INVALID,
} DIR_DIR_t;

DIR_DIR_t EncoderDirectionDetectionMapping[2][2][2][2] =
  {
                                  /* {CLK = 0, B=0/1},                 {CLK = 1, B=0/1}*/
/* PREV_CLK=0 PREV_B=0 */ { { { DIR_INVALID, DIR_INVALID, }, { DIR_FORWARD, DIR_BCKWARD, }, },
/* PREV_CLK=0 PREV_B=1 */ { {   DIR_INVALID, DIR_INVALID, }, { DIR_BCKWARD, DIR_FORWARD, }, }, },
/* PREV_CLK=1 PREV_B=0 */ { { { DIR_BCKWARD, DIR_FORWARD, }, { DIR_INVALID, DIR_INVALID, }, },
/* PREV_CLK=1 PREV_B=1 */ { {   DIR_FORWARD, DIR_BCKWARD, }, { DIR_INVALID, DIR_INVALID, }, }, },
};

void HAL_GPIO_EXTI_Callback(uint16_t gpio_pin)
{
  if(gpio_pin == ENCODER_R_CLK_Pin)
  {
    incrementTicks(MOTOR_RIGHT);
  }
  else if(gpio_pin == ENCODER_L_CLK_Pin)
  {
    incrementTicks(MOTOR_LEFT);
  }
  else
  {
    ticks_general_invalid++;
  }
}

static void incrementTicks(bool right)
{
    GPIO_PinState clk, b;
    clk = HAL_GPIO_ReadPin(encoders[right].clk_gpio_port, encoders[right].clk_gpio_pin);
    b = HAL_GPIO_ReadPin(encoders[right].b_gpio_port  , encoders[right].b_gpio_pin);

    DIR_DIR_t direction = EncoderDirectionDetectionMapping[prev_clk[right]][prev_b[right]][clk][b];
    if(direction == DIR_FORWARD)
    {
      if(right)
      {
        ticks_exti[right]--;
      }
      else
      {
        ticks_exti[right]++;
      }
    }
    else if(direction == DIR_BCKWARD)
    {
      if(right)
      {
        ticks_exti[right]++;
      }
      else
      {
        ticks_exti[right]--;
      }
    }
    else
    {
      ticks_general_invalid++;
    }
    prev_clk[right] = clk;
    prev_b[right] = b;
}