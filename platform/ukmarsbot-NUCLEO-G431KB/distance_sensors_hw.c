
#include "stm32g4xx_hal.h"

#include "ret_codes.h"
#include "main.h"
#include "useful.h"
#include "platform_base.h"
#include "math.h"
#include "distance_sensors.h"
#include "distance_sensors_hw.h"

static volatile bool ConversionDone = false;

void TurnIrOn(void)
{
  HAL_GPIO_WritePin(LED_SENSOR_DIST_GPIO_Port, LED_SENSOR_DIST_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(LED_NUCLEO_GPIO_Port, LED_NUCLEO_Pin, GPIO_PIN_SET);
}

void TurnIrOff(void)
{
  HAL_GPIO_WritePin(LED_SENSOR_DIST_GPIO_Port, LED_SENSOR_DIST_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED_NUCLEO_GPIO_Port, LED_NUCLEO_Pin, GPIO_PIN_RESET);
}

#define REFERENCE_MIN_CM 2
#define REFERENCE_MAX_CM 19
#define REFERENCE_ENTRIES (REFERENCE_MAX_CM - REFERENCE_MIN_CM + 1)
double ReadingToDistance(uint8_t led_id, uint16_t reading)
{
  /**
   * @brief A map of distance->reading
   * starting from 19 cm, then going down to 2 cm
   * got from many hw tests
   * https://docs.google.com/spreadsheets/d/1-vQRXz1JoSx8ub0733uJxIVzZ1Kiq49H88ZRTM-nruM/edit#gid=1801395835
   */
  const double ReferenceReadings[DISTANCE_SENSORS_NUMBER][REFERENCE_ENTRIES] =
      {
          {
              4096, // changed manually
              3087,
              2840,
              2465,
              2102,
              1814,
              1562,
              1371,
              1216,
              1094,
              1001,
              921,
              852,
              795,
              748,
              708,
              672,
              646,
          },
          {
              // 4096, // changed manually
              // 4090, // changed manually
              // 4071,
              // 3633,
              // 3120,
              // 2729,
              // 2395,
              // 2138,
              // 1923,
              // 1761,
              // 1632,
              // 1526,
              // 1437,
              // 1363,
              // 1303,
              // 1250,
              // 1209,
              // 1173,
              4095,
              4095,
              3825,
              3340,
              2927,
              2564,
              2247,
              2010,
              1817,
              1662,
              1543,
              1439,
              1359,
              1289,
          },
          {
              4096, // changed manually
              1868,
              1800,
              1629,
              1428,
              1250,
              1087,
              955,
              840,
              752,
              676,
              616,
              563,
              520,
              486,
              456,
              430,
              409,
          }};

  double distance = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM;

  for(uint8_t i = 1; i<REFERENCE_ENTRIES; i++)
  {
    if(reading > ReferenceReadings[led_id][i])
    {
      // linearize around this one cetimeter
      distance = i + REFERENCE_MIN_CM;
      distance -= (double)(reading - ReferenceReadings[led_id][i]) /
                  (double)((ReferenceReadings[led_id][i - 1]) - ReferenceReadings[led_id][i]);
      distance *= 10; // change cm to mm
      break;
    }
  }
  if(distance > DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM)
  {
    distance = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM;
  }

  return distance;
}

ret_code_t getSensorReadings(uint16_t buf[], uint8_t numberOfSamples)
{
  HAL_StatusTypeDef hal_status;

  ConversionDone = false;
  hal_status = HAL_ADC_Start_DMA(&hadc2, (uint32_t *)buf, numberOfSamples);
  if (hal_status != HAL_OK)
  {
    log_warn("DS: start problem = %3d ", hal_status);
    return RET_ERR_OTHER;
  }

  while (ConversionDone == false)
  {
    // log_warn("DS: waiting ");
    // This implementation is quite stupid, but it is "good enough" and works
  }

  return RET_SUCCESS;
}

ret_code_t dsGetDistances(double *dists, int len)
{
  ret_code_t ret = RET_SUCCESS;
  uint16_t ReadingsBackground[DISTANCE_SENSORS_NUMBER];
  uint16_t ReadingsWithIR[DISTANCE_SENSORS_NUMBER];
  uint16_t ReadingsDiff[DISTANCE_SENSORS_NUMBER];


  if(len < DISTANCE_SENSORS_NUMBER)
  {
    ret = RET_ERR_NO_MEM;
  }
  if(dists == NULL)
  {
    ret = RET_ERR_INVALID_PARAM;
  }

  if(ret == RET_SUCCESS)
  {
    // no wait needed, as probably there passed a lot of time from last calling this function
    ret = getSensorReadings(ReadingsBackground, DISTANCE_SENSORS_NUMBER);
  }

  if(ret == RET_SUCCESS)
  {
    TurnIrOn();
    platformDelayUs(100);
    ret = getSensorReadings(ReadingsWithIR, DISTANCE_SENSORS_NUMBER);
    TurnIrOff();
  }

  if(ret == RET_SUCCESS)
  {
    for(uint8_t i = 0; i<DISTANCE_SENSORS_NUMBER; i++)
    {
      ReadingsDiff[i] = ReadingsWithIR[i] - ReadingsBackground[i];
      dists[i] = ReadingToDistance(i, ReadingsDiff[i]);
      log_debug("DS: %d w: %5d b: %5d d: %5d. Dist=%4.1f",
                i,
                ReadingsWithIR[i],
                ReadingsBackground[i],
                ReadingsDiff[i],
                dists[i]);
    }
  }
  log_debug("DS: L=%3.1fcm  M=%3.1fcm  R=%3.1fcm",
            dists[0],
            dists[1],
            dists[2]
            );
  log_debug("------%d", ret);
  return ret;
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
  (void)hadc;
  ConversionDone = true;
}


dsConfig_t dsGetCfgPlatformDefault(void)
{
  static dsConfig_t defaultCfg =
  {
    .numberOfSensors = 3,
    .sensors =
    {
      //left
      {.offset = {-40, 50, DEG_2_RAD(52)}},
      //middle
      {.offset = {     0, 61, 0}},
      //right
      {.offset = { 41, 49, DEG_2_RAD(-62)}}
    },
    .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
    .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
    .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS
  };

  return defaultCfg;
}

void dsRegisterNoiseFunction(noiseFunction nf)
{
  (void)nf;
  log_warn("DS: not implemented noise function on this platform");
}