

#include "ret_codes.h"
#include "labirynths.h"
#include "logger.h"

ret_code_t labirynthSetup(LABIRYNTHS_NAME name)
{
  log_info("The labirynth should be set to: %d", name);
  return RET_SUCCESS;
}
