

#include <math.h>
#include <stddef.h>
#include <stdbool.h>

#include "config.h"

#define RADIUS_MM 16.00
#define IMPULSES_PER_ENCODER_ROTATION 12
#define MOTOR_GEAR 50

static const config_t cfg =
{
  // .whlTrack        = 67.07, from theory
  .whlTrack        = 72.0, // from experiments
  .whlTicksPerTurn = IMPULSES_PER_ENCODER_ROTATION*MOTOR_GEAR,

  .whlRadius       = RADIUS_MM,
  .whlCirc         = RADIUS_MM * 2 * M_PI,

  .linearPIDp = 0.0010,
  .linearPIDi = 0.02,
  .linearPIDd = 0.000008,
  // OR
  // .linearPIDp = 0.0022,
  // .linearPIDi = 0.04,
  // .linearPIDd = 0.000008,
  // OR
  // .linearPIDp = 0.0015,
  // .linearPIDi = 0.009,
  // .linearPIDd = 0.000004,

  .angularPIDp = 0.050,
  .angularPIDi = 0.7,
  .angularPIDd = 0.0002,
  // .angularPIDp = 0.10,
  // .angularPIDi = 0.1,
  // .angularPIDd = 0.000,
};


const config_t* configGet(void)
{
  return &cfg;
}
