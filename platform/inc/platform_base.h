#ifndef PLATFORM_H__
#define PLATFORM_H__

#include <stdint.h>
#include <stdbool.h>

/*
 * Initialize platform
 */
void platformInit(void);

/*
 * Delay
 */
void platformDelayMs(uint32_t ms);
void platformDelayUs(uint32_t us);

/**
 * @brief Loop with period. Instead of dummy wait (platformDelayMs)
 * use this function. It will count how many ms FW should wait more,
 * so that a loop will be executed every ms milliseconds
 *
 * @param ms     period time in milliseconds
 * @return true  if success
 * @return false if previous run was longer then ms
 */
bool platformLoopWithSpecifiedPeriod(uint32_t ms);

/*
 * On this function relies all logger module
 */
void platformPutChar(const char c);

/*
 * Turn on or off specific led
 * This API tries to be platform independent, so idexes are specified by platform
*/
void platformSetLedState(uint8_t led_idx, bool on);

/**
 * @brief Check if battery is empty.
 *
 * Return true if the battery is empty
 */
bool platformCheckIfBatteryIsEmpty(void);

/**
 * @brief Get how many microseconds elapsed from initialization
 */
uint32_t platformGetElapsedUsFromInit(void);

/**
 * @brief Get state of a button at specified index
 *
 * @param idx index of a button
 *
 * @return true if the button is being pushed at the moment
 */
bool platoformIsButtonPressed(uint8_t idx);

#endif // PLATFORM_H__