/**
 * @brief This module is HW layer, which allows to set motor speeds
 * and get encoder ticks.
 */
#ifndef MOTOR_H__
#define MOTOR_H__

#include "ret_codes.h"

/**
 * @note Motor speeds MUST BE WITHIN <-1, 1>
 * Meaning:
 * -1: drive max speed backward,
 * 1: drive max speed forward,
 * 0: do not move
 */
#define MAX_MOTOR_SPEED  (double)1.0

typedef enum
{
  MOTOR_LEFT,
  MOTOR_RIGHT
} motor_t;

/*
 * -1 drive back full speed
 *  0 stop
 *  1 drive forward full speed
 */
typedef struct{
  float left;
  float right;
} motors_speed_t;

/*
 * Number of encoder ticks from last read
 */
typedef struct{
  int32_t left;
  int32_t right;
} motors_ticks_t;

/*
 * Set motor power
 * values must be in range <-1, 1>
 */
ret_code_t motorSetSpeed(motors_speed_t values);

/*
 * Get number of tics in encoder
 * Load this number to @ticks
 */
ret_code_t motorGetTicks(motors_ticks_t *ticks);



#endif // MOTOR_H__
