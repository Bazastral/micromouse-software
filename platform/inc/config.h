#ifndef CONFIG__H__
#define CONFIG__H__


#define MOTOR_NUMBER 2


typedef struct config{
  double whlTrack;           //wheel track. Distance between middles of both wheels [mm]
  double whlRadius;          //radius of the wheels [mm]
  double whlTicksPerTurn;    //encoder resolution

  double linearPIDp;
  double linearPIDi;
  double linearPIDd;

  double angularPIDp;
  double angularPIDi;
  double angularPIDd;

  // based on previous values
  double whlCirc;   //circumverence of the wheels [mm]
} config_t;


/**
 * Get configuration
 */
const config_t* configGet(void);



#endif // CONFIG__H__
