#pragma once

#include <stdint.h>
#include <stdbool.h>

/**
 * Initialize platform logger
 *
 * Need to implement circullar buffer to log characters one by one
 */
void platformLoggerInit(void);

/**
 * Blocking wait until all data in buffer is logged (sent)
 */
void platformLoggerFlush(void);
