#pragma once


#include "ret_codes.h"
#include "point.h"
#include "vector.h"
#include "state.h"
#include "distance_sensors.h"


/**
 * Get distances
 *
 * @returns distances measured by all sensors in mm.
 */
ret_code_t dsGetDistances(double* dists, int len);

/**
 * @brief Get default platform distance sensors configuration
 */
dsConfig_t dsGetCfgPlatformDefault(void);

typedef double (*noiseFunction)(double);

/**
 * @brief This is rather for testing purposes
 *
 * @param nf
 */
void dsRegisterNoiseFunction(noiseFunction nf);
