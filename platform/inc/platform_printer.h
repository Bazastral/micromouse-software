#pragma once

#include "ret_codes.h"
#include "labirynth_utils.h"
#include "vector.h"

/**
 * @brief Show the path
 * depends on the system / bsp
 *
 * @param[in] points vector of point_t
 */
ret_code_t printPathPlannerPoints(vector_t points);

/**
 * @brief Show the path
 * @param[in] lab_cells vector of lab_cellxy_t
 */
ret_code_t printPathPlannerCells(vector_t lab_cells);

/**
 * @brief Notify change of one wall in the maze knowledge
 * Useful on simulator - not to update all maze, but only one specific wall
 *
 * @param coords        coordinates of the cell
 * @param cell          mask of walls that changed
 * @param exists        1-exists, 0-there is no wall
 * @return RET_SUCCESS on success
 */
ret_code_t printOnlyOneWallNotification(const lab_cellxy_t coords, const lab_cell_t cell, const bool exists);
