
#include <math.h>
#include <stdbool.h>

#include "distance_sensors_hw.h"
#include "useful.h"
#include "simulator_broker.h"
#include "ret_codes.h"


typedef double (*noiseFunction)(double);

noiseFunction nf_ = NULL;
void dsRegisterNoiseFunction(noiseFunction nf)
{
  nf_ = nf;
}

ret_code_t dsGetDistances(double* dists, int len)
{
  ret_code_t ret = RET_SUCCESS;
  dsConfig_t *cfg = dsGetCfg();
  if(len < cfg->numberOfSensors)
  {
    return RET_ERR_NO_MEM;
  }
  ret = sim_get_param(SIM_PARAM_DIST_SENSORS, (void*) dists);
  if(ret == RET_SUCCESS)
  {
    if(nf_ != NULL)
    {
      for(int i = 0; i < len; i++)
      {
        dists[i] = nf_(dists[i]);
      }
    }
  }
  return ret;
}

dsConfig_t dsGetCfgPlatformDefault(void)
{
  dsConfig_t defaultCfg =
  {
    .numberOfSensors = 3,
    .sensors =
    {
      //left
      {.offset = {-40, 50, DEG_2_RAD(52)}},
      //middle
      {.offset = {     0, 61, 0}},
      //right
      {.offset = { 41, 49, DEG_2_RAD(-62)}}
    },
    .max_range_mm = DISTANCE_SENSOR_DFEAULT_MAX_RANGE_MM,
    .min_range_mm = DISTANCE_SENSOR_DFEAULT_MIN_RANGE_MM,
    .allowed_measurement_angle_deg = DISTANCE_SENSOR_ALLOWED_ANGLE_DEGS
  };

  return defaultCfg;
}