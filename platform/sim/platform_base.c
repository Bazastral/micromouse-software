


#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>

#include "platform_base.h"
#include "platform_logger.h"
#include "simulator_broker.h"
#include "config.h"
#include "distance_sensors.h"
#include "distance_sensors_hw.h"
#include "led.h"
#include "exhibit.h"
#include "useful.h"
#include "logger.h"

static uint64_t simulated_time_us = 0;

void platformInit(void)
{
  platformLoggerInit();
  log_set_verbosity_level(LOGGER_LVL_INFO);

  sim_set_param(SIM_PARAM_CONFIG, configGet());
  dsInit(dsGetCfgPlatformDefault());
  sim_set_param(SIM_PARAM_CONFIG_DISTANCE_SENSORS, dsGetCfg());
  sim_set_param(SIM_PARAM_NOTIFY_WALL_CLEAR, NULL);
  sim_set_param(SIM_PARAM_CLEAR_CELL_TEXTS, NULL);
  sim_set_param(SIM_PARAM_PATH, NULL);
  sim_set_param(SIM_PARAM_PATH, NULL);
  state_t initial_state = stateStartingPoint();
  sim_set_param(SIM_PARAM_STATE, &initial_state);
  motorSetSpeed((motors_speed_t){0, 0});
  exhibitEvent(EXHIBIT_PLATFORM_INITIALIZED);
}

void platformDelayUs(uint32_t us)
{
  uint64_t time_prev_ms = MICROSECONDS_TO_MILLISECONDS(simulated_time_us);
  simulated_time_us += (uint64_t)(us);
  uint64_t time_now_ms = MICROSECONDS_TO_MILLISECONDS(simulated_time_us);
  uint64_t need_to_delay_ms = time_now_ms - time_prev_ms;
  if(need_to_delay_ms != 0)
  {
    double time = need_to_delay_ms;
    sim_set_param(SIM_PARAM_EXECUTE, &time);
    // usleep(us);
  }
}

void platformDelayMs(uint32_t ms)
{
  simulated_time_us += (uint64_t) MILLISECONDS_TO_MICROSECONDS(ms);
  double time = ms;
  sim_set_param(SIM_PARAM_EXECUTE, &time);
  // usleep(ms*1000);
}

void platformSetLedState(uint8_t led_idx, bool on)
{
  struct led_state_t led;
  led.id = led_idx;
  led.on = on;
  sim_set_param(SIM_PARAM_LEDS, (void*) &led);
}

bool platformCheckIfBatteryIsEmpty(void)
{
  return false;
}

uint32_t platformGetElapsedUsFromInit(void)
{
  return simulated_time_us;
}

bool platoformIsButtonPressed(uint8_t idx)
{
  (void) idx; // there is only one button
  bool pressed = false;
  (void)sim_get_param(SIM_PARAM_BUTTON_PRESSED, &pressed);

  return pressed;
}
