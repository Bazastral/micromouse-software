
#include "simulator_broker.h"
#include "platform_printer.h"
#include "vector.h"
#include "point.h"
#include "labirynth.h"

ret_code_t printPathPlannerPoints(vector_t points)
{
  return sim_set_param(SIM_PARAM_PATH, (void*) &points);
}

ret_code_t printPathPlannerCells(vector_t lab_cells)
{
  vector_t points;
  char pointsMem[5000];
  point_t point;
  lab_cellxy_t tmp;
  ret_code_t ret = RET_ERR_OTHER;

  ret = vector_init(&points, pointsMem, sizeof(pointsMem), sizeof(point_t));

  for (uint32_t i = 0; i < vector_item_cnt(lab_cells); i++)
  {
    if (ret == RET_SUCCESS)
    {
      ret = vector_at(lab_cells, i, &tmp);
    }
    if (ret == RET_SUCCESS)
    {
      ret = labGetMidCell(tmp, &point);
    }
    if (ret == RET_SUCCESS)
    {
      ret = vector_add(&points, &point);
    }
  }
  if (ret == RET_SUCCESS)
  {
    ret = printPathPlannerPoints(points);
  }

  return ret;
}

ret_code_t printOnlyOneWallNotification(const lab_cellxy_t coords, const lab_cell_t cell, const bool exists)
{
  SIM_WALL_NOTIFY_t notification;

  notification.x = coords.x;
  notification.y = coords.y;
  notification.mask_walls = cell;
  notification.exists = exists;

  return sim_set_param(SIM_PARAM_NOTIFY_WALL, (void *)&notification);
}