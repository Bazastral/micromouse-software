

#include "platform_state.h"
#include "simulator_broker.h"



ret_code_t statePlatformSet(state_t st)
{
  return sim_set_param(SIM_PARAM_STATE, (void*) &st);
}

ret_code_t statePlatformGet(state_t *st)
{
  return sim_get_param(SIM_PARAM_STATE, (void*) st);
}

ret_code_t statePlatformNotify(state_t st)
{
  return sim_set_param(SIM_PARAM_STATE_NOTIFY, (void*) &st);
}

