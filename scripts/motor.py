#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt




class Motor:
    def __init__(self, t=0.1, k=100.0, step_time_s=0.01, radius=0.01):
        self.T = t
        self.K = k
        self.velocity = 0
        self.step_s = step_time_s
        self.radius = radius
        # print("Motor new. Config: T={:7.6f}, k={:9.6f}, step_s={:9.6f}"
        #         .format(self.T, self.K, self.step_s))

    def step(self, u=0):
        # assert u >= -1.0 and u <= 1.0, "set velocity must be within <-1, 0>"
        u = np.maximum(u, -1.0)
        u = np.minimum(u, 1.0)
        self.velocity = \
                (self.T*self.velocity/self.step_s + self.K*u) \
                / (1 + self.T/self.step_s)
        return self.velocity

    def getTrajectoryAngular(self, u=0, time=1):
        trajectory = [self.velocity]
        for t in np.arange(0, time, self.step_s):
            self.step(u)
            trajectory.append(self.velocity)

        return trajectory

    def getTrajectoryLinear(self, u=0, time=1):
        trajectory = self.getTrajectoryAngular(u, time)
        return [velocity_angular*self.radius for velocity_angular in trajectory]


def main():
    return 0


if __name__ == '__main__':
    main()
