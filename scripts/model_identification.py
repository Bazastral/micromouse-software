#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import motor as motor
from scipy.optimize import curve_fit


def load_cases_from_file(path):
    with open(path, 'r') as f:
        lines = f.readlines()

        cases = []
        case = []
        for line in lines:
            if(line.find("Test params") != -1):
                cases.append(case.copy())
                case.clear()
            case.append(line)
        cases.append(case.copy())
        cases = cases[1:] # remove first case as these are garbage

        print("loaded cases: ", len(cases))
    return cases

def get_trajectory(case):
    trajectory = []
    for row in case[2:]:
        if "-----" in row:
            break
        words = row.split(',')
        words = sum([w.split() for w in words], [])
        trajectory.append([float(v) for v in words])

    return trajectory

def get_case_config(case):
    words = case[0].split(',')
    words = sum([w.split() for w in words], [])

    step = float(words[words.index("step") + 1])
    speed = float(words[words.index("speed") + 1])
    time = float(words[words.index("time") + 1])
    print("step ", step, "speed", speed, "total time", time)

    return step, speed, time


def sim_velocity(x, t, k):
    # print("speed {}, x size {}, t {}, k {}".format(SPEED, len(x), t, k))
    m = motor.Motor(t=t, k=k, step_time_s=CONST_STEP_S)
    traj = m.getTrajectoryLinear(SPEED, x[-1])
    return traj


def main():
    cases = load_cases_from_file("../platform/ukmarsbot-NUCLEO-G431KB/logs/linear.data")
    cases_sim = load_cases_from_file("../platform/ukmarsbot-NUCLEO-G431KB/logs/linear_sim.data")
    optimal_params = []
    np.printoptions(precision=3, suppress=True)
    global SPEED
    global CONST_STEP_S

    for i in range(min(len(cases), len(cases_sim))):
        print("\n\nCASE ", i)
        case = cases[i]
        case_sim = cases_sim[i]
        trajectory_real = get_trajectory(case)
        trajectory_sim = get_trajectory(case_sim)
        step, speed, time = get_case_config(case)
        step_sim, speed_sim, _ = get_case_config(case_sim)
        if step != step_sim:
            print("DIFFERENT")
        if speed != speed_sim:
            print("DIFFERENT")

        CONST_STEP_S= step
        SPEED = speed
        RADIUS =0.01

        xdata = [row[0] for row in trajectory_real]
        ydata = [row[1] for row in trajectory_real]
        popt, pcov = curve_fit(f=sim_velocity, xdata=xdata, ydata=ydata)
        optimal_params.append([*popt])

        print('curve_fit {} t={:5.3f}, k={:5.3f}'.format(SPEED, popt[0], popt[1]))

        fig, ax = plt.subplots()
        ax.scatter([row[0] for row in trajectory_real], [row[1] for row in trajectory_real],
                c='g', marker='x', label="real {}".format(speed))
        ax.scatter([row[0] for row in trajectory_sim], [row[1] for row in trajectory_sim],
                c='magenta', marker='x', label="sim {}".format(speed))
        ax.plot(xdata, sim_velocity(xdata, *popt), 'r-',
            label='curve_fit {} t={:5.3f}, k={:5.3f}'.format(SPEED, popt[0], popt[1]))
        ax.plot(xdata, [popt[1]*SPEED*(1. - np.exp(-t/popt[0]))*RADIUS for t in xdata], 'b-',
            label='analitycal {} t={:5.3f}, k={:5.3f}'.format(SPEED, popt[0], popt[1]))


        ax.legend()
        ax.grid(True)

        ax.set_xlabel("time [s]")
        ax.set_ylabel("velocity [m/s]")
        ax.set_title(f"linear velocity in time for output {SPEED}")
        plt.xlim([0, 0.2])

        plt.show()

    for params in optimal_params:
        print("T {:5.3f}, K {:5.3f}".format(params[0], params[1]))
    optimal_params_average_T = np.average([params[0] for params in optimal_params])
    print(optimal_params_average_T)
    optimal_params_average_K = np.average([params[1] for params in optimal_params])
    print(optimal_params_average_K)


if __name__ == "__main__":
    main()

