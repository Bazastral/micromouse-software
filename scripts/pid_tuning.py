#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import motor as motor
from scipy.optimize import minimize
from operator import itemgetter



class Pid:
    def __init__(self, P=1.0, I=1.0, D=1.0):
        self.P = P
        self.I = I
        self.D = D
        self.prev_error = 0
        self.error_integral = 0

    def step(self, error, time):
        error_d = (error - self.prev_error)/time
        self.error_integral = self.error_integral + error*time
        self.error_integral = min(self.error_integral, 1/self.I)
        self.error_integral = max(self.error_integral, -1/self.I)

        p = self.P * error
        i = self.I * self.error_integral
        d = self.D * error_d
        output = p + i + d
        self.prev_error = error
        output = min(output, 1)
        output = max(output, -1)
        # print("PID p {:8.4f}, i {:8.4f}, d {:8.4f}, err {:8.4f}, o {:8.4f}".format(p, i, d, error, output))
        return output


def sim_velocity(x, t, k):
    traj = m.getTrajectoryLinear(SPEED, x[-1])
    return traj

def calculate_overshoot(trajectory, target):
    maxx = max(trajectory)
    overshoot_percent = 100*(maxx-target)/target
    overshoot_percent = max(0, overshoot_percent)
    # print("overshoot = ", overshoot_percent)
    return overshoot_percent

def calculate_raise_time(trajectory, target, step):
    start_level = target * (1/10)
    end_level = target * (9/10)
    start_found = False
    end_found = False
    start_sample = 0
    end_sample = 0

    for i in range(len(trajectory)):
        t = trajectory[i]
        if not start_found:
            if t > start_level:
                start_found = True
                start_sample = i
        if not end_found:
            if t > end_level:
                end_found = True
                end_sample = i
                break

    if start_found and end_found:
        raise_time = (end_sample - start_sample) * step
    else:
        raise_time = len(trajectory) * step
    # print("raise time = ", raise_time)
    return raise_time

def calculate_final_offset(trajectory, target):
    end = trajectory[-5:]
    avg_last = np.average(trajectory[-5:])
    final_offset_percent = np.abs(avg_last - target)*100/target
    # print("final_offset_percent = ", final_offset_percent)
    return final_offset_percent

"""
Lower better
"""
def calculate_score(trajectory, pid_outputs, target, step):
    overshoot = calculate_overshoot(trajectory, target)
    raise_time = calculate_raise_time(trajectory, target, step)
    final_offset = calculate_final_offset(trajectory, target)
    stdd = np.std(pid_outputs[int(len(pid_outputs)/2):])

    score = 0.1*overshoot + 100*raise_time + final_offset + 100*stdd

    msg = "overshoot {:1.2f}% raise time {:1.2f}ms final_offset {:1.2f}% std {:1.2f} SCORE {:1.4f}".format(overshoot, 1000*raise_time, final_offset, stdd, score)

    return score, msg



def show_figure(trajectory, pid_trajectory, time, target, title, suptitle):
    fig, ax = plt.subplots()
    ax.plot(time, trajectory, c='g', label="velocity")
    ax.plot(time, [target for t in time], c='black', label="target velocity")
    ax.scatter(time, pid_trajectory, c='b', marker='.', label="PID output")

    ax.legend()
    ax.grid(True)

    ax.set_xlabel("time [s]")
    ax.set_ylabel("velocity [m/s]")
    plt.suptitle(suptitle)
    plt.title(title)
    # plt.xlim([0, 0.2])
    plt.ylim([-0.1, target*1.2])
    plt.show()

def simulate(P, I, D, time, TARGET_SPEED_M_PER_S, step):
    K = 140
    T = 0.038
    RADIUS =0.01
    m = motor.Motor(t=T, k=K, step_time_s=step)
    pid = Pid(P, I, D)

    trajectory = [0]
    pid_outputs = [0]

    for t in time[1:]:
        error = TARGET_SPEED_M_PER_S - m.velocity * RADIUS
        pid_output = pid.step(error, step)
        # print("error {:2.4f}, steering {:2.4f}, velo {:2.4f}, target {:2.4f}"
            #   .format(error, pid_output, m.velocity*RADIUS, TARGET_SPEED_M_PER_S))
        m.step(pid_output)

        trajectory.append(m.velocity*RADIUS)
        pid_outputs.append(pid_output)

    return trajectory, pid_outputs

def step_to_optimize(x, ):
    TIME_S = 0.4
    TARGET_SPEED_M_PER_S = 1.
    CONST_STEP_S = 0.001
    time = np.arange(0, TIME_S, CONST_STEP_S)

    trajectory, pid_outputs = simulate(*x, time, TARGET_SPEED_M_PER_S, CONST_STEP_S)

    score = calculate_score(trajectory, pid_outputs, TARGET_SPEED_M_PER_S, CONST_STEP_S)
    return score

def minimize_my(fun,  bounds):
    print(type(bounds[0]))
    xs = []
    scores = []
    merged = []
    for i in range(10000):
        x = [
            np.random.random()*(bounds[0][1] - bounds[0][0]) + bounds[0][0],
            np.random.random()*(bounds[1][1] - bounds[1][0]) + bounds[1][0],
            np.random.random()*(bounds[2][1] - bounds[2][0]) + bounds[2][0],
        ]
        score = fun(x)
        scores.append(score)
        xs.append(x)
        merged.append(np.array([*x, score]))

    sorted_merged = sorted(merged, key=itemgetter(3))
    for s in sorted_merged:
        print("P {:8.3f} I {:8.3f} D {:8.3f} score {:02.3f}".format(s[0], s[1], s[2], s[3]))

    print("best", min(scores), "for ", xs[np.argmin(scores)])
    return xs[np.argmin(scores)]


def main():
    TIME_S = 0.8
    TARGET_SPEED_M_PER_S = 0.5
    CONST_STEP_S = 0.01

    ######################### 1 option
    # x0 = [35, 3, 0]
    # res = minimize(step_to_optimize, x0, bounds=[(0, 200), (0, 10), (0, 1)], method='Nelder-Mead')
    # P, I, D = res.x
    # print(res.message)
    ######################### 2 option
    # print(res.message)
    # res = minimize_my(step_to_optimize, bounds=[(0, 50), (0, 4), (0, 2)])
    # P, I, D = res
    ######################### 3 option - manual
    # P, I, D = 12.00, 0.11, 0.00
    # P, I, D = 36.83, 3.32, 0.00
    # P, I, D = 28.83, 6.72, 0.00
    P, I, D = 2.2, 40, 0.004
    print("------------------")
    time = np.arange(0, TIME_S, CONST_STEP_S)

    trajectory, pid_outputs = simulate(P, I, D, time, TARGET_SPEED_M_PER_S, CONST_STEP_S)
    score, title = calculate_score(trajectory, pid_outputs, TARGET_SPEED_M_PER_S, CONST_STEP_S)
    suptitle = "Target speed {}m/s, P {:1.2f} I {:1.2f} D {:1.2f}".format(TARGET_SPEED_M_PER_S, P, I, D)
    print(title)
    print(suptitle)

    show_figure(trajectory, pid_outputs, time, TARGET_SPEED_M_PER_S, title, suptitle)


if __name__ == "__main__":
    main()

