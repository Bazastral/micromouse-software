#pragma once

#include "ret_codes.h"
#include "labirynth.h"
#include "state.h"

/**
 * @brief Initialize module
 *
 * @param initial_state starting state of the mouse, it will reset to this state
 * @param goal          the aim of the mission
 *
 * @return RET_SUCCESS for success
 */
ret_code_t missionInit(state_t initial_state, lab_cellxy_t goal);

/**
 * @brief Initialize module - default version
 *
 * @return RET_SUCCESS for success
 */
ret_code_t missionInitDefault(void);

/**
 * @brief Execute competition flow
 * The main looping function for competition behavior
 *
 * This is the highest-level function that calls combines modules together
 * It contains machine state inside it.
 * Because state is also used as a physical state of the mouse,
 * it is better to use here 'stage' naming convention.
 * As long as one stage is performed, the function will return REG_AGAIN.
 * Once it's finished it will return RET_SUCCESS and automatically shift gears
 * for next stage. So it is the time to manually clean the wheels etc.
 * Critical and unrecoverable errors return other values.
 *
 * The flow goes like this (states):
 * 1. (init to initial_state) Go to target
 * 2. (init to initial_state) Map the labirynths to find optimial way
 * 3+.(init to initial_state) Speedrun
 *
 * @returns RET_AGAIN   to call it again
 * @returns RET_SUCCESS when one stage is finished
 * @returns other       on fatal errors
 */
ret_code_t missionCompetitionMainThread(void);

/**
 * @brief Get the state of the mouse
 *
 * @return state_t
 */
state_t missionGetCurrentMouseState(void);

