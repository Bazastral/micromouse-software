#pragma once

#include "useful.h"

#define TIMING_PREV_DURATIONS_NUMBER 10

/**
 * @brief Starts measurement. start_timestamp is OUT
 */
void timingMeasureStart(uint32_t *start_timestamp);

/**
 * @brief Returns how many us elapsed from starting.
 * Can be used multiple times after timingMeasureStart was called
 */
uint32_t timingMeasureElapsedUs(const uint32_t start_timestamp);

typedef struct{
    struct
    {
        /**
         * Do not access this struct directly.
         * Use API below
         */
        uint32_t duration_us;
        uint32_t previous_timestamp_us;
        uint32_t previous_task_durations[TIMING_PREV_DURATIONS_NUMBER];
        bool inited;
    } internal;
    struct
    {
        uint32_t errors_number;
        uint32_t too_long_total_loops_number;
        uint32_t total_loops_number;
        uint32_t task_duration_average_us;
        uint32_t previous_task_duration;
        uint32_t task_duration_max;
        uint32_t task_duration_min;
    } external;
} timing_loop_ctx_t;

void timingLoopInitialize(timing_loop_ctx_t* const ctx, uint32_t duration_us);

void timingLoopControl(timing_loop_ctx_t* const ctx);

void timingLoopPrintStats(const timing_loop_ctx_t* const ctx);