#pragma once

#include "point.h"

#define WORLD_DIR_N (4u)

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

#define SECONDS_TO_MILLISECONDS(x) (1000*x)
#define SECONDS_TO_MICROSECONDS(x) (1000000*x)
#define MILLISECONDS_TO_SECONDS(x) (x/1000)
#define MILLISECONDS_TO_MICROSECONDS(x) (x*1000)
#define MICROSECONDS_TO_MILLISECONDS(x) (x/1000)
#define MICROSECONDS_TO_SECONDS(x) (x/1000000)

#define ONE_METER 1000
#define METERS_TO_MILLIMETERS(x) (1000*x)
#define MILLIMETERS_TO_METERS(x) (x/1000)

#define DEG_2_RAD(deg) ((deg * M_PI) / 180)

/*
 * Normalizes rad to be inside (-PI,PI]
 */
double normalizeRad(double rad);

/**
 * @brief Extracts only sign of the value
 *
 * @param val value to get sign
 * @return -1 or 1
 */
double sign_double(double val);

/**
 * @brief Converts degress to radians
 */
double deg2rad(double deg);

/**
 * @brief Converts radians to degress
 */
double rad2deg(double rad);

/**
 * @brief Calculates cotangens of an angle
 */
double ctg(double rad);

/**
 * @brief Generates normal (Gauss) distribution
 *
 * @param mu
 * @param sigma
 * @return double
 */
double generate_normal_distribution(double mu, double sigma);
