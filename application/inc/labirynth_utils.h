#pragma once

#include <stdbool.h>

#include "ret_codes.h"
#include "point.h"
#include "logger.h"

#define LAB_N 16

// All dimensions in millimeters
#define LAB_DIM_CELL            (180)
#define LAB_DIM_CELL_HALF       (LAB_DIM_CELL/2)
#define LAB_DIM_WALL            (12)
#define LAB_DIM_WALL_HALF       (LAB_DIM_WALL/2)
#define LAB_DIM_CORRIDOR        (LAB_DIM_CELL - LAB_DIM_WALL)
#define LAB_DIM_CORRIDOR_HALF   (LAB_DIM_CORRIDOR/2)
#define LAB_DIM_MAX             (LAB_DIM_CELL*LAB_N + LAB_DIM_WALL)

enum lab_cell_mask{
  LAB_NORTH     = 0x01,
  LAB_EAST      = 0x02,
  LAB_SOUTH     = 0x04,
  LAB_WEST      = 0x08,
  LAB_ALL_DIRS  = (LAB_NORTH | LAB_SOUTH | LAB_EAST | LAB_WEST)
};
typedef enum lab_cell_mask lab_cell_mask_t;

struct lab_cellxy{
  uint8_t x;
  uint8_t y;
};
typedef struct lab_cellxy lab_cellxy_t;

typedef uint8_t lab_cell_t;
typedef lab_cell_t labirynth_t[LAB_N][LAB_N];
typedef struct {
  lab_cellxy_t cell_xy;
  lab_cell_mask_t direction;
} lab_specific_wall_t;


/**
 * @brief A wall is mathematically a straight line with constant X or Y value.
 * So it can be described with only one coordination
 */
typedef struct{
  double N;
  double S;
  double E;
  double W;
} lab_walls_coords_t;

/**
 * @brief Convert point in space into cell x y coordinates
 */
ret_code_t labPoint2Cellxy(lab_cellxy_t* cellxy, point_t point);
lab_cellxy_t labPoint2CellXY(point_t point);

/**
 * @brief Convert cell x y coordinations into point (get the middle of the cell)
 *
 * @param[in] cellxy  cell to convert
 * @param[out] point  loads here middle point of the provided cell
 *
 * @return ret_code_t RET_SUCCESS if proper cellxy provided
 */
ret_code_t labGetMidCell(lab_cellxy_t cellxy, point_t* point);

/**
 * @brief Works as labGetMidCell, but returns directly calculated point
 *
 * @param[in] cellxy cell to convert
 *
 * @return point_t middle point of cell
 */
point_t labCellToPoint(lab_cellxy_t cellxy);

/**
 * Check if coords are in the bounds (valid)
 */
ret_code_t labCheckXy(lab_cellxy_t coords);

/**
 * @brief Checks if coords are the same
 *
 * @param a       first coords to compare
 * @param b       second coords to compare
 * @return true   equal
 * @return false  not equal
 */
bool labCoordsEqual(lab_cellxy_t a, lab_cellxy_t b);


/**
 * @brief Get coords of walls in a given cell
 * Each wall can be described as a line with constant x or y value.
 * @note This function returns inner sides of the walls, so half of the
 * wall's thickness is substracted
 *
 * @param cellxy
 *
 * @return lab_walls_coords_t
 */
lab_walls_coords_t labGetWallsCoordsFromCellXY(lab_cellxy_t cellxy);

/**
 * @brief Get coordinations of specific places in maze
 *
 * @return lab_cellxy_t
 */
lab_cellxy_t labCoordsStartingPoint(void);
lab_cellxy_t labCoordsCornerNE(void);
lab_cellxy_t labCoordsCornerNW(void);
lab_cellxy_t labCoordsCornerSE(void);
lab_cellxy_t labCoordsCornerSW(void);
lab_cellxy_t labCoordsGoalNE(void);
lab_cellxy_t labCoordsGoalNW(void);
lab_cellxy_t labCoordsGoalSE(void);
lab_cellxy_t labCoordsGoalSW(void);

/**
 * @brief Dump wall using logger
 */
void labDumpSpecificWall(LOGGER_LVL_t log_level, char* description, lab_specific_wall_t wall);

/**
 * @brief Dump lab_cell_xy_t using logger
 */
void labDumpCellxy(char* description, lab_cellxy_t cellxy);