
#pragma once


enum EXHIBIT_EVENT{
  EXHIBIT_PLATFORM_INITIALIZED,
  EXHIBIT_SUCCESS,
  EXHIBIT_CRITICAL_ERROR,
  EXHIBIT_SOME_FAILURE,
  EXHIBIT_BATTERY_EMPTY,
};
typedef enum EXHIBIT_EVENT EXHIBIT_EVENT_t;

void exhibitFailures(int failures);

/**
 * @brief Show result of the test
 */
void exhibitEvent(EXHIBIT_EVENT_t event);

