
#pragma once

#include "labirynth.h"

typedef enum
{
  LABIRYNTHS_A,
  LABIRYNTHS_ALL_WALLS,
  LABIRYNTHS_TEST_STRAIGHT_NS,
  LABIRYNTHS_TEST_STRAIGHT_NS_3,
  LABIRYNTHS_TEST_STRAIGHT_EW,
  LABIRYNTHS_ARENA,
  LABIRYNTHS_ALLJAPAN_001_1980,
  LABIRYNTHS_HITEL,
  LABIRYNTHS_CLASSIC_86,
  LABIRYNTHS_5_TWO_ROUTES,
  // WARNING
  // adding here requires to add entry in labirynthNameEnumToString function
  LABIRYNTHS_NUMBER,
} LABIRYNTHS_NAME;

extern const labirynth_t labirynths[LABIRYNTHS_NUMBER];
const char * labirynthNameEnumToString(LABIRYNTHS_NAME);


