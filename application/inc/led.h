
#pragma once

#include "ret_codes.h"

#define LED_ON 1
#define LED_OFF 0

enum LED_ID
{
  LED_FRONT_LEFT,
  LED_FRONT_RIGHT,
  LED_NUCLEO,
};
typedef enum LED_ID LED_ID_t;


typedef struct led_state_t
{
    LED_ID_t id;
    int on;
}led_state_t;

