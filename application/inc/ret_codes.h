#pragma once


#include <stdint.h>

typedef enum
{
    RET_SUCCESS = 0x00,
    RET_ERR_OTHER = 0x01,
    RET_ERR_OUT_OF_BOUNDS = 0x02,
    RET_ERR_INVALID_PARAM = 0x03,
    RET_ERR_NOT_IMPLEMENTED = 0x04,
    RET_ERR_NOT_INITED = 0x05,
    RET_ERR_TIMEOUT = 0x06,
    RET_ERR_BAD_CONNECTION = 0x07,
    RET_ERR_SERVER_RESPONSE = 0x08,
    RET_ERR_NO_MEM = 0x09,
    RET_ERR_NOT_FOUND = 0x0a,
    RET_AGAIN = 0x0b,
    RET_MAPPING_NEEDED = 0x0c,
} ret_code_t;
