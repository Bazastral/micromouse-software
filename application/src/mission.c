
#include <string.h>

#include "mission.h"
#include "labirynth.h"
#include "path_planner.h"
#include "distance_sensors.h"
#include "distance_sensors_hw.h"
#include "platform_printer.h"
#include "state.h"
#include "printer.h"
#include "executor.h"
#include "motor.h"
#include "platform_base.h"
#include "platform_state.h"
#include "labirynth_alter.h"
#include "timing.h"
#include "logger.h"

#define NEXT_POINT_IN_PATH 1

static const uint8_t MAX_HARD_ESTIMATION_ERROR_NUMBER = 12;
static const double STEPS_SECONDS = 0.01;
static const double STEPS_MICROSECONDS = SECONDS_TO_MICROSECONDS(STEPS_SECONDS);


typedef enum{
    STAGE_FIRST_RUN,
    STAGE_FURTHER_MAPPING,
    STAGE_SPEEDRUN,
    STAGE_NUMBER
} MISSION_STAGE_t;

typedef ret_code_t (*mission_function)(void);
struct stage_funs
{
    mission_function prepare;
    mission_function function;
};

ret_code_t FirstRunPrepare(void);
ret_code_t FirstRunStage(void);
ret_code_t MoreMappingPrepare(void);
ret_code_t MoreMappingStage(void);
ret_code_t SpeedRunPrepare(void);
ret_code_t SpeedRunStage(void);
void MazeChanged(void);

static struct
{
    MISSION_STAGE_t mission_stage;                 // current stage of the competition
    MISSION_STAGE_t mission_stage_prev;                 // current stage of the competition
    vector_t path;                                 // needed by path planner
    uint8_t path_memory[1000];                     // needed by path planner
    vector_t path_b;                               // needed by path planner
    uint8_t path_memory_b[1000];                   // needed by path planner
    state_t mouse_state;                           // state of the mouse (position, velocities)
    double measurements[MAX_DISTANCE_SENSORS_NUM]; // measurements from distance sensors
    lab_cellxy_t from;                             // beginning cellxy for path planning
    uint8_t state_estimation_error_cnt;            // used for slip and huge unrecoverable errors
    lab_cellxy_t goal;                             // the final goal of the mouse
    lab_cellxy_t mapping_goal;                     // temporary goal of the mouse (during mapping)
    state_t initial_state;                         // physical state from which the mouse is starting new part
    bool dirty_mapping_activated;                  // dirty hack for mapping middle maze
    timing_loop_ctx_t looping_ctx;                 // context for looping timings
    struct{
        uint8_t maze_changed_counter;
        uint8_t counter_to_replan_path;
        bool force_path_replanning;
    } planning;
}ctx;

void MazeChanged(void)
{
    if(ctx.dirty_mapping_activated == false)
    {
        ctx.planning.maze_changed_counter++;
    }
}

static lab_cellxy_t getDefaultGoal(void)
{
    return labCoordsGoalNE();
}

ret_code_t missionInitDefault(void)
{
    return missionInit(stateStartingPoint(), getDefaultGoal());
}

ret_code_t missionInit(state_t initial_state, lab_cellxy_t goal)
{
    ret_code_t ret = RET_ERR_OTHER;

    memset(&ctx, 0, sizeof(ctx));
    ctx.planning.force_path_replanning = true;
    ctx.initial_state = initial_state;
    ctx.goal = goal;

    ret = executorInit(configGet());
    if (ret == RET_SUCCESS)
    {
        ret = dsInit(dsGetCfgPlatformDefault());
    }
    if (ret == RET_SUCCESS)
    {
        ret = statePlatformSet(initial_state);
        statePlatformNotify(initial_state);
    }
    if (ret == RET_SUCCESS)
    {
        ret = vector_init(&ctx.path, ctx.path_memory, sizeof(ctx.path_memory), sizeof(lab_cellxy_t));
    }
    if (ret == RET_SUCCESS)
    {
        ret = vector_init(&ctx.path_b, ctx.path_memory_b, sizeof(ctx.path_memory_b), sizeof(lab_cellxy_t));
    }
    if (ret == RET_SUCCESS)
    {
        if(dsGetCfgPlatformDefault().numberOfSensors > ARRAY_SIZE(ctx.measurements))
        {
            ret = RET_ERR_NO_MEM;
        }
    }
    if (ret == RET_SUCCESS)
    {
        ret = labResetToDefault();
    }
    if (ret == RET_SUCCESS)
    {
        memset(ctx.path_memory, 0, sizeof(ctx.path_memory));
        stateInitModule(configGet());
        ctx.mouse_state = initial_state;
        ctx.mission_stage = STAGE_FIRST_RUN;
        labRegisterChangeCallback(MazeChanged);
        // set to invalid one. Just to make sure that
        // prepare for first run will be called
        ctx.mission_stage_prev = STAGE_NUMBER;
        timingLoopInitialize(&ctx.looping_ctx, STEPS_MICROSECONDS);
    }

    return ret;
}

static ret_code_t missionGoToTarget(void)
{
    ret_code_t ret = RET_ERR_OTHER;
    motors_speed_t motors;
    motors_ticks_t ticks = {0};
    lab_cellxy_t current_target;

    ret = vector_at(ctx.path, 0, &current_target);

    if (ret == RET_SUCCESS)
    {
        ctx.from = current_target;
        if (vector_item_cnt(ctx.path) > 1)
        {
            ret = executorGoToTarget(labCellToPoint(current_target),
                                        ctx.mouse_state, STEPS_SECONDS, &motors);
        }
        else
        {
            ret = executorGoToTargetAndStop(labCellToPoint(current_target),
                                        ctx.mouse_state, STEPS_SECONDS, &motors);
        }
    }

    if (ret == RET_SUCCESS)
    {
        // current target achieved, remove it from path, and take next one
        ret = vector_del_idx(&ctx.path, 0);
        if (vector_item_cnt(ctx.path) == 0)
        {
            return RET_SUCCESS;
        }
        else
        {
            return RET_AGAIN;
        }
    }
    else if(ret == RET_AGAIN)
    {
        ret = RET_SUCCESS;
    }

    if(ret == RET_SUCCESS)
    {
        ret = motorSetSpeed(motors);
    }
    if (ret == RET_SUCCESS)
    {
        timingLoopControl(&ctx.looping_ctx);
        if((ctx.looping_ctx.external.total_loops_number%30) == 0)
        {
            timingLoopPrintStats(&ctx.looping_ctx);
        }
    }
    if (ret == RET_SUCCESS)
    {
        ret = motorGetTicks(&ticks);
    }
    if (ret == RET_SUCCESS)
    {
        ret = dsGetDistances(ctx.measurements, dsGetCfg()->numberOfSensors);
    }
    if (ret == RET_SUCCESS)
    {
        ret = stateStep(STEPS_SECONDS, &ctx.mouse_state,
                        ctx.measurements, dsGetCfg()->numberOfSensors,
                        ticks);

        if(ret != RET_SUCCESS)
        {
            ctx.state_estimation_error_cnt++;
            log_warn("Mission: warn state est %d", (int)ctx.state_estimation_error_cnt);
            if(ctx.state_estimation_error_cnt > MAX_HARD_ESTIMATION_ERROR_NUMBER)
            {
                log_error("Mission: error state est");
            }
            else
            {
                ret = RET_SUCCESS;
            }
        }
        else
        {
            ctx.state_estimation_error_cnt = 0;
        }
    }

    if (ret == RET_SUCCESS)
    {
        if ((ctx.mission_stage == STAGE_FIRST_RUN) ||
            (ctx.mission_stage == STAGE_FURTHER_MAPPING))
        {
            // do not bother with the return status, it is not important
            stateDetectWalls(ctx.mouse_state, ctx.measurements, dsGetCfg()->numberOfSensors);
        }
        statePlatformNotify(ctx.mouse_state);
        ret = RET_AGAIN;
    }
    return ret;
}


ret_code_t FirstRunPrepare(void)
{
    timingLoopInitialize(&ctx.looping_ctx, STEPS_MICROSECONDS);
    ctx.from = labPoint2CellXY(stateGetPoint(ctx.initial_state));
    return RET_SUCCESS;
}

ret_code_t dirtyMiddleMapping(void)
{
    if((labCoordsEqual(ctx.goal, getDefaultGoal()) == true) &&
        (ctx.dirty_mapping_activated == false))
    {
        ctx.dirty_mapping_activated = true;
        vector_clear(&ctx.path);
        lab_cellxy_t goal_coords;
        goal_coords = labCoordsGoalSE();
        vector_add(&ctx.path, &goal_coords);
        goal_coords = labCoordsGoalSW();
        vector_add(&ctx.path, &goal_coords);
        goal_coords = labCoordsGoalNW();
        vector_add(&ctx.path, &goal_coords);
        goal_coords = labCoordsGoalNE();
        vector_add(&ctx.path, &goal_coords);
        return RET_AGAIN;
    }
    return RET_SUCCESS;
}

static bool CheckIfReplanningNeeded(void)
{
    /*
    Robot needs huge amount of time to do the planning.
    So in order to optimize it, do not replan path always.
    Do it in two cases:
    1. 3 new walls were mapped (all new in one cell)
    2. at least one wall is new and during few loop spins
        there was no other wall detected
    */
    const int CALLS_TO_WAIT_FOR_SECOND_CHANGE = 5;
    const uint8_t WALLS_TO_MAP_IN_SINGLE_CELL = 3;

    if (ctx.planning.force_path_replanning == true)
    {
        ctx.planning.counter_to_replan_path = 0;
        ctx.planning.force_path_replanning = false;
        return true;
    }
    else if (ctx.planning.maze_changed_counter >= WALLS_TO_MAP_IN_SINGLE_CELL)
    {
        ctx.planning.counter_to_replan_path = 0;
        ctx.planning.maze_changed_counter = 0;
        return true;
    }
    else if (ctx.planning.maze_changed_counter != 0)
    {
        ctx.planning.counter_to_replan_path++;
        if (ctx.planning.counter_to_replan_path >= CALLS_TO_WAIT_FOR_SECOND_CHANGE)
        {
            ctx.planning.maze_changed_counter = 0;
            ctx.planning.counter_to_replan_path = 0;
            return true;
        }
    }
    return false;
}

ret_code_t FirstRunStage(void)
{
    ret_code_t ret = RET_SUCCESS;

    if(CheckIfReplanningNeeded() == true)
    {
        // very dirty hack.... this function is too long,
        // so stop motors not to be suprised, how far we went
        motorSetSpeed((motors_speed_t){0, 0});
        ret = pp_find_path(ctx.from, ctx.goal, &ctx.path);
        printPathPlannerCells(ctx.path);
        printLabWithOneRoute(ctx.path);
    }

    if(ret == RET_MAPPING_NEEDED)
    {
        // the maze is not fully known
        // mouse has to map it first
        ret = RET_SUCCESS;
    }

    if (ret == RET_SUCCESS)
    {
        ret = missionGoToTarget();
    }

    if(ret == RET_SUCCESS)
    {
        ret = dirtyMiddleMapping();
    }

    if(ret == RET_SUCCESS)
    {
        log_error("Mission first run done!");
        ctx.mission_stage = STAGE_FURTHER_MAPPING;
    }

    return ret;
}

ret_code_t MoreMappingPrepare(void)
{
    ctx.mouse_state = ctx.initial_state;
    ctx.planning.force_path_replanning = true;
    ctx.from = labPoint2CellXY(stateGetPoint(ctx.initial_state));
    timingLoopInitialize(&ctx.looping_ctx, STEPS_MICROSECONDS);
    return RET_SUCCESS;
}

ret_code_t MoreMappingStage(void)
{
    ret_code_t ret = RET_SUCCESS;

    if (ret == RET_SUCCESS)
    {
        if(CheckIfReplanningNeeded() == true)
        {
            ret = pp_plan_mapping(ctx.from, ctx.goal, &ctx.mapping_goal, &ctx.path, &ctx.path_b);
            if (ret == RET_SUCCESS)
            {
                // TODO why two times?
                log_error("Mission mapping fully done!");
                ctx.mission_stage = STAGE_SPEEDRUN;
                return RET_SUCCESS;
            }
            else if (ret == RET_MAPPING_NEEDED)
            {
                ret = RET_SUCCESS;
            }

            // find the way to go there
            if (ret == RET_SUCCESS)
            {
                ret = pp_find_path(ctx.from, ctx.mapping_goal, &ctx.path);
                if (ret == RET_MAPPING_NEEDED)
                {
                    ret = RET_SUCCESS;
                }
            }

            printPathPlannerCells(ctx.path);
            printLabWithOneRoute(ctx.path);
        }
    }

    if (ret == RET_SUCCESS)
    {
        ret = missionGoToTarget();

        if(ret == RET_SUCCESS)
        {
                // TODO why two times?
            log_error("Mission whole mapping done!");
            ctx.mission_stage = STAGE_SPEEDRUN;
        }
    }

    return ret;
}

ret_code_t SpeedRunPrepare(void)
{
    ctx.mouse_state = ctx.initial_state;
    ctx.from = labPoint2CellXY(stateGetPoint(ctx.initial_state));
    ret_code_t ret = pp_find_path(ctx.from, ctx.goal, &ctx.path);
    printPathPlannerCells(ctx.path);
    printLabWithOneRoute(ctx.path);
    timingLoopInitialize(&ctx.looping_ctx, STEPS_MICROSECONDS);
    executorSetSpeedRun(true);

    return ret;
}

ret_code_t SpeedRunStage(void)
{
    ret_code_t ret = missionGoToTarget();

    if (ret == RET_SUCCESS)
    {
        ctx.mission_stage_prev = STAGE_NUMBER; //force calling prepare step
    }

    return ret;
}

struct stage_funs stages[STAGE_NUMBER] = {
    { FirstRunPrepare, FirstRunStage },
    { MoreMappingPrepare, MoreMappingStage },
    { SpeedRunPrepare, SpeedRunStage },
};

ret_code_t missionCompetitionMainThread()
{
    ret_code_t ret = RET_SUCCESS;

    if(ctx.mission_stage != ctx.mission_stage_prev)
    {
        ctx.mission_stage_prev = ctx.mission_stage;
        ret = stages[ctx.mission_stage].prepare();
        log_debug("Mission prepare next stage %d, Status %d", ctx.mission_stage, ret);
        ctx.dirty_mapping_activated = false;
    }

    if(ret == RET_SUCCESS)
    {
        ret = stages[ctx.mission_stage].function();
    }

    if(ret != RET_AGAIN)
    {
        // there was error or the whole stage done. In both scenarios don't move
        motorSetSpeed((motors_speed_t){0, 0});
    }

    log_debug("Mission to goal. Stage %d, Status %d", ctx.mission_stage, ret);

    return ret;
}

state_t missionGetCurrentMouseState(void)
{
    return ctx.mouse_state;
}
