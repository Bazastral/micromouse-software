

#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <float.h>

#include "path_planner.h"
#include "logger.h"

typedef struct
{
  lab_cellxy_t pos;
  lab_cellxy_t parent;
  double cost;
} node;

static char memOpen[sizeof(lab_cellxy_t) * LAB_N * LAB_N];
static char memClosed[sizeof(lab_cellxy_t) * LAB_N * LAB_N];
static node nodes[LAB_N][LAB_N];
static vector_t open, closed;
static lab_cellxy_t pp_aim;

/**
 * @brief Calculate distance between middle of two cells
 *
 * @param[in] from  coords to first cell
 * @param[in] to    coords to second cell
 *
 * @return double   distance in millimeters
 */
static double pp_dist_cells(lab_cellxy_t from, lab_cellxy_t to);

/**
 * @brief Adds cell to open set and/or updates its cost
 * Does not modify closed nodes.
 * Function is called whenever new main node in A* algorithm is picked,
 * then data for all neighbors of this cell is updated
 *
 * @param ref   reference node
 * @param neigh for which cel the function was called
 * @param cell  state of walls for this (neigh) cell
 */
static void pp_check_neighbors(lab_cellxy_t ref, lab_cellxy_t neigh, lab_cell_t cell);

/**
 * @brief Generate final path of lab_cellxy_t cells
 * from set of closed nodes
 *
 * @param[out] path vector of lab_cellxy_t that will be filled
 *
 * @return ret_code_t
 */
static ret_code_t pp_reconstruct_path(vector_t *path);

/**
 * @brief Internal function for mapping
 * Returns next "target" cell for robot to map
 *
 * @param[in] pos     Current position of the robot
 * @param[out] target Here result will be stored
 * @param[in] path    Vector needed for internal calculations
 * @param[in] pathB   Vector needed for internal calculations
 *
 * @returns if operation succedded
 */
static bool pp_find_closest_cell_to_map(
  lab_cellxy_t pos,
  lab_cellxy_t *target,
  vector_t *path,
  vector_t *pathB);


void pp_init(void)
{
}

static double pp_dist_cells(lab_cellxy_t from, lab_cellxy_t to)
{
  point_t start, end;

  if (labGetMidCell(from, &start) != RET_SUCCESS)
  {
    return -1;
  }
  if (labGetMidCell(to, &end) != RET_SUCCESS)
  {
    return -1;
  }
  double dist = pointGetDistance(start, end);
  log_debug("Cost from cell (%d, %d) to (%d, %d) is %f",
            from.x, from.y, to.x, to.y, dist);
  return dist;
}

static void pp_check_neighbors(lab_cellxy_t ref, lab_cellxy_t neigh, lab_cell_t cell)
{
  uint32_t index;
  double new_cost;

  log_debug("pp_check_neighbors (%d, %d)= %d", neigh.x, neigh.y, cell);

  // node is not in closed
  if (vector_get_item_idx(closed, &index, &neigh) != RET_SUCCESS)
  {
    log_debug("not in closed");
    // calculate new cost
    new_cost = nodes[ref.x][ref.y].cost +
               pp_dist_cells(ref, neigh) +
               pp_dist_cells(neigh, pp_aim);

    // not in open
    if (vector_get_item_idx(open, &index, &neigh) != RET_SUCCESS)
    {
      log_debug("not yet in open - add to open");
      vector_add(&open, &neigh);
      log_debug("new cost");
      nodes[neigh.x][neigh.y].cost = new_cost;
      nodes[neigh.x][neigh.y].parent = ref;
    }
    else
    {
      log_debug("neighbor is in open, checking if new cost is smaller than actual");

      if (new_cost < nodes[neigh.x][neigh.y].cost)
      {
        log_debug("new cost");
        nodes[neigh.x][neigh.y].cost = new_cost;
        nodes[neigh.x][neigh.y].parent = ref;
      }
    }
  }
  else
  {
    log_debug("cell is in closed...");
  }
}

static ret_code_t pp_reconstruct_path(vector_t *path)
{
  uint32_t maxIterations = 1000u;
  ret_code_t ret;
  lab_cellxy_t nodeTmp, nodeParent;
  bool mappingStillNeeded = false;

  // get last
  log_debug("pp_reconstruct_path, closed %d", vector_item_cnt(closed));
  ret = vector_at(closed, vector_item_cnt(closed) - 1, &nodeTmp);

  if (ret == RET_SUCCESS)
  {
    ret = vector_del_idx(&closed, vector_item_cnt(closed) - 1);
  }

  if (ret == RET_SUCCESS)
  {
    while (!(nodeTmp.x == nodes[nodeTmp.x][nodeTmp.y].parent.x &&
             nodeTmp.y == nodes[nodeTmp.x][nodeTmp.y].parent.y) &&
           --maxIterations)
    {
      log_debug("add node %d %d", nodeTmp.x, nodeTmp.y);
      if (labWallsKnown(nodeTmp, LAB_ALL_DIRS) == false)
      {
        mappingStillNeeded = true;
      }
      ret = vector_add(path, &nodeTmp);
      if (ret != RET_SUCCESS)
      {
        break;
      }
      nodeParent = nodes[nodeTmp.x][nodeTmp.y].parent;
      nodeTmp = nodeParent;
    }
  }

  if (ret == RET_SUCCESS)
  {
    // add the starting point
    ret = vector_add(path, &nodeTmp);
  }

  if (ret == RET_SUCCESS)
  {
    log_debug("path has %d points", vector_item_cnt(*path));
    ret = vector_turnover(path);
  }

  if (ret == RET_SUCCESS)
  {
    if (mappingStillNeeded)
    {
      ret = RET_MAPPING_NEEDED;
    }
  }
  return ret;
}

ret_code_t pp_find_path(lab_cellxy_t from, lab_cellxy_t to, vector_t *path)
{
  log_debug(" pp_find_path");
  labDumpCellxy("pp_find_path from", from);
  labDumpCellxy("pp_find_path to", to);

  lab_cellxy_t nodeNow, nodeTmp;
  ret_code_t ret;
  bool found = false;
  uint32_t index;
  uint32_t maxIterations = 500;
  uint32_t iter = 0;

  if (path->itemSize != sizeof(lab_cellxy_t))
  {
    return RET_ERR_INVALID_PARAM;
  }
  if (labCheckXy(from) != RET_SUCCESS)
  {
    return RET_ERR_INVALID_PARAM;
  }
  if (labCheckXy(to) != RET_SUCCESS)
  {
    return RET_ERR_INVALID_PARAM;
  }

  vector_init(&open, memOpen, sizeof(memOpen), sizeof(lab_cellxy_t));
  vector_init(&closed, memClosed, sizeof(memClosed), sizeof(lab_cellxy_t));
  vector_clear(path);
  pp_aim = to;

  // initialize map of nodes
  for (uint16_t x = 0; x < LAB_N; x++)
  {
    for (uint16_t y = 0; y < LAB_N; y++)
    {
      nodes[x][y].cost = DBL_MAX;
    }
  }

  // initialize open set with first node
  nodeTmp = from;
  nodes[nodeTmp.x][nodeTmp.y].parent = from;
  nodes[nodeTmp.x][nodeTmp.y].cost = pp_dist_cells(nodeTmp, to);
  vector_add(&open, &nodeTmp);

  // loop as long as there are open nodes
  while (vector_item_cnt(open) > 0u)
  {
    log_debug("****** LOOP %d, cnt open: %d, closed: %d",
              iter,
              vector_item_cnt(open),
              vector_item_cnt(closed));

    if (++iter > maxIterations)
    {
      log_warn("PP max iterations reached");
      return RET_ERR_NOT_FOUND;
    }
    // get next node from open set
    // must have the lowest cost (greedy algorithm)
    ret = vector_at(open, 0, &nodeNow);
    if (ret == RET_SUCCESS)
    {
      for (uint32_t i = 1u; i < vector_item_cnt(open); i++)
      {
        log_debug("looking for low cost %d of %d", i, vector_item_cnt(open));
        ret = vector_at(open, i, &nodeTmp);
        if (ret != RET_SUCCESS)
        {
          break;
        }
        if (nodes[nodeTmp.x][nodeTmp.y].cost < nodes[nodeNow.x][nodeNow.y].cost)
        {
          nodeNow = nodeTmp;
        }
      }
    }

    if (ret == RET_SUCCESS)
    {
      log_debug("nodenow (%d, %d), cost %f", nodeNow.x, nodeNow.y, nodes[nodeNow.x][nodeNow.y].cost);
      // calculate the real cost so far
      nodeTmp = nodes[nodeNow.x][nodeNow.y].parent;
      nodes[nodeNow.x][nodeNow.y].cost -= pp_dist_cells(nodeNow, to);

      // now remove from the open set and add to closed
      ret = vector_get_item_idx(open, &index, &nodeNow);
    }
    if (ret == RET_SUCCESS)
    {
      ret = vector_del_idx(&open, index);
    }
    if (ret == RET_SUCCESS)
    {
      ret = vector_add(&closed, &nodeNow);
    }

    if (ret == RET_SUCCESS)
    {
      // check if finish found
      if (nodeNow.x == to.x && nodeNow.y == to.y)
      {
        found = true;
        log_debug("found end!!");
        break;
      }

      labForEachNeighbor(nodeNow, pp_check_neighbors);
    }
  }

  if (ret == RET_SUCCESS)
  {
    if (!found)
    {
      ret = RET_ERR_NOT_FOUND;
    }
    else
    {
      ret = pp_reconstruct_path(path);
    }
  }

  return ret;
}

ret_code_t pp_plan_mapping(
    lab_cellxy_t pos,
    lab_cellxy_t aim,
    lab_cellxy_t *target,
    vector_t *path,
    vector_t *pathB)
{
  ret_code_t ret = RET_SUCCESS;
  bool success = false;

  if ((target == NULL) ||
      (path == NULL) ||
      (pathB == NULL) ||
      (labCheckXy(pos) != RET_SUCCESS))
  {
    return RET_ERR_INVALID_PARAM;
  }

  ret = pp_find_path(labCoordsStartingPoint(), aim, path);

  if (ret == RET_MAPPING_NEEDED)
  {
    log_info("mapping needed");
    success = pp_find_closest_cell_to_map(pos, target, path, pathB);
    if(success == false)
    {
      ret = RET_ERR_OTHER;
    }
  }
  else if(ret == RET_SUCCESS)
  {
    log_info("full path known");
    // no mapping needed, go to start point
    *target = labCoordsStartingPoint();
  }

  return ret;
}


bool pp_find_closest_cell_to_map(
  lab_cellxy_t pos,
  lab_cellxy_t *target,
  vector_t *path,
  vector_t *pathB)
{
  bool success = false;
  ret_code_t ret = RET_ERR_OTHER;
  lab_cellxy_t tmpGoal = {0, 0};
  uint16_t pathLenToGo = UINT16_MAX;

  for (uint16_t i = 0u; i < vector_item_cnt(*path); i++)
  {
    ret = vector_at(*path, i, &tmpGoal);
    if (ret == RET_SUCCESS)
    {
      if (labWallsKnown(tmpGoal, LAB_ALL_DIRS) != true)
      {
        // this cell is not known, calculate how far it is to go there
        ret = pp_find_path(pos, tmpGoal, pathB);
        if (ret == RET_SUCCESS || ret == RET_MAPPING_NEEDED)
        {
          if (vector_item_cnt(*pathB) < pathLenToGo)
          {
            // this cell is closer, go there
            *target = tmpGoal;
            pathLenToGo = vector_item_cnt(*pathB);
            success = true;
          }
        }
        else
        {
          break;
        }
      }
    }
    else
    {
      break;
    }
  }

  if(ret == RET_SUCCESS || ret == RET_MAPPING_NEEDED)
  {
    return success;
  }

  return false;
}