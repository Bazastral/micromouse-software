#include <stdio.h>

#include "platform_base.h"
#include "logger.h"
#include "mission.h"
#include "exhibit.h"

static void WaitForButtonClicked(void)
{
  while(platoformIsButtonPressed(0) == false)
  {
    platformDelayMs(100);
  }
  while(platoformIsButtonPressed(0) == true)
  {
    platformDelayMs(100);
  }
  platformDelayMs(1000);
}

int main(void)
{
  ret_code_t ret = RET_SUCCESS;
  platformInit();
  missionInit(stateStartingPoint(), (lab_cellxy_t){4u, 4u});
  log_set_verbosity_level(LOGGER_LVL_WARN);

  WaitForButtonClicked();
  while (true)
  {
    ret = missionCompetitionMainThread();
    if(ret == RET_AGAIN)
    {
      continue;
    }
    else if(ret == RET_SUCCESS)
    {
      // Important step was done. Clean wheels, move mouse to
      // the starting point
      exhibitEvent(EXHIBIT_SUCCESS);
      WaitForButtonClicked();
    }
    else
    {
      // unexpected error
      exhibitEvent(EXHIBIT_CRITICAL_ERROR);
      while(1)
      {
        // wait for reset
        platformDelayMs(1000);
      }
    }
  }
  return 0;
}
