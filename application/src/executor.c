
#include <stdio.h>
#include <math.h>

#include "ret_codes.h"
#include "executor.h"
#include "labirynth.h"
#include "pid.h"
#include "useful.h"
#include "config.h"
#include "position.h"
#include "logger.h"


static const double DISTANCE_MARGIN_MM = 10;
static const double MAX_SPEED_LINEAR = 800;
static const double MIN_SPEED_LINEAR = 10;
static const double HEADING_THRESHOLD_TURNING = DEG_2_RAD(5);
static const double HEADING_THRESHOLD_CORRECTION = DEG_2_RAD(1);
static const double DISTANCE_CLOSE_TO_TARGET_MM = 50;
static const double TYPICAL_SPEED_LINEAR_SPEED_RUN = 170;
static const double TYPICAL_SPEED_LINEAR_FAST = 150;
static const double TYPICAL_SPEED_LINEAR_SLOW = 80;
static const double TYPICAL_SPEED_ANGULAR_TURNING = M_PI;
static const double TYPICAL_SPEED_ANGULAR_FORWARD = M_PI/10;

uint8_t memPidV[PID_CONTROLLER_STRUCT_SIZE_BYTES];
uint8_t memPidW[PID_CONTROLLER_STRUCT_SIZE_BYTES];
static pid_controller_t pidV = (pid_controller_t) memPidV;
static pid_controller_t pidW = (pid_controller_t) memPidW;
static const config_t *cfg;

static bool speed_run = false;
void executorSetSpeedRun(const bool speed_run_active)
{
  speed_run = speed_run_active;
}
ret_code_t executorInit(const config_t *cfg_)
{
  ret_code_t ret = RET_ERR_INVALID_PARAM;
  speed_run = false;

  if(cfg_ != NULL)
  {
    cfg = cfg_;
    ret = pidInit(pidV, cfg->linearPIDp, cfg->linearPIDi, cfg->linearPIDd);
  }

  if(ret == RET_SUCCESS)
  {
    ret = pidInit(pidW, cfg->angularPIDp, cfg->angularPIDi, cfg->angularPIDd);
  }


  return ret;
}

ret_code_t executorGoToTarget(point_t aim, state_t state, double time, motors_speed_t *motors)
{
  double w, v;
  ret_code_t ret = RET_ERR_OTHER;

  double distance_to_aim_mm = pointGetDistance(stateGetPoint(state), aim);

  if(distance_to_aim_mm > LAB_DIM_MAX*sqrt(2))
  {
    log_debug("EXE: distance too big: %f > %f", distance_to_aim_mm, LAB_DIM_MAX);
    return RET_ERR_OUT_OF_BOUNDS;
  }
  if(distance_to_aim_mm < DISTANCE_MARGIN_MM)
  {
    log_debug("EXE: target achieved");
    return RET_SUCCESS;
  }
  if(motors == NULL)
  {
    return RET_ERR_INVALID_PARAM;
  }
  if(cfg == NULL)
  {
    return RET_ERR_NOT_INITED;
  }

  double heading = posGetHeadingToPoint(stateGetPosition(state), aim);
  double linear_velocity_mm_per_s = 0.0;
  double rotational_velocity = heading;
  double sign = 0;
  if(fabs(heading) >= HEADING_THRESHOLD_CORRECTION)
  {
    sign = sign_double(heading);
  }

  if(fabs(heading) < HEADING_THRESHOLD_TURNING)
  {
    if (distance_to_aim_mm < DISTANCE_CLOSE_TO_TARGET_MM)
    {
      linear_velocity_mm_per_s = TYPICAL_SPEED_LINEAR_SLOW;
    }
    else
    {
      // TODO this does not work
      // if(linear_velocity_mm_per_s < TYPICAL_SPEED_LINEAR)
      // {
      //   linear_velocity_mm_per_s = state.v + time*SPEED_LINEAR_ACCELERATION_MM_PER_S_SQARE;
      // }
      // else
      {
        linear_velocity_mm_per_s = TYPICAL_SPEED_LINEAR_FAST;
      }
    }

    if(speed_run)
    {
      linear_velocity_mm_per_s = TYPICAL_SPEED_LINEAR_SPEED_RUN;
    }

    if(linear_velocity_mm_per_s > MAX_SPEED_LINEAR)
    {
      linear_velocity_mm_per_s = MAX_SPEED_LINEAR;
    }
    if (linear_velocity_mm_per_s < MIN_SPEED_LINEAR)
    {
      linear_velocity_mm_per_s = MIN_SPEED_LINEAR;
    }
    rotational_velocity = sign*TYPICAL_SPEED_ANGULAR_FORWARD;
  }
  //only rotation
  else
  {
    rotational_velocity = sign * TYPICAL_SPEED_ANGULAR_TURNING;
    if(speed_run)
    {
      linear_velocity_mm_per_s = TYPICAL_SPEED_LINEAR_SLOW;
    }
  }


  ret = pidStep(pidV, time, linear_velocity_mm_per_s - state.v, &v);

  if(ret == RET_SUCCESS)
  {
    ret = pidStep(pidW, time, rotational_velocity - state.w, &w);
  }

  // log_info("%+5.0f, actual %+5.0f, pid %+3.2f", linear_velocity_mm_per_s, state.v, v);
  // log_info("%+5.2f, actual %+5.2f, pid %+3.2f", rotational_velocity, state.w, w);

  if(ret == RET_SUCCESS)
  {
    *motors = executorSetRawVelocities(v, w);
  }

  return RET_AGAIN;;
}


ret_code_t executorGoToTargetAndStop(point_t aim, state_t state, double time, motors_speed_t *motors)
{
  ret_code_t ret = RET_SUCCESS;

  ret = executorGoToTarget(aim, state, time, motors);

  if(ret == RET_SUCCESS)
  {
    motors->left = 0;
    motors->right = 0;
    ret = RET_SUCCESS;
  }

  return ret;
}

motors_speed_t executorSetRawVelocities(double raw_linear, double raw_angular)
{
  motors_speed_t motors;

  motors.left = raw_linear - raw_angular;
  motors.right = raw_linear + raw_angular;

  // log_debug("EXE: set v %-1.3f, w %-1.3f -> L %+1.3f R %+1.3f", raw_linear, raw_angular, motors.left, motors.right);

  return motors;
}