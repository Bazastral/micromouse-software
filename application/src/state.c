

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "state.h"
#include "labirynth.h"
#include "labirynth_mapping.h"
#include "useful.h"
#include "distance_sensors.h"
#include "distance_sensors_hw.h"
#include "logger.h"



static const config_t* cfg = NULL;
static bool inited = false;
static const double STATE_ESTIMATION_ALLOWED_ERROR_DISTANCE_MM = 10;
static const double STATE_ESTIMATION_ALLOWED_ERROR_ANGLE_RAD = M_PI_4;
static char printing_buf[100];

/**
 * @brief Check if measurements are valid. Measurements are valid only when the mouse
 * is in corridor (not during turning), because IR sensors have terrible outcomes
 * when they point the obstacle with different angle then the model one
 *
 */
static bool stateDistSensorsValidMouseHeading(double mouse_heading_rad, double allowed_angle_deg)
{
  double modulo = fmod(fabs(mouse_heading_rad), deg2rad(90));
  double down =  deg2rad(allowed_angle_deg);
  double up = deg2rad(90 - allowed_angle_deg);
  if (modulo > down &&
      modulo < up)
  {
    return false;
  }
  return true;
}

void stateInitModule(const config_t *cfg_)
{
  cfg = cfg_;
  inited = true;
}

ret_code_t stateNextModel(
    double time,
    state_t now,
    state_t *next)
{
  if(!inited) { log_debug("State module not inited"); return RET_ERR_NOT_INITED; }

  next->x = now.x + now.v * cos(now.t) * time;
  next->y = now.y + now.v * sin(now.t) * time;
  next->t = now.t + now.w * time;
  next->v = now.v;
  next->w = now.w;
  return RET_SUCCESS;
}

ret_code_t stateNextEncodersDeprecated(
    double time,
    state_t now,
    state_t *next,
    motors_ticks_t ticks)
{
  if(time == 0) return RET_ERR_INVALID_PARAM;
  if(!inited) { log_debug("State module not inited"); return RET_ERR_NOT_INITED; }
  /*
  This implementation is quite simple but also quite not accurate
  And I think it has some bugs in turning...
  */

  double avg_ticks = (ticks.left + ticks.right) / 2;
  //assume that robot drives forward - there is no curve
  next->x = now.x + cfg->whlCirc * avg_ticks * cos(now.t) / cfg->whlTicksPerTurn;
  next->y = now.y + cfg->whlCirc * avg_ticks * sin(now.t) / cfg->whlTicksPerTurn;
  double dt = ((ticks.right - ticks.left) * cfg->whlCirc / cfg->whlTrack) / cfg->whlTicksPerTurn;
  next->t = now.t + dt;
  next->v = (cfg->whlCirc * avg_ticks / time) / cfg->whlTicksPerTurn;
  next->w = dt / time;

  return RET_SUCCESS;
}

ret_code_t stateNextEncoders(
    double time,
    state_t now,
    state_t *next,
    motors_ticks_t ticks)
{
  /*
  Implementation based on https://www.cs.columbia.edu/~allen/F19/NOTES/icckinematics.pdf
  Dudek and Jenkin, Computational Principles of Mobile Robotics
  */
  if(time == 0) return RET_ERR_INVALID_PARAM;
  if(!inited) { log_debug("State module not inited"); return RET_ERR_NOT_INITED; }


  double distance_left = ticks.left * cfg->whlCirc / cfg->whlTicksPerTurn;
  double distance_right = ticks.right * cfg->whlCirc / cfg->whlTicksPerTurn;

  if(distance_left == distance_right)
  {
    // robot drives forward - there is no curve
    next->x = now.x + distance_left * cos(now.t);
    next->y = now.y + distance_left * sin(now.t);
    next->t = now.t;
    next->v = distance_left / time;
    next->w = 0;
  }
  else
  {
    double R = (cfg->whlTrack/2) * (distance_left+distance_right) / (distance_right-distance_left);
    double w = (distance_right-distance_left)/(cfg->whlTrack);
    point_t ICC;
    ICC.x = now.x - R*sin(now.t);
    ICC.y = now.y + R*cos(now.t);

    next->x = cos(w) * (now.x-ICC.x) - sin(w) * (now.y - ICC.y) + ICC.x;
    next->y = sin(w) * (now.x-ICC.x) + cos(w) * (now.y - ICC.y) + ICC.y;
    next->t = normalizeRad(now.t + w);
    next->v = ((distance_left+distance_right)/2) / time;
    next->w = w / time;
  }

  // stateDump("now", now);
  // stateDump("nex", *next);
  // log_debug("ticks %d %d, circ %f ", ticks.left, ticks.right, cfg->whlCirc);
  // log_debug("circ %f", 2*M_PI*cfg->whlRadius);
  // log_debug("distance l %f, r %f", distance_left, distance_right);
  // stateDump("enc", *next);

  return RET_SUCCESS;
}

ret_code_t stateInitialize(state_t *st)
{
  st->x = LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL;
  st->y = LAB_DIM_CORRIDOR_HALF + LAB_DIM_WALL;
  st->t = M_PI/2;
  st->v = 0;
  st->w = 0;
  return RET_SUCCESS;
}

state_t stateStartingPoint(void)
{
  state_t s;
  stateInitialize(&s);
  return s;
}

ret_code_t stateNextDistSensors(
    const state_t prev,
    state_t *next,
    double *measurements,
    uint8_t measurements_num)
{
  ret_code_t ret = RET_SUCCESS;
  state_t next_states[MAX_DISTANCE_SENSORS_NUM] = {0};
  dsConfig_t* dsCfg = dsGetCfg();
  bool take_into_account[MAX_DISTANCE_SENSORS_NUM];

  memset(take_into_account, 0, sizeof(take_into_account));

  if(dsCfg->numberOfSensors == 0)
  {
    return RET_ERR_OTHER;
  }
  else if(measurements_num > dsCfg->numberOfSensors)
  {
    return RET_ERR_INVALID_PARAM;
  }

  if(stateDistSensorsValidMouseHeading(prev.t, dsCfg->allowed_measurement_angle_deg) == false)
  {
    log_raw("turn");
    return RET_ERR_OUT_OF_BOUNDS;
  }

  for (uint8_t i = 0u; i < measurements_num; i++)
  {
    // adjust min, max by some factor
    if(measurements[i] < dsCfg->min_range_mm * 1.1)
    {
      log_debug("skip sensor %d reading %f, too close", i, measurements[i]);
      log_raw("<");
      continue;
    }
    if(measurements[i] > dsCfg->max_range_mm * 0.9)
    {
      log_debug("skip sensor %d reading %f, too far", i, measurements[i]);
      log_raw(">");
      continue;
    }

    pos_t sensor_pos = posCoordsLocalToGlobal(dsCfg->sensors[i].offset, stateGetPosition(prev));
    pos_t sensor_pos_updated = sensor_pos;
    posDump("sensor", sensor_pos);
    lab_specific_wall_t wall;
    ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &wall, NULL);
    labDumpSpecificWall(LOGGER_LVL_DEBUG," ", wall);
    if(ret != RET_SUCCESS)
    {
      log_raw("!");
      continue;
    }

    lab_walls_coords_t walls_positions = labGetWallsCoordsFromCellXY(labPoint2CellXY(posGetPoint(sensor_pos)));

    double wall_axis_xy = walls_positions.E;
    if(wall.direction & LAB_WEST)
    {
      wall_axis_xy = walls_positions.W;
    }
    else if(wall.direction&LAB_NORTH)
    {
      wall_axis_xy = walls_positions.N;
    }
    else if(wall.direction&LAB_SOUTH)
    {
      wall_axis_xy = walls_positions.S;
    }

    next_states[i] = prev;

    if(measurements[i] > LAB_DIM_CORRIDOR*sqrt(2))
    {
      // only the closest wall can be used
      log_raw(">");
      continue;
    }
    if(measurements[i] < LAB_DIM_WALL_HALF)
    {
      // assume that measurement cannot be too small
      log_raw("<");
      continue;
    }

    if(labWallsExist(labPoint2CellXY(posGetPoint(sensor_pos)), wall.direction))
    {
      next_states[i] = prev;
      if((wall.direction&LAB_WEST) || (wall.direction&LAB_EAST))
      {
        dsEstimatePositionOfOneSensor(
          sensor_pos,
          &sensor_pos_updated,
          measurements[i],
          wall_axis_xy);

          next_states[i].x = prev.x + (sensor_pos_updated.x - sensor_pos.x);
          next_states[i].t = prev.t + (sensor_pos_updated.t - sensor_pos.t);
      }
      else
      {
        dsEstimatePositionOfOneSensorY(
          sensor_pos,
          &sensor_pos_updated,
          measurements[i],
          wall_axis_xy);

          next_states[i].y = prev.y + (sensor_pos_updated.y - sensor_pos.y);
          next_states[i].t = prev.t + (sensor_pos_updated.t - sensor_pos.t);
      }
      take_into_account[i] = true;
      log_debug("take into account %d", i);
      log_raw("+");
    }
    else
    {
      log_debug("Proper reading, but no wall?");
      log_raw("-");
    }
  }

  *next = prev;
  uint8_t sensors_taken = 0;
  for (uint8_t i = 0u; i < dsCfg->numberOfSensors; i++)
  {
    if(take_into_account[i])
    {
      next->x += next_states[i].x;
      next->y += next_states[i].y;
      next->t += next_states[i].t;
      sensors_taken++;
    }
  }
  log_debug("sensors taken %d", sensors_taken);
  next->x /= (sensors_taken+1);
  next->y /= (sensors_taken+1);
  next->t /= (sensors_taken+1);

  return RET_SUCCESS;
}

point_t stateGetPoint(state_t state)
{
  return (point_t) {state.x, state.y};
}

pos_t stateGetPosition(state_t state)
{
  return (pos_t) {state.x, state.y, state.t};
}

void stateDump(char* desc, state_t s)
{
  char* ptr = printing_buf;
  // uint16_t len = 0;

  snprintf(ptr, sizeof(printing_buf) - (ptr - printing_buf),
   "St %s:%4.0f %4.0f %+3.0f' %3.0f", desc, s.x, s.y, rad2deg(s.t), s.v);

  // len = snprintf(ptr, sizeof(printing_buf) - (ptr - printing_buf), "State %s: ", desc); ptr += len; if(ptr > printing_buf + sizeof(printing_buf)){return;}
  // len = snprintf(ptr, sizeof(printing_buf) - (ptr - printing_buf), "x=%4.0f ", s.x); ptr += len; if(ptr > printing_buf + sizeof(printing_buf)){return;}
  // len = snprintf(ptr, sizeof(printing_buf) - (ptr - printing_buf), "y=%4.0f ", s.y); ptr += len; if(ptr > printing_buf + sizeof(printing_buf)){return;}
  // len = snprintf(ptr, sizeof(printing_buf) - (ptr - printing_buf), "t=%+2.2f (%+4.0f') ", s.t, rad2deg(s.t)); ptr += len; if(ptr > printing_buf + sizeof(printing_buf)){return;}
  // len = snprintf(ptr, sizeof(printing_buf) - (ptr - printing_buf), "v=%+4.0f ", s.v); ptr += len; if(ptr > printing_buf + sizeof(printing_buf)){return;}
  // len = snprintf(ptr, sizeof(printing_buf) - (ptr - printing_buf), "w=%+2.2f ", s.w); ptr += len; if(ptr > printing_buf + sizeof(printing_buf)){return;}

  log_info(printing_buf);
}

bool checkIfDistanceSensorsAreVeryDifferentThanEncoders(
  const state_t state_next_ds,
  const state_t state_next_encoders)
{
  if (fabs(state_next_encoders.x - state_next_ds.x) > STATE_ESTIMATION_ALLOWED_ERROR_DISTANCE_MM)
  {
    log_warn("State est warn x %5.2f vs %5.2f", state_next_ds.x, state_next_encoders.x);
    return true;
  }
  if (fabs(state_next_encoders.y - state_next_ds.y) > STATE_ESTIMATION_ALLOWED_ERROR_DISTANCE_MM)
  {
    log_warn("State est warn y %5.2f vs %5.2f", state_next_ds.y, state_next_encoders.y);
    return true;
  }
  if (fabs(normalizeRad(state_next_encoders.t - state_next_ds.t)) > STATE_ESTIMATION_ALLOWED_ERROR_ANGLE_RAD)
  {
    log_warn("State est warn t %5.2f vs %5.2f", state_next_ds.t, state_next_encoders.t);
    return true;
  }
  return false;
}

ret_code_t stateStep(
    const double time,
    state_t *state,
    double *measurements,
    uint8_t measurements_num,
    motors_ticks_t ticks)
{
  ret_code_t ret = RET_SUCCESS;
  state_t state_next_encoders;
  state_t state_next_ds;
  state_t state_now = *state;
  log_raw("\n");
  /*
    This is the trickies place in the whole codebase...
    I want to simplify as much as I can. So I implement weighted averaging of two
    kinds of sensors: encoders and distance.
    I assume that encoders will be more precise.
    For now, do not take into account the model of the robot
  */

  state_next_encoders = state_now;
  state_next_ds = state_now;

  ret = stateNextEncoders(time, state_now, &state_next_encoders, ticks);

  if(ret == RET_SUCCESS)
  {
    // as an input take state from encoders
    // so the new position will be only adjusted
    ret = stateNextDistSensors(state_next_encoders, &state_next_ds, measurements, measurements_num);
    if(ret == RET_ERR_OUT_OF_BOUNDS)
    {
      // measurement cannot be done... pretend that it is valid
      state_next_ds = state_next_encoders;
      ret = RET_SUCCESS;
    }
  }

  if(ret == RET_SUCCESS)
  {
    state_now.x = (9 * state_next_encoders.x + 1 * state_next_ds.x) / 10;
    state_now.y = (9 * state_next_encoders.y + 1 * state_next_ds.y) / 10;
    state_now.t = (9 * state_next_encoders.t + 1 * state_next_ds.t) / 10;
    state_now.w = (6 * state_next_encoders.w + 0 * state_next_ds.w) / 6;
    state_now.v = (6 * state_next_encoders.v + 0 * state_next_ds.v) / 6;

    log_raw("\n");
    // stateDump("ds ", state_next_ds);
    // log_info("%4.0f %4.0f %4.0f %4.0f",
    //          state_next_encoders.x, state_next_ds.x,
    //          state_next_encoders.y, state_next_ds.y);
    stateDump("now", state_now);
    *state = state_now;

    // check if distance sensors are very different than encoders
    if (
        checkIfDistanceSensorsAreVeryDifferentThanEncoders(state_next_ds, state_next_encoders) == true)
    {
      ret = RET_ERR_OTHER;
    }
  }

  return ret;
}

ret_code_t stateDetectWalls(
    const state_t current_state,
    const double *measurements,
    const double measurements_num)
{

  ret_code_t ret = RET_SUCCESS;
  ret_code_t ret_sensor = RET_SUCCESS;
  dsConfig_t *dsCfg = dsGetCfg();

  if(measurements == NULL)
  {
    return RET_ERR_INVALID_PARAM;
  }
  if(stateDistSensorsValidMouseHeading(current_state.t, dsCfg->allowed_measurement_angle_deg) == false)
  {
    return RET_ERR_OUT_OF_BOUNDS;
  }

  log_debug("");
  log_set_verbosity_level(LOGGER_LVL_INFO);
  for(uint8_t i = 0u; i < measurements_num; i++)
  {
    log_debug("sensor %u, measured %f", i, measurements[i]);
    ret_sensor = dsDetectWalls(
        posCoordsLocalToGlobal(dsCfg->sensors[i].offset, stateGetPosition(current_state)),
        measurements[i]);
    if(ret_sensor != RET_SUCCESS)
    {
      ret = ret_sensor;
    }
  }

  return ret;
}