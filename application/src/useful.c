

#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "useful.h"


double normalizeRad(double rad)
{
  while(rad > M_PI) { rad -= 2*M_PI; }
  while(rad <= -M_PI) { rad += 2*M_PI; }
  return rad;
}

double sign_double(double val)
{
  if(val < 0.0)
  {
    return -1;
  }

  return 1;
}

double deg2rad(double deg)
{
    return normalizeRad((deg * M_PI) / 180);
}

double rad2deg(double rad)
{
    return (rad * 180)/M_PI;
}

double ctg(double rad)
{
  double s = sin(rad);
  if(s != 0)
  {
    return cos(rad)/sin(rad);
  }
  return __DBL_MAX__;
}

// https://phoxis.org/2013/05/04/generating-random-numbers-from-normal-distribution-in-c/
double generate_normal_distribution(double mu, double sigma)
{
  double U1, U2, W, mult;
  static double X1, X2;
  static int call = 0;

  if (call == 1)
  {
    call = !call;
    return (mu + sigma * (double)X2);
  }

  do
  {
    U1 = -1 + ((double)rand() / RAND_MAX) * 2;
    U2 = -1 + ((double)rand() / RAND_MAX) * 2;
    W = pow(U1, 2) + pow(U2, 2);
  } while (W >= 1 || W == 0);

  mult = sqrt((-2 * log(W)) / W);
  X1 = U1 * mult;
  X2 = U2 * mult;

  call = !call;

  return (mu + sigma * (double)X1);
}