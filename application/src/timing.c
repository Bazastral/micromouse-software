

#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "timing.h"
#include "platform_base.h"
#include "logger.h"

const uint32_t LOOP_WAITING_DURATION_US = 1;

void timingMeasureStart(uint32_t *start_timestamp)
{
  if(start_timestamp != 0)
  {
    *start_timestamp =  platformGetElapsedUsFromInit();
  }
}

uint32_t timingMeasureElapsedUs(const uint32_t start_timestamp)
{
  uint32_t now = 0;
  uint32_t elapsed = 0;

  now = platformGetElapsedUsFromInit();
  elapsed = now - start_timestamp;

  return elapsed;
}

void timingLoopInitialize(timing_loop_ctx_t* const ctx, uint32_t duration_us)
{
  memset(ctx, 0, sizeof(*ctx));

  ctx->internal.duration_us = duration_us;
  ctx->internal.inited = true;
  ctx->internal.previous_timestamp_us = platformGetElapsedUsFromInit();
  ctx->external.task_duration_min = UINT32_MAX;
}

static void calculate_task_duration(timing_loop_ctx_t* const ctx, const uint32_t now)
{
  ctx->external.previous_task_duration = now - ctx->internal.previous_timestamp_us;
}
static void calculate_task_statistics(timing_loop_ctx_t* const ctx, const uint32_t new_duration)
{
  if(ctx->external.task_duration_max < new_duration)
  {
    ctx->external.task_duration_max = new_duration;
  }
  if(ctx->external.task_duration_min > new_duration)
  {
    ctx->external.task_duration_min = new_duration;
  }

  uint64_t sum = 0;
  for(uint8_t i = 0; i<TIMING_PREV_DURATIONS_NUMBER; i++)
  {
    sum += ctx->internal.previous_task_durations[i];
  }

  ctx->external.task_duration_average_us = sum / TIMING_PREV_DURATIONS_NUMBER;

  ctx->internal.previous_task_durations[ctx->external.total_loops_number % TIMING_PREV_DURATIONS_NUMBER] = new_duration;
}

void timingLoopControl(timing_loop_ctx_t* const ctx)
{
  if(ctx == NULL)
  {
    return;
  }
  ctx->external.total_loops_number++;
  if (ctx->internal.inited == false)
  {
    log_error("timing loop not inited");
    ctx->external.errors_number++;
  }
  else
  {
    uint32_t now = platformGetElapsedUsFromInit();
    calculate_task_duration(ctx, now);
    calculate_task_statistics(ctx, ctx->external.previous_task_duration);
    bool waited_additionaly_in_this_loop = false;

    while ((now - ctx->internal.previous_timestamp_us) < ctx->internal.duration_us)
    {
      platformDelayUs(LOOP_WAITING_DURATION_US);
      now = platformGetElapsedUsFromInit();
      waited_additionaly_in_this_loop = true;
    }

    if (waited_additionaly_in_this_loop == false)
    {
      ctx->external.too_long_total_loops_number++;
      log_warn("loop too long: %dus", ctx->external.previous_task_duration);
      ctx->internal.previous_timestamp_us = now;
    }
    else
    {
      ctx->internal.previous_timestamp_us += ctx->internal.duration_us;
    }
  }
}

void timingLoopPrintStats(const timing_loop_ctx_t* const ctx)
{
  log_info("timing prev %5d min %5d max %5d avg %5d. total %5d long %3d err %3d",
           (int)ctx->external.previous_task_duration,
           (int)ctx->external.task_duration_min,
           (int)ctx->external.task_duration_max,
           (int)ctx->external.task_duration_average_us,

           (int)ctx->external.total_loops_number,
           (int)ctx->external.too_long_total_loops_number,
           (int)ctx->external.errors_number);
  // log_error("%9d", (int)ctx->internal.previous_timestamp_us);
}