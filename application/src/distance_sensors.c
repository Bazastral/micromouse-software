
#include <math.h>
#include <stdbool.h>

#include "distance_sensors_hw.h"
#include "useful.h"
#include "ret_codes.h"
#include "labirynth_mapping.h"
#include "logger.h"


static dsConfig_t cfg = { 0 };
static const double PERPENDICULLAR_OFFSET = DEG_2_RAD(15);

static const double DETECTION_THRESHOLD_MM = 30;

ret_code_t dsInit(const dsConfig_t dsCfg)
{
  cfg = dsCfg;
  return RET_SUCCESS;
}

dsConfig_t* dsGetCfg(void)
{
  return &cfg;
}

ret_code_t dsEstimatePositionOfOneSensor(
    pos_t sensor_pos,
    pos_t *sensor_pos_updated,
    double distance,
    double wall_x)
{
  /**
   * @brief explanation
   * Function updates all three components of the position of the ds (distance sensor): x, y, theta
   * Of course, algorithm has to update 3 variables based on only one measurement. It is not possible.
   * So it updates each one variable as if the two others were constant.
   *
   * Naming convention:
   * o - position where ray of the "laser" crosses wall.
   * @see doc/distance_sensors.drawio.png
   */
  // distance in x axis from wall to sensor
  double dx = wall_x - sensor_pos.x;
  // ax y value where laser hits wall
  double yop = sensor_pos.y + dx * tan(sensor_pos.t);
  // distance measured by sensor (squared)
  double dm_sqared = distance*distance;
  double dy = sqrt(fabs(dm_sqared - dx*dx)) * sign_double(sin(sensor_pos.t));

  *sensor_pos_updated = sensor_pos;
  double modulo = fmod(fabs(sensor_pos.t), M_PI);
  if ((modulo > M_PI-PERPENDICULLAR_OFFSET) || (modulo < +PERPENDICULLAR_OFFSET))
  {
    log_debug("ds skip");
    // skip it, do not update angle of the mouse when the sensor is pointing the wall perpendicullary
  }
  else
  {
    sensor_pos_updated->t = atan2(dy, dx);
  }

  sensor_pos_updated->x = wall_x - sqrt(fabs(dm_sqared - pow(yop - sensor_pos.y, 2))) * sign_double(cos(sensor_pos.t));
  sensor_pos_updated->y = yop - sqrt(fabs(dm_sqared - pow(wall_x - sensor_pos.x, 2))) * sign_double(sin(sensor_pos.t));

  return RET_SUCCESS;
}

ret_code_t dsEstimatePositionOfOneSensorY(
    pos_t sensor_pos,
    pos_t *sensor_pos_updated,
    double distance,
    double wall_y)
{
  double dy = wall_y - sensor_pos.y;
  double dm_sqared = distance*distance;
  double dx = sqrt(fabs(dm_sqared - dy*dy)) * sign_double(cos(sensor_pos.t));
  double xop = sensor_pos.x + dy * ctg(sensor_pos.t);

  *sensor_pos_updated = sensor_pos;
  double modulo = fmod(fabs(sensor_pos.t), M_PI);
  if ((modulo > M_PI_2-PERPENDICULLAR_OFFSET) && (modulo < M_PI_2+PERPENDICULLAR_OFFSET))
  {
    log_debug("ds skip y");
    // skip it, do not update angle of the mouse when the sensor is pointing the wall perpendicullary
  }
  else
  {
    sensor_pos_updated->t = atan2(dy, dx);
  }
  sensor_pos_updated->x = xop - sqrt(fabs(dm_sqared - pow(wall_y - sensor_pos.y, 2))) * sign_double(cos(sensor_pos.t));
  sensor_pos_updated->y = wall_y - sqrt(fabs(dm_sqared - pow(xop - sensor_pos.x, 2))) * sign_double(sin(sensor_pos.t));

  return RET_SUCCESS;
}

lab_cell_mask_t dsWhichWallSensorPoints(pos_t sensor_pos)
{
  double theta = normalizeRad(sensor_pos.t);
  lab_cellxy_t cellxy = labPoint2CellXY(posGetPoint(sensor_pos));
  lab_walls_coords_t walls = labGetWallsCoordsFromCellXY(cellxy);

  if ((sensor_pos.x <= walls.W) ||
      (sensor_pos.x >= walls.E) ||
      (sensor_pos.y <= walls.S) ||
      (sensor_pos.y >= walls.N))
  {
    return LAB_ALL_DIRS;
  }

  double thNW = atan2(walls.N - sensor_pos.y, walls.W - sensor_pos.x);
  double thNE = atan2(walls.N - sensor_pos.y, walls.E - sensor_pos.x);
  double thSE = atan2(walls.S - sensor_pos.y, walls.E - sensor_pos.x);
  double thSW = atan2(walls.S - sensor_pos.y, walls.W - sensor_pos.x);

  if (theta <= M_PI && theta > thNW)
  {
    return LAB_WEST;
  }
  else if (theta > thNE)
  {
    return LAB_NORTH;
  }
  else if (theta > thSE)
  {
    return LAB_EAST;
  }
  else if (theta > thSW)
  {
    return LAB_SOUTH;
  }

  return LAB_WEST;
}


ret_code_t dsWhichClosestWallSensorShouldPoint(
    pos_t sensor_pos,
    lab_specific_wall_t* wall,
    double *distance
)
{
  ret_code_t ret = RET_SUCCESS;

  double y = sensor_pos.y;
  lab_cellxy_t cellxy = labPoint2CellXY(posGetPoint(sensor_pos));
  lab_cell_mask_t stupid_wall = dsWhichWallSensorPoints(sensor_pos);

  if(stupid_wall == LAB_ALL_DIRS)
  {
    return RET_ERR_INVALID_PARAM;
  }

  if(distance != NULL)
  {
    if (stupid_wall & LAB_NORTH)
    {
      *distance = (((double)(cellxy.y + 1u) * LAB_DIM_CELL) - y) / sin(sensor_pos.t);
    }
    else if (stupid_wall & LAB_SOUTH)
    {
      *distance = (((double)(cellxy.y) * LAB_DIM_CELL) + LAB_DIM_WALL - y) / sin(sensor_pos.t);
    }
    else if (stupid_wall & LAB_EAST)
    {
      *distance = (((double)(cellxy.x + 1u) * LAB_DIM_CELL) - sensor_pos.x) / cos(sensor_pos.t);
    }
    else
    {
      *distance = (((double)(cellxy.x) * LAB_DIM_CELL) + LAB_DIM_WALL - sensor_pos.x) / cos(sensor_pos.t);
    }
  }

  if(wall != NULL)
  {
    wall->cell_xy = cellxy;
    wall->direction = stupid_wall;
  }

  return ret;
}

ret_code_t dsWhichWallSensorPointsWithDistance(
    pos_t sensor_pos,
    lab_specific_wall_t* wall,
    double *distance
)
{
  ret_code_t ret = RET_SUCCESS;

  ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, wall, distance);

  return ret;
}

ret_code_t dsDetectWalls(
    const pos_t sensor_pos,
    const double measurement)
{
  const double SMALL_MARGIN_MM = 1;
  ret_code_t ret = RET_ERR_OUT_OF_BOUNDS;
  if (measurement < cfg.min_range_mm + SMALL_MARGIN_MM)
  {
    log_debug("dsDetectWalls skip, too short %f", measurement);
  }
  else
  {
    double measurement_if_wall_exists = 0;
    lab_specific_wall_t spec_wall = {0};
    ret = dsWhichClosestWallSensorShouldPoint(sensor_pos, &spec_wall, &measurement_if_wall_exists);
    // TODO check status

    if (measurement_if_wall_exists > (cfg.max_range_mm - DETECTION_THRESHOLD_MM - SMALL_MARGIN_MM))
    {
      log_debug("dsDetectWalls skip, wall too far %f", measurement_if_wall_exists);
    }
    else
    {
      bool exists = false;
      if (measurement < measurement_if_wall_exists + DETECTION_THRESHOLD_MM)
      {
        exists = true;
      }
      labDumpSpecificWall(LOGGER_LVL_DEBUG, "w", spec_wall);
      log_warn("%+6.1f %+6.1f %d", measurement, measurement_if_wall_exists, (int)exists);
      if(ret != RET_SUCCESS)
      {
        // log_warn("ERR");
        return ret;
      }
      ret = labNoticeWall(spec_wall, exists);
    }
  }

  return ret;
}