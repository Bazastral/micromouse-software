
#include <assert.h>

#include "led.h"
#include "exhibit.h"
#include "platform_base.h"
#include "logger.h"


void exhibitFailures(int failures)
{
  if(failures > 0)
  {
    exhibitEvent(EXHIBIT_SOME_FAILURE);
  }
  else
  {
    exhibitEvent(EXHIBIT_SUCCESS);
  }

}

static void blinkAllLeds(int times)
{
  for(int i=0; i<times; i++)
  {
    platformSetLedState(LED_NUCLEO, LED_ON);
    platformSetLedState(LED_FRONT_LEFT, LED_ON);
    platformSetLedState(LED_FRONT_RIGHT, LED_ON);
    platformDelayMs(100u);
    platformSetLedState(LED_NUCLEO, LED_OFF);
    platformSetLedState(LED_FRONT_LEFT, LED_OFF);
    platformSetLedState(LED_FRONT_RIGHT, LED_OFF);
    platformDelayMs(100u);
  }
}

void exhibitEvent(EXHIBIT_EVENT_t event)
{
  switch(event)
  {
  case EXHIBIT_PLATFORM_INITIALIZED:
    blinkAllLeds(2);
    break;

  case EXHIBIT_SUCCESS:
    platformSetLedState(LED_NUCLEO, LED_ON);
    platformSetLedState(LED_FRONT_LEFT, LED_ON);
    platformSetLedState(LED_FRONT_RIGHT, LED_ON);

    platformDelayMs(3000);

    platformSetLedState(LED_NUCLEO, LED_OFF);
    platformSetLedState(LED_FRONT_LEFT, LED_OFF);
    platformSetLedState(LED_FRONT_RIGHT, LED_OFF);
    break;

  case EXHIBIT_CRITICAL_ERROR:
    log_error("CRITICAL ERROR");
    break;

  case EXHIBIT_SOME_FAILURE:
    blinkAllLeds(10);
    break;

  case EXHIBIT_BATTERY_EMPTY:
    while (1)
    {
      platformSetLedState(LED_FRONT_RIGHT, LED_ON);
      platformDelayMs(100u);
      platformSetLedState(LED_FRONT_RIGHT, LED_OFF);
      platformDelayMs(100u);
      log_error("BATTERY EMPTY!");
    }
    break;

  default:
    log_error("EXHIBIT not defined event");
  }
}